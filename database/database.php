<?php

class Database {
    private $database;

    public function __construct($servername, $username, $password, $dbname, $port) {
        $this->database = new mysqli($servername, $username, $password, $dbname, $port);
        if($this->database->connect_error) {
            die("Database connection failed;");
        }
    }

    public function getDBRowsCount() {
        $statement = $this->database->prepare("SELECT table_name, table_rows FROM INFORMATION_SCHEMA.TABLES");
        $statement->execute();
        $result = $statement->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
}

?>