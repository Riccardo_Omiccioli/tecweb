<?php
    use Database\Utente as Utente;

    require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");

    if(!isset($_SESSION)) {
        session_start();
    }
    if (isset($_SESSION["user"])){
		$user = Database\Utente::get($_SESSION["user"]);
        if($user->getSellerID($user->getID())) {
	    	$params["page"] = PAGE."ordersSeller.php";
	    	$params["title"] = "Ordini Venditore";
	    	$params["css"] = ["mainStyleSheet.css", "notificationsSheet.css", "ordersStyleSheet.css"];
			$params["scriptjs"] = [ "mainPageScript.js" , "orders.js", "vendor.js"];
	    	require_once(TEMPLATE."base.php");
		} else {
	    	$params["page"] = PAGE."orders.php";
	    	$params["title"] = "Ordini";
	    	$params["css"] = ["mainStyleSheet.css", "notificationsSheet.css", "ordersStyleSheet.css"];
			$params["scriptjs"] = [ "mainPageScript.js" , "orders.js"];
	    	require_once(TEMPLATE."base.php");
    	}
	}
?>

