<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."productList.php";
    $params["title"] = "Lista";
    $params["css"] = ["mainStyleSheet.css", "productListSheet.css"];
	$params["scriptjs"] = [ "mainPageScript.js" , "productList.js"];
    require_once(TEMPLATE."base.php");
?>