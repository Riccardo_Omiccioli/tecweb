<?php
    session_start();
    //var_dump($_SESSION["user"]);
    //unset($_SESSION["user"]); // Function to use to reset session user
    if(!isset($_SESSION["user"])){
        header("location: login.php");
        exit;
    }

	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."user.php";
    $params["title"] = "Utente";
	$params["css"] = ["mainStyleSheet.css", "inputSheet.css", "userSheet.css"];
	$params["scriptjs"] = [ "mainPageScript.js" , "userPageScript.js"];
    require_once(TEMPLATE."base.php");

?>
