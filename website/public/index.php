<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."home.php";
    $params["title"] = "Home";
    $params["css"] = ["mainStyleSheet.css"];
	$params["scriptjs"] = [ "mainPageScript.js"];
    require_once(TEMPLATE."base.php");
?>
