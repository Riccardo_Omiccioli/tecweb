<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."productPage.php";
    $params["title"] = "Prodotto";
	$params["css"] = ["mainStyleSheet.css", "productPageSheet.css"];
	$params["scriptjs"] = [ "mainPageScript.js" , "productPageScript.js"];
	$params["product"] = \Database\Products\Prodotto::get($_GET["product"])[0]; //TODO: set page to 404/400, or error.php with ["error"] = 404/400?
    require_once(TEMPLATE."base.php");
?>
