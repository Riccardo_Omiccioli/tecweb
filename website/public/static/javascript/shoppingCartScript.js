function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    // because unescape has been deprecated, replaced with decodeURI
    //return unescape(dc.substring(begin + prefix.length, end));
    return decodeURI(dc.substring(begin + prefix.length, end));
}

$(document).ready(function(){
    var oldTotale = parseFloat( $("#totale").text().toString().match("Totale: ([0-9\.\,]*)€")[1].replace(",",""));
    var lastNumber = {};
    var prima = {};
    $("body > main > div > ul > li > input").each(function() {
        this.addEventListener("input", function() {
            var newTotale;
            if(this.value <= this.max){
                //$(this).next().next()
                $(this).prev().find("p:nth-of-type(4)").css("display", "none");
               // console.log($(this).next().next().text());
                var localOldTotale = oldTotale;
                var prezzoUnitario = parseFloat($(this).prev().find("p:nth-of-type(2)").text().match("([0-9\.\,]*)€")[1].replace(",","") );
                //ora posso applicare il totale nuovo
                    /*
                if(this.value != $(this).attr("value")){
                    newTotale = localOldTotale - $(this).attr("value") * prezzoUnitario + this.value * prezzoUnitario;
                } else {
                    */
                    if(!prima[this.id]){
                        lastNumber[this.id]=$(this).attr("value");
                        prima[this.id] = true;
                    }
                    if( (this.value == lastNumber[this.id])){
                         newTotale = parseFloat(localOldTotale);
                    } else {
                        newTotale = parseFloat(localOldTotale)+(-(lastNumber[this.id] - this.value )) * prezzoUnitario;
                    }
                
                var oldCart = JSON.parse(getCookie("cart"));
                oldCart[this.id] = this.value;
                document.cookie ="cart="+JSON.stringify(oldCart);
                var c_number = shoppingCartNumberCalc();
                setCartNumber(c_number);
                newTotale = parseFloat(newTotale).toFixed(2);
                $("#totale").text("Totale: "+newTotale+"€");
                oldTotale = newTotale;
                lastNumber[this.id] = this.value;
            } else {
                $(this).prev().find("p:nth-of-type(4)").css("display", "block");
            }
        });
    });

    $("body > main > div > ul > li > button").click(function(event){

        event.preventDefault();
        var toDelete = $(this.previousElementSibling).attr('id');
        var newCookie = {};
        newCookie = JSON.parse(getCookie("cart"));
        delete newCookie[toDelete];
        document.cookie = "cart=" + JSON.stringify(newCookie);
        location.reload();
        return false;
    });
/*
                    if(JSON.parse(getCookie("cart")) === null ){
                        //caso in cui è il primo oggetto aggiunto al carrello
                        var cart = {}; 
                        cart[idProduct] = totalQuantityOfProduct;
                    } else {
                        //caso in cui localStorage contiene altri prodotti
                        cart = JSON.parse(getCookie("cart"));
                        cart[idProduct] = totalQuantityOfProduct;
                    }
                    document.cookie = "cart=" + JSON.stringify(cart);
                    alert(JSON.parse(getCookie("cart"))[idProduct]);
*/
    $(document.getElementById('buy')).click(function(event){
        event.preventDefault();

        if(getCookie("cart") === null || getCookie("cart") === undefined){   

            $(document.getElementById('error')).css("display","block");
            var intervaError = window.setTimeout(function(){
                location.reload();
            }, 2500);
            return false;
        }

        /* ultimo controllo che l'utente non inserisca quantità errate e ultimo aggiornamento di cookie con i valori attuali negli input */
        var temp = false;
        var zero = 1;
        $("body > main > div > ul > li > input").each(function() {
            if(parseInt(this.value) < parseInt(parseInt($(this).attr("max"))+parseInt(1)) ){
                temp = false;
                if(this.value == 0){
                    $(document.getElementById('error')).html("Non è possibile acquistare quantità minori di uno");
                    $(document.getElementById('error')).css("display","block");
                    zero = 0;             
                } else {
                    var oldCart = JSON.parse(getCookie("cart"));
                    oldCart[this.id] = this.value;
                    document.cookie ="cart="+JSON.stringify(oldCart); 
                }
            } else {
                temp = true;
                return;
            }
        });
        if(temp === true) {
            $(document.getElementById('error')).html("Inserire numero di prodotto che sia disponibile all'acquisto");
            $(document.getElementById('error')).css("display","block");
            var intervaError = window.setTimeout(function(){
                    window.location.reload();
                }, 1500);
            return false;
        }
        if(zero == 0){
            return;
        }
        /* parte ajax */
        var buyReq = new XMLHttpRequest();
        buyReq.open("POST", "/api/order.php");
        var formData = new FormData();
        formData.set("cart", getCookie("cart"));
        buyReq.send(formData);

        buyReq.onreadystatechange = function(){
            if (this.readyState != 4) {
                return;
            }
            if (this.status == 200) {
                var cart = {};
                document.cookie = "cart=" + JSON.stringify(cart); // da reimpostare lato server
                /* imposto il numero a 0*/
                var c_number = shoppingCartNumberCalc();
                setCartNumber(c_number);
                var intervaError = window.setTimeout(function(){
                    window.location.href = "/orders.php#"+JSON.parse(buyReq.response).orderID;
                }, 2500);
                $(document.getElementById('error')).html("Ordine eseguito con successo!");
                $(document.getElementById('error')).removeClass("unavailable");
                $(document.getElementById('error')).addClass("available");
                $(document.getElementById('error')).css("display","block");
            } else if(this.status == 401){
                $(document.getElementById('error')).html("Eseguire prima l'accesso!");
                $(document.getElementById('error')).css("display","block");
                var intervaError = window.setTimeout(function(){
                    window.location.href = "/login.php";
                }, 1500);
                return false;
            } else if (this.status == 400) {
                var error = "Errore richiesta";
        		if (this.response) {
                    var response = new Object(); 
                    try { response = JSON.parse(this.response); } catch (e){}
        		    if (response.error) {
                        error = response.error;
                    }
        		    if (response.redirect) {
                        window.setTimeout(function() { window.location.href = response.redirect; }, 1500);
        		        return false;
        		    }
        	    }
                $("#error").html(response.error);
                $("#error").css("display", "block");
            } else {
            	$(document.getElementById('error')).html("Errore sconosciuto");
            	$(document.getElementById('error')).css("display","block");
            	var intervaError = window.setTimeout(function(){
            	    location.reload();
            	}, 1500);
            	return false;
            }
        }
    });
});

/* Funzione per aggiungere al carrello
    $(document.getElementById("carrello")).click(function(event){
        /* prendere un cookie con quel nome

        event.preventDefault();

        var quantityToBeAdded = parseInt(document.getElementById("quantity").value);
        if(isNaN(quantityToBeAdded)){
            quantityToBeAdded = 0;
        }
        var queryString = window.location.search;
        var urlParams = new URLSearchParams(queryString);
        var idProduct = urlParams.get('product');
        var oldCart;
        if(getCookie("cart") === null || JSON.parse(getCookie("cart"))[idProduct] === undefined ){
            oldCart = 0
        } else {
            oldCart = parseInt( JSON.parse(getCookie("cart"))[idProduct] );
        }
        /* vecchia parte dove si usava localstorage (funzionante)
        if(localStorage.getItem("cart") === null) {
            oldCart = 0;
        } else {
            oldCart = parseInt( JSON.parse(localStorage.getItem("cart"))[idProduct] );
        }
        if(isNaN(oldCart)) {
            oldCart = 0;
        }
        var totalQuantityOfProduct = oldCart + quantityToBeAdded;

        if(totalQuantityOfProduct > 0 ) {
            /* recupero l'id del prodotto dall'url
            var sendParams = new URLSearchParams();
            sendParams.append('product', idProduct);
            sendParams.append('quantity', totalQuantityOfProduct )

            var oReq = new XMLHttpRequest();
            oReq.open("GET", "/api/isProductAvailable.php?" + sendParams.toString());
            oReq.send(/*sendParams);
            oReq.onreadystatechange = function() {
                if (this.readyState != 4) {
                    return;
                }
                console.log(this.status);
                console.log(this.response);
                if (this.status == 200) {
                    if(JSON.parse(getCookie("cart")) === null ){
                        //caso in cui è il primo oggetto aggiunto al carrello
                        var cart = {}; 
                        cart[idProduct] = totalQuantityOfProduct;
                    } else {
                        //caso in cui localStorage contiene altri prodotti
                        cart = JSON.parse(getCookie("cart"));
                        cart[idProduct] = totalQuantityOfProduct;
                    }
                    document.cookie = "cart=" + JSON.stringify(cart);
                    alert(JSON.parse(getCookie("cart"))[idProduct]);
                } else if(this.status == 204){
                    //TODO prodotto esaurito da stampare
                    $("body > main > div > div > p:last-of-type").css('display','inline-block');
                    //TODO ricaricare la pagina dopo un tot secondi
                } else {
                    console.log("ciaone");
                    $("body > main > div > div > p:last-of-type").html("Errore sconosciuto.");
                    $("body > main > div > div > p:last-of-type").css('display','inline-block');
                }
            }
         } else {
            //TODO quantità non corretta
        }
    });

});
*/
