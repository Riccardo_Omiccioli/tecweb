function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    // because unescape has been deprecated, replaced with decodeURI
    //return unescape(dc.substring(begin + prefix.length, end));
    return decodeURI(dc.substring(begin + prefix.length, end));
}

$(document).ready(function(){

    $(document.getElementById("-")).click(function(event){
        event.preventDefault();
        var quantity = document.getElementById("quantity");
        if (quantity.value > 1) {
            quantity.value = quantity.value - 1;
        }
    });

    $(document.getElementById("+")).click(function(event){
        event.preventDefault();
        var quantity = document.getElementById("quantity");
        quantity.value = parseInt(quantity.value) + 1;
    });

/* Funzione per aggiungere al carrello */
    $(document.getElementById("carrello")).click(function(event){
        /* prendere un cookie con quel nome */
        event.preventDefault();

        var quantityToBeAdded = parseInt(document.getElementById("quantity").value);
        if(isNaN(quantityToBeAdded)){
            quantityToBeAdded = 0;
        }
        if(quantityToBeAdded < 1){
            $("body > main > div > div > p:last-of-type").removeClass("available");
            $("body > main > div > div > p:last-of-type").addClass("unavailable");
            $("body > main > div > div > p:last-of-type").html("Inserire numero positivo di prodotti");
            $("body > main > div > div > p:last-of-type").css('display','inline-block');
            return;
        }
        var queryString = window.location.search;
        var urlParams = new URLSearchParams(queryString);
        var idProduct = urlParams.get('product');
        var oldCart;
        if(getCookie("cart") === null || getCookie("cart") === undefined || JSON.parse(getCookie("cart"))[idProduct] === undefined){
            oldCart = 0
        } else {
            oldCart = parseInt( JSON.parse(getCookie("cart"))[idProduct] );
        }
        /* vecchia parte dove si usava localstorage (funzionante)
        if(localStorage.getItem("cart") === null) {
            oldCart = 0;
        } else {
            oldCart = parseInt( JSON.parse(localStorage.getItem("cart"))[idProduct] );
        }
        if(isNaN(oldCart)) {
            oldCart = 0;
        }
        */
        var totalQuantityOfProduct = oldCart + quantityToBeAdded;
        var toIdentify = $("body > main > div > div > section > p.available").text();

        var matchR = parseInt( toIdentify.toString().match("Disponibili: ([0-9]*)")[1]);

        if(isNaN(matchR)) matchR = 0;
        if( (totalQuantityOfProduct > 0) && (totalQuantityOfProduct < matchR + 1) ) {
            /* recupero l'id del prodotto dall'url */
            var sendParams = new URLSearchParams();
            sendParams.append('product', idProduct);
            sendParams.append('quantity', totalQuantityOfProduct );

            var oReq = new XMLHttpRequest();
            oReq.open("GET", "/api/isProductAvailable.php?" + sendParams.toString());
            oReq.send(/*sendParams*/);
            oReq.onreadystatechange = function() {
                if (this.readyState != 4) {
                    return;
                }
                if (this.status == 200) {
                    if(JSON.parse(getCookie("cart")) === null ){
                        //caso in cui è il primo oggetto aggiunto al carrello
                        var cart = {}; 
                        cart[idProduct] = totalQuantityOfProduct;
                    } else {
                        //caso in cui localStorage contiene altri prodotti
                        cart = JSON.parse(getCookie("cart"));
                        cart[idProduct] = totalQuantityOfProduct;
                    }
                    document.cookie = "cart=" + JSON.stringify(cart);
                    for (var i = quantityToBeAdded - 1; i >= 0; i--) {
                        increaseCartNumber();
                    }
                    $("body > main > div > div > p:last-of-type").removeClass("unavailable");
                    $("body > main > div > div > p:last-of-type").addClass("available");
                    $("body > main > div > div > p:last-of-type").html("Aggiunto al carrello");
                    $("body > main > div > div > p:last-of-type").css('display','inline-block');
                } else if(this.status == 204){
                    //TODO prodotto esaurito da stampare
                    $("body > main > div > div > p:last-of-type").removeClass("available");
                    $("body > main > div > div > p:last-of-type").addClass("unavailable");
                    $("body > main > div > div > p:last-of-type").css('display','inline-block');
                    //TODO ricaricare la pagina dopo un tot secondi
                } else {
                    $("body > main > div > div > p:last-of-type").removeClass("available");
                    $("body > main > div > div > p:last-of-type").addClass("unavailable");
                    $("body > main > div > div > p:last-of-type").html("Errore sconosciuto");
                    $("body > main > div > div > p:last-of-type").css('display','inline-block');
                }
            }
         } else {
            $("body > main > div > div > p:last-of-type").removeClass("available");
            $("body > main > div > div > p:last-of-type").addClass("unavailable");
            $("body > main > div > div > p:last-of-type").html("Stai cercando di acquistare troppi prodotti");
            $("body > main > div > div > p:last-of-type").css('display','inline-block');
        }

    });

    $(document.getElementById("modifica")).click(function(event){
        event.preventDefault();
        var queryString = window.location.search;
        var urlParams = new URLSearchParams(queryString);
        var idProduct = urlParams.get('product');
        //console.log(idProduct);
        window.location.href = "/addProduct.php?productID=" + idProduct;
    });

});