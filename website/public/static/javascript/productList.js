
function toggleDrawerFilterLeft() {
    if($("body > main > div > nav:nth-of-type(1)").hasClass("open")) {
        $("body > main > div > nav:nth-of-type(1)").removeClass("open").slideUp();
    } else {
        $('body > main > div > nav:nth-of-type(1)').addClass("open").slideDown().css('display','flex');
    }
}




function toggleDrawerFilterRight() {
    if($("body > main > div >  nav:nth-of-type(2)").hasClass("open")) {
        $("body > main > div > nav:nth-of-type(2)").removeClass("open").slideUp();
    } else {
        $('body > main > div > nav:nth-of-type(2)').addClass("open").slideDown().css('display','flex');
    }
}


$(document).ready(function(){
    $("body > main > div > div > div:nth-of-type(1)").click(function(event){
        event.preventDefault();
        if($("body > main > div > nav:nth-of-type(2)").hasClass("open")){
            toggleDrawerFilterRight();
           
        }
        toggleDrawerFilterLeft();
    });

    $("body > main > div > div > div:nth-of-type(2)").click(function(event){
        event.preventDefault();
        if($("body > main > div > nav:nth-of-type(1)").hasClass("open")){
            toggleDrawerFilterLeft();
           
        }

        toggleDrawerFilterRight();
    });

    /*window.onresize = function(event){
        event.preventDefault();
        if($("body > header > form").is(":visible")) {
            closeSearch();
        }
    }*/
});