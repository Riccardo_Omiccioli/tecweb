$(document).ready(function(){

    var product = document.getElementById("tipoIn").value;
    if(product) {
        var text = $("#tipoIn > [value=" + product + "]")[0].innerHTML;
        text = text.replace(/ /g, "").toLowerCase();
        $("#" + text).show();
    }

    /*
    $('body > main > div > form:not(#' + text + '):not(#productForm)').hide();
    */

    $(document.getElementById("tipoIn")).change(function(event){
        event.preventDefault();
        var product = document.getElementById("tipoIn").value;
        var text = $("#tipoIn > [value=" + product + "]")[0].innerHTML;
        text = text.replace(/ /g, "").toLowerCase();
        $('body > main > div > form:not(#' + text + '):not(#productForm)').hide();
        $("#" + text).show();
    });

    $(document.getElementById("saveButton")).click(function(event){
        event.preventDefault();

        //var errorMessage = document.getElementById("errorMessage");
        //var successMessage = document.getElementById("successMessage");

        var sendParams = new FormData();
        var productParams = new Object();

        $("#productForm > input, #productForm > textarea, #productForm > select").each(function() {
            productParams[this.name] = this.value;
            console.log(this.name + ' : ' + this.value);
        });

        var product = document.getElementById("tipoIn").value;
        var text = $("#tipoIn > [value=" + product + "]")[0].innerHTML;
        text = text.replace(/ /g, "").toLowerCase();
        $("#" + text + " > input, #" + text + " > textarea, #" + text + " > select").each(function() {
            productParams[this.name] = this.value;
            console.log(this.name + ' : ' + this.value);
        });

        sendParams.set("product", JSON.stringify(productParams));
        var image = document.getElementById("immagineIn");
        if (image.files && image.files.length > 0) {
            sendParams.set("image", image.files[0]);
        }
        /*
        $("#" + tipo + " > input, #" + tipo + " > textarea, #" + tipo + " > select").each(function() {
            sendParams.set(this.name, this.value);
            console.log(this.name);
        });
        */

        var oReq = new XMLHttpRequest();
        oReq.open("POST", "/api/addProduct.php");
        oReq.send(sendParams);

        oReq.onreadystatechange = function() {
            if (this.readyState != 4) {
                return;
            }
            console.log(this.status);
            console.log(this.response);
            if (this.status == 200) {
                errorMessage.textContent = "";
                successMessage.textContent = "Impostazioni salvate";
            } else if(this.status == 400){
                errorMessage.textContent = "Errore: richiesta non valida";
            } else if(this.status == 401){
                errorMessage.textContent = "Errore: dati errati";
            } else if(this.status == 404){
                errorMessage.textContent = "Errore: URL non trovato";
            } else if(this.status == 500){
            } else {
            }
        }

    });

});
