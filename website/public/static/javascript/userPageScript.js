$(document).ready(function(){

    $(document.getElementById("showPassword")).click(function(event){
        event.preventDefault();
        if($(document.getElementById("passwordIn")).attr("type") == "password") {
            $(document.getElementById("passwordIn")).attr("type", "text");
            $(document.getElementById("newPasswordIn")).attr("type", "text");
            $(document.getElementById("newRePasswordIn")).attr("type", "text");
        } else {
            $(document.getElementById("passwordIn")).attr("type", "password");
            $(document.getElementById("newPasswordIn")).attr("type", "password");
            $(document.getElementById("newRePasswordIn")).attr("type", "password");
        }
    });

    $(document.getElementById("ordini")).click(function(event){
        event.preventDefault();
        window.location.href = "/orders.php";
    });

    $(document.getElementById("logout")).click(function(event){
        event.preventDefault();
        var oReq = new XMLHttpRequest();
        oReq.open("POST", "/api/logout.php");
        oReq.send();

        oReq.onreadystatechange = function() {
            if (this.readyState != 4) {
                return;
            }
            console.log(this.status);
            console.log(this.response);
            if (this.status == 200) {
                window.location.href = "/";
            } else if(this.status == 400){
                console.log("Error 400: User not logged in ?")
            } else if(this.status == 500){

            } else {

            }
        }
    });

    $(document.getElementById("addProduct")).click(function(event){
        event.preventDefault();
        window.location.href = "/addProduct.php";
    });

    $(document.getElementById("save")).click(function(event){
        event.preventDefault();

        var errorMessage = document.getElementById("errorMessage");
        var successMessage = document.getElementById("successMessage");

        var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/;
        var regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;

        var name = document.getElementById("nomeIn").value;
        var address = document.getElementById("indirizzoIn").value;
        var streetNumber = document.getElementById("civicoIn").value;
        var city = document.getElementById("cittaIn").value;
        var cap = document.getElementById("CAPIn").value;
        var note = document.getElementById("noteIn").value;
        var email = document.getElementById("emailIn").value;
        var password = document.getElementById("passwordIn").value;
        var newPassword = document.getElementById("newPasswordIn").value;
        var newRePassword = document.getElementById("newRePasswordIn").value;
        if (!email.match(regexEmail)) {
            errorMessage.textContent = "Inserire un indirizzo email valido";
	} else if (!password.match(regexPassword)) {
            errorMessage.textContent = "Inserire la propria password attuale";
        } else if (email.match(regexEmail) && password.match(regexPassword)) {
        var sendParams = new URLSearchParams();
            sendParams.append('name', name);
            sendParams.append('address', address);
            sendParams.append('streetNumber', streetNumber);
            sendParams.append('city', city);
            sendParams.append('cap', cap);
            sendParams.append('note', note);
            sendParams.append('email', email);
            sendParams.append('password', password);
            if (newPassword.match(regexPassword) && newPassword === newRePassword) {
                sendParams.append('newPassword', newPassword);
            }

            var oReq = new XMLHttpRequest();
            oReq.open("POST", "/api/saveUserSettings.php");
            oReq.send(sendParams);

            oReq.onreadystatechange = function() {
                if (this.readyState != 4) {
                    return;
                }
                console.log(this.status);
                console.log(this.response);
                if (this.status == 200) {
                    errorMessage.textContent = "";
                    successMessage.textContent = "Impostazioni salvate";
                    document.getElementById("passwordIn").value = "";
                    document.getElementById("newPasswordIn").value = "";
                    document.getElementById("newRePasswordIn").value = "";
                } else if(this.status == 400){
                    var error = "richiesta non valida";
                    try {
                        var response = JSON.parse(this.response);
                        error = response.error;
                    } catch (e) {
                    }
                    errorMessage.textContent = "Errore: " + error;
                } else if(this.status == 401){
                    errorMessage.textContent = "Errore: email o password errati";
                } else if(this.status == 500){

                } else {

                }
            }
        }
    });

});
