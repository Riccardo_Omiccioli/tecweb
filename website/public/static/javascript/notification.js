/*
function toggleDrawer(element) {
    if($("body > main > div > ul > li > p").hasClass("open")) {
        $("body > main > div > ul > li > p").removeClass("open").slideUp();
    } else {
        $('body > main > div > ul > li > p').addClass("open").slideDown();
    }
}
*/

$(document).ready(function(){
    $("#readAll").click(function(event){

        var notifReadAllReq = new XMLHttpRequest();
        notifReadAllReq.open("GET", "/api/readAllNotifications.php");
        notifReadAllReq.send();
        notifReadAllReq.onreadystatechange = function() {
            if (this.readyState != 4){
                return;
            }
            if (this.status == 200){
                setNotifNumber(0);
                window.setTimeout(function(){
                    window.location.reload();
                    return false;
                }, 500);
            } else if(this.status == 401){
                window.location.href = "/login.php";
                return false;
                //utente non loggato
            } else {
                window.location.href = "/index.php";
                return false;
                console.log(500);
            }        
        }

    });

    $("body > main > div > ul > li > p").click(function(event){
        event.preventDefault();
        if( $(this).next().next().hasClass("open") ){
            $(this).next().next().removeClass("open").slideUp();
        } else {
            $(this).next().next().addClass("open").slideDown();
            //chiamata ajax per aggiornare notifica

            var notifReadReq = new XMLHttpRequest();
            var sendParams = new URLSearchParams();
            sendParams.append('notifica', $(this).attr("id"));

            notifReadReq.open("GET", "/api/updateNotification.php?"+sendParams.toString());
            notifReadReq.send(/*sendParams*/);
            $(this).removeClass("unread");
            notifReadReq.onreadystatechange = function() {
                if (this.readyState != 4){
                    return;
                }
                if (this.status == 200){
                    setNotifNumber(-1);
                } else if(this.status == 204){
                    console.log("notifica già letta");
                    /* sembrava una buona idea
                    window.setTimeout(function(){
                        window.location.reload();
                        return false;
                    }, 1500);
                    */

                } else if(this.status == 401){
                    window.location.href = "/login.php";
                    return false;
                    
                } else {
                    console.log(500);
                    window.location.href = "/index.php";
                    return false;
                }
            }
        }
    });
});

/*
    $("body > main > div > div > div:nth-of-type(2)").click(function(event){
        event.preventDefault();
        if($("body > main > div > nav:nth-of-type(1)").hasClass("open")){
            toggleDrawerFilterLeft();
        }
        toggleDrawerFilterRight();
    });

    window.onresize = function(event){
        event.preventDefault();
        if($("body > header > form").is(":visible")) {
            closeSearch();
        }
    }
*/
