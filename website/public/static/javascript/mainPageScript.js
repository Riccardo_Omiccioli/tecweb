function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    // because unescape has been deprecated, replaced with decodeURI
    //return unescape(dc.substring(begin + prefix.length, end));
    return decodeURI(dc.substring(begin + prefix.length, end));
}

function toggleDrawer() {
    if($("body > main > aside").hasClass("open")) {
        $("body > main > aside").removeClass("open").animate({left: "-=40%"}, 300, "linear", console.log("closing drawer"));
    } else {
        $('body > main > aside').addClass("open").animate({left: "+=40%"}, 300, "linear", console.log("opening drawer"));
    }
}

function closeSearch() {
    if($("body > main > form").hasClass("open")) {
        $("body > main > form").removeClass("open").hide();
    }
}

function toggleSearch() {
    if($("body > main > form").hasClass("open")) {
        $("body > main > form").removeClass("open").slideUp();
    } else {
        $('body > main > form').addClass("open").slideDown();
    }
}

function setCartNumber(number) {
    var svgObject = document.getElementById('cart').contentDocument;
    if( svgObject === null) return false;
    var svg = svgObject.getElementById('cartNumber');
    if(svg === null) return false;
    if(number > 99) {
        svg.textContent = "..";
        svg.setAttribute("x", "4200px");
    } else if(number > 9) {
        svg.textContent = number;
        svg.setAttribute("x", "4165px");
    } else {
        svg.textContent = number;
        svg.setAttribute("x", "4200px");
    }
    return true;
}

function setNotifNumber(number) {
    var svgObject = document.getElementById('notification').contentDocument;
    if( svgObject === null ) return false;
    var svg = svgObject.getElementById('notificationNumber');
    if(svg === null) return false;
    if( parseInt(number) < 0) {
        //con questo if posso passare numeri negativi per diminuire le notifiche
        number = parseInt(parseInt(svg.textContent) + parseInt(number));
    }
    if(number > 99) {
        svg.textContent = "..";
        svg.setAttribute("x", "4200px");
    } else if(number > 9) {
        svg.textContent = number;
        svg.setAttribute("x", "4165px");
    } else {
        svg.textContent = number;
        svg.setAttribute("x", "4200px");
    }
    return true;
}

//clearInterval(intervalId); 
function increaseCartNumber() {
    var svgObject = document.getElementById('cart').contentDocument;
    var svg = svgObject.getElementById('cartNumber');
    if(svg.textContent != "..") {
        svg.textContent = parseInt(svg.textContent) + 1;
    }
    if(parseInt(svg.textContent) > 9 && parseInt(svg.textContent) < 99) {
        svg.setAttribute("x", "4165px");
    } else if(parseInt(svg.textContent) > 99) {
        svg.textContent = "..";
        svg.setAttribute("x", "4200px");
    } else {
        svg.setAttribute("x", "4200px");
    }
}

function shoppingCartNumberCalc(){
    var oldCart = 0;
    if(getCookie("cart") === null || JSON.parse(getCookie("cart")) === {} ){
        oldCart = 0;
    } else {
        Object.keys(JSON.parse(getCookie("cart"))).forEach(function(key, index) {
            oldCart = oldCart + parseInt(JSON.parse(getCookie("cart"))[key]);
        }, JSON.parse(getCookie("cart")) );
    }
    return oldCart;
}

function print_on_loading(){
    var perfEntries = performance.getEntriesByType("navigation");
    for (var i=0; i < perfEntries.length; i++) {
        var p = perfEntries[i];
    }

    var c_number = shoppingCartNumberCalc();
    return setCartNumber(c_number);
}

$(document).ready(function(){

    //chiama una funzione ogni 5 secondi

/***************************************************************************************
    console.info(performance.navigation.type);
if (performance.navigation.type == performance.navigation.TYPE_RELOAD) {
  console.info( "This page is reloaded" );
}
    if(getCookie("notify") === null || getCookie("notify") === undefined){
        /*non deve far niente se perché di base sta a 0
        if(JSON.parse(getCookie("notify"))["cart_number"] === undefined){
            var cartNumberCookieCopy = {};
            cartNumberCookieCopy = JSON.parse(getCookie("notify"));
            cartNumberCookieCopy["cart_number"] = 0;
            document.cookie = "notify=" + JSON.stringify(cartNumberCookieCopy); 
        }
    } else {
        var cartNumberCookieCopy = {};
        cartNumberCookieCopy = JSON.parse(getCookie("notify"));
        var cart_number = parseInt(cartNumberCookieCopy["cart_number"]);
        setCartNumber(cart_number);
    }
********************************************/
    $(document.getElementById('menu')).click(function(event){
        event.preventDefault();
        toggleDrawer();
    });

    $(document.getElementById('search')).click(function(event){
        event.preventDefault();
        toggleSearch();
    });

    window.addEventListener("keydown", function (event) {
        if (event.defaultPrevented) {
          return;
        }
        if (event.key == "Enter") {
            var searchString = null;
            if (this.window.innerWidth < 768) {
                if (document.getElementById('search_m') === document.activeElement && document.getElementById('search_m').value != "") {
                    searchString = document.getElementById('search_m').value;
                }
            } else {
                if (document.getElementById('search_d') === document.activeElement && document.getElementById('search_d').value != "") {

                    searchString = document.getElementById('search_d').value;
                }
            }
            if (searchString !== null) {
                var sendParams = new URLSearchParams();
                sendParams.append('search', searchString);
                window.location.href = "/productList.php?" + sendParams.toString();
            }
        } else if (event.key == "`") {
            console.log("search_m: " + document.getElementById('search_m').value);
            console.log("Search_d: " + document.getElementById('search_d').value);
        } else {
            return;
        }
        event.preventDefault();
    }, true);

    window.onresize = function(event){
        event.preventDefault();
        if($("body > header > form").is(":visible")) {
            closeSearch();
        }
    }

    var intervalId = window.setInterval(function(){
        if (print_on_loading() ) clearInterval(intervalId);
    }, 10);

    var intervalFirstNotification = window.setTimeout(function(){
            var notificationReq = new XMLHttpRequest();
            notificationReq.open("GET", "/api/getNotificationCount.php");
            notificationReq.send(/*sendParams*/);
            notificationReq.onreadystatechange = function() {
                if (this.readyState != 4){
                    return;
                }
                if (this.status == 200){
                    var notifRetValue = JSON.parse(notificationReq.response).count;
                    /*
                        ore l'unica cosa che manca è che anziche increaseCartNumber deve esserci 
                        una funzione per incrementare il numero sui messaggini (e devo usare set anziché increase)
                    */
                    setNotifNumber(notifRetValue);
                } else if(this.status == 204){
                    //TODO prodotto esaurito da stampare
                    setNotifNumber(0);
                    //TODO ricaricare la pagina dopo un tot secondi
                } else if(this.status == 401){
                    setNotifNumber(0);
                    //TODO utente non loggato
                } else {
                    setNotifNumber(0);
                }
            }

    }, 400);

    var intervalNotification = window.setInterval(function(){
        var notificationReq = new XMLHttpRequest();
        notificationReq.open("GET", "/api/getNotificationCount.php");
        notificationReq.send(/*sendParams*/);
        notificationReq.onreadystatechange = function() {
            if (this.readyState != 4){
                return;
            }
            if (this.status == 200){
                var notifRetValue = JSON.parse(notificationReq.response).count;
                /*
                    ore l'unica cosa che manca è che anziche increaseCartNumber deve esserci 
                    una funzione per incrementare il numero sui messaggini (e devo usare set anziché increase)
                */
                setNotifNumber(notifRetValue);
            } else if(this.status == 204){
                //TODO prodotto esaurito da stampare
                setNotifNumber(0);
                //TODO ricaricare la pagina dopo un tot secondi
            } else if(this.status == 401){
                setNotifNumber(0);
                //TODO utente non loggato
            } else {
                setNotifNumber(0);
            }
        }
    }, 5000);

});