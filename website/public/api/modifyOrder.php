<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/../private/path.php");
use \Util as Util;
use \Database\Products\Prodotto as Prodotto;
use \Database\Ordine as Ordine;
use \Database\Notifica as Notifica;
use \Database\Utente as Utente;
use \Database\Indirizzo as Indirizzo;
use \Database\DatabaseWriter as DatabaseWriter;
session_start();

if (!isset($_GET["idOrder"]) || !isset($_GET["status"])) {
	//TODO: proper 400
	http_response_code(400);
	echo "{err: 'Missing arguments'}";
	exit();
}
$order = $_GET['idOrder'];
$status = $_GET['status'];

//TODO STO FACENDO TUTTO SENZA MEZZO CONTROLLO -----NON MI RITENGO RESPONSABILE SE CONTINUATE A USARE SENZA SISTEMARE

$toModifyOrder = Ordine::get($order);

$toModifyOrder->setStatus($status);
if ($status === "Spedito") {
    $toModifyOrder->setShippingDate("now");
} else if ($status === "Consegnato") {
    $toModifyOrder->setDeliveryDate("now");
}
$db = DatabaseWriter::get();
$db->superTransaction();
$toModifyOrder->save();

$shortOrderID = substr($order, 0, 8);
$productsString = "";
foreach ($toModifyOrder->getProducts() as $row) {
	$quantity = $row[1];
	$productsString = $productsString." ".$quantity."×".$row[0]->getName().",";
}
$status = strtolower($status);
(new Notifica(null, "Ordine $shortOrderID $status", "Il tuo ordine <a href='/orders.php#$order'>$order</a> che contiente:".$productsString." è stato $status", $toModifyOrder->getCustomer()->getID(), true, "now"))->save();

$db->superCommit();


http_response_code(200);
die();

?>
