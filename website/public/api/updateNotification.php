<?php
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use \Database\Utente as Utente;
use \Database\Notifica as Notifica;

session_start();

if(!$_GET['notifica']){
	http_response_code(401);
	exit();
}

$notifID = $_GET['notifica'];

if (!isset($_SESSION['user'])) {
	http_response_code(401);
	exit();
}

$notificationOfUser = Notifica::get($notifID);

if (!$notificationOfUser) {
	http_response_code(404);
	exit();
}
if(!$notificationOfUser->isUnread()){
	http_response_code(204);
	exit();
}
$notificationOfUser->setNew(false);

$notificationOfUser->save();
http_response_code(200);

?>
