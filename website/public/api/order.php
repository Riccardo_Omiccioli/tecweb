<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/../private/path.php");

use \Util as Util;
use \Database\Products\Prodotto as Prodotto;
use \Database\Ordine as Ordine;
use \Database\Notifica as Notifica;
use \Database\Utente as Utente;
use \Database\Indirizzo as Indirizzo;
use \Database\DatabaseWriter as DatabaseWriter;
session_start();
if (!isset($_SESSION["user"]) ) {
	http_response_code(401);
	exit();
}

if(!$_POST['cart']){
	http_response_code(500);
	exit();
}

try {
	$utonto = Utente::get($_SESSION["user"]);
} catch (Exception $e) {
	http_response_code(500);
	error_log($e);
	exit();
}
try {
    if ($utonto->getSellerID() !== null) {
        http_response_code(403);
        echo json_encode(["redirect" => "/", "error" => "Un account dipendente non può acquistare prodotti"]);
        exit();
    }
	$indir = Indirizzo::getUserAddress($utonto);
	$nome = $utonto->getName();
	if($indir === null || $nome === null){
		http_response_code(400);
		echo '{"redirect":"/user.php", "error":"Inserire nome ed indirizzo"}';
		exit();
	}
	$name = $utonto->getCustomerID();
	if(!$name){
		http_response_code(204);
		exit();
	}
	$postOrder = new Ordine(null, $name, $indir->getID(),"In elaborazione","now" , null, null);
} catch (Exception $e) {
	http_response_code(500);
	error_log($e);
	exit();
}
try {
	$cookieArrayDecoded = json_decode( $_POST['cart']);
} catch (Exception $e) {
	http_response_code(500);
	error_log($e);
	exit();
}
$setParray = [];

foreach ($cookieArrayDecoded as $key => $value) {
	try {
		$localPr = Prodotto::get($key)[0];
		array_push($setParray, [$localPr, $value,($localPr->getPrice())*(1-$localPr->getDiscount()) ]);
	} catch (Exception $e) {
		http_response_code(500);
		error_log($e);
		exit();
	}
}


try {
	$postOrder->setProducts($setParray);
} catch (Exception $e) {
	http_response_code(500);
	error_log($e);
	exit();
}


try {
	$db = DatabaseWriter::get();
	$db->superTransaction();
    $orderID = $postOrder->save();
    $shortOrderID = substr($orderID, 0, 8);
	$productsString = "";
	foreach ($postOrder->getProducts() as $row) {
		$availability = $row[0]->getQuantity();
		$quantity = $row[1];
		$availability -= $quantity;
		$row[0]->setQuantity($availability);
		$row[0]->save();
		if ($availability == 0) {
			$name = $row[0]->getName();
			$id = $row[0]->getID();
			(new Notifica(null, "Prodotto $name in esaurimento", "Il prodotto <a href='/productPage.php?product=$id'>$name ($id)</a> è esaurito", null, true, "now"))->save();
		}
		$productsString = $productsString." ".$quantity."×".$row[0]->getName().",";
	}
	(new Notifica(null, "Ordine $shortOrderID in elaborazione", "Il tuo ordine <a href='/orders.php#$orderID'>$orderID</a> che contiente:".$productsString." è in elaborazione", $_SESSION['user'], true, "now"))->save();
	(new Notifica(null, "Nuovo ordine $shortOrderID", "Un ordine <a href='/orders.php#$orderID'>$orderID</a> è stato effettuato", null, true, "now"))->save();
	$db->superCommit();
} catch (Exception $e) {
	$db->rollback();
	http_response_code(500);
	error_log($e);
	exit();
}
http_response_code(200);
echo json_encode(["orderID" => $orderID]);
exit();

?>
