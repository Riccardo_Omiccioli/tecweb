<?php

require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\Utente as Utente;

if (!isset($_POST["email"]) || !isset($_POST["password"])) {
	http_response_code(400);
	exit();
}

$result = null;
try {
    $user = new Utente(null, null, $_POST["email"], password_hash($_POST["password"], PASSWORD_DEFAULT), null, null, null);
	$result = $user->save();
} catch (Exception $e) {
	http_response_code(500);
	exit();
}
if ($result !== null) {
    session_start();
    $_SESSION["user"] = $result;
    http_response_code(200);
    exit();
} else {
	http_response_code(401);
	exit();
}

http_response_code(500);
die();

?>
