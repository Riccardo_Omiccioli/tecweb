<?php
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use \Database\Utente as Utente;
use \Database\Notifica as Notifica;

session_start();

if (!isset($_SESSION['user'])) {
	http_response_code(401);
	exit();
}

$user = Utente::get($_SESSION['user']);
if ($user !== null) {
    Notifica::readAll($user);
}

http_response_code(200);

?>
