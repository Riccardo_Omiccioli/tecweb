<?php
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use \Database\Utente as Utente;
use \Database\Notifica as Notifica;

session_start();
$rand = \Util::uuid();
if (!isset($_SESSION['user'])) {
    http_response_code(401);
	exit();
}
$userID = $_SESSION['user'];
$user = Utente::get($userID);
if ($user === null) {
    session_destroy();
    http_response_code(500);
    die();
}
$count = 0;
if ($user->getSellerID() !== null) {
    $count = Notifica::getSellerUnreadNotificationCount();
} else {
    $count = Notifica::getUserUnreadNotificationCount($user);
}

if ($count == 0) {
	http_response_code(204);
	exit();
}

http_response_code(200);
echo "{\"count\": $count}";

?>
