<?php

session_start();
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use \Database\Utente as Utente;
use \Database\Indirizzo as Indirizzo;

if(!isset($_POST["email"]) || !isset($_POST["password"])) {
	http_response_code(400);
	exit();
}

$result = null;
try {
	$id = Utente::login($_POST["email"], $_POST["password"]);
    if($id !== null) {
		$user = Utente::get($id);
		$user->setName($_POST["name"]);
		$user->setEmail($_POST["email"]);
		if(isset($_POST["newPassword"])) {
			$user->setPassword(password_hash($_POST["newPassword"], PASSWORD_DEFAULT));
		}
		$result = $user->save();

		if ($_POST["address"] !== "" || $_POST["streetNumber"] !== "" || $_POST["city"] !== "" || $_POST["cap"] !== "") {
			if (!isset($_POST["address"], $_POST["streetNumber"], $_POST["city"], $_POST["cap"]) ||
				$_POST["address"] === "" || $_POST["streetNumber"] === "" || $_POST["city"] === "" || $_POST["cap"] === "") {
				http_response_code(400);
				echo '{"error":"Bisogna impostare tutti i campi dell\'indirizzo."}';
				exit();
			}
			$notes = ($_POST["note"] === "") ? (null) : ($_POST["note"]);
			try {
				$address = Indirizzo::getUserAddress($user) ?? new Indirizzo(null, $user->getCustomerID(), $_POST["address"], $_POST["streetNumber"], $_POST["city"], $_POST["cap"], $notes);
				$address->setAddress($_POST["address"]);
				$address->setStreetNumber($_POST["streetNumber"]);
				$address->setCity($_POST["city"]);
				$address->setZipCode($_POST["cap"]);
				$address->setNotes($notes);
			} catch (Exception $e) {
				http_response_code(400);
				echo json_encode(["error" => $e->getMessage()]);
				exit();
			}
			$address->save();
		}

		http_response_code(200);
		exit();
	}
} catch (Exception $e) {
	http_response_code(500);
	exit();
}
if ($result !== null) {
    http_response_code(200);
    exit();
} else {
	http_response_code(401);
	exit();
}

http_response_code(500);
die();

?>
