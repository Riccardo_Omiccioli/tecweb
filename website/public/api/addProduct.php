<?php

session_start();
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\Products\Prodotto as Prodotto;

if(!isset($_POST["product"])) {
	http_response_code(400);
	exit();
}

$result = null;
try { 
    $productData = (array)json_decode($_POST["product"]);
    unset($productData["image"]);
    if (!$productData["ID"]) {
        $productData["ID"] = null;
    }
    if (empty($_FILES["image"]) && $productData["ID"] === null) {
        http_response_code(400);
        echo json_encode(["error" => "Allegare un'immagine"]);
        exit();
    }
    $productData["date"] = "now";
    $type = $productData["type"];
    $className = Prodotto::getProductClassName($type);
    $class = "\\Database\\Products\\$className";
    $product = new $class(...$productData);
    if (!empty($_FILES["image"])) {
        $tmpName = $_FILES["image"]["tmp_name"];
        $check = getimagesize($tmpName);
        if ($check === false) {
            http_response_code(400);
            echo json_encode(["error" => "Immagine non valida"]);
            exit();
        }
        $size = max($check[0], $check[1]);
        $offsetX = round(($size - $check[0])/2);
        $offsetY = round(($size - $check[1])/2);
        $ext = "";
        $image;
        switch($check[2]) {
        case IMAGETYPE_JPEG:
            $ext = "jpg";
            $image = imagecreatefromjpeg($tmpName);
            break;
        case IMAGETYPE_PNG:
            $ext = "png";
            $image = imagecreatefrompng($tmpName);
            break;
        default:
            http_response_code(400);
            echo json_encode(["error" => "Immagine non valida"]);
            exit();
        }
        $newImage = imagecreatetruecolor($size, $size);
        imagefill($newImage, 0, 0, imagecolorallocate($image, 255, 255, 255));
        imagecopy($newImage, $image, $offsetX, $offsetY, 0, 0, $check[0], $check[1]);
        $imgName = Util::uuid() . ".jpg";
        imagejpeg($newImage, PROJECT_ROOT . "/public/" . PRODUCTIMAGE . $imgName);
        $product->setImages([$imgName]);
    }
    $product->setID($productData["ID"]);
    $result = $product->save();
} catch (Exception $e) {
    echo $e;
	http_response_code(500);
	exit();
}
if ($result !== null) {
    http_response_code(200);
    exit();
} else {
	http_response_code(401);
	exit();
}

http_response_code(500);
die();

?>
