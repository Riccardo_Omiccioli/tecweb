<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/../private/path.php");

use \Util as Util;

use \Database\Products\Prodotto as Prodotto;

if (!isset($_GET["product"]) || !isset($_GET["quantity"])) {
	//TODO: proper 400
	http_response_code(400);
	echo "{err: 'Missing arguments'}";
	exit();
}

$product = $_GET['product'];
$quantity = $_GET['quantity'];

if (!is_numeric($quantity) || intval($quantity) === 0) {
	http_response_code(400);
	echo "{err: 'Invalid quantity'}";
	exit();
}

if (!Util::isUuid($product)) {
	http_response_code(400);
	echo "{err: 'Invalid product ID'}";
	exit();
}

try {
	$product = Prodotto::get($product)[0];
} catch (Exception $e) {
	http_response_code(404);
	echo "{err: 'Product not found'}";
	exit();
}

$availability = $product->getQuantity();

if ($availability >= $quantity) {
	http_response_code(200);
	echo "{status: 'available'}";
	exit();
} else {
	http_response_code(204);
	exit();
}

http_response_code(500);
die();
?>
