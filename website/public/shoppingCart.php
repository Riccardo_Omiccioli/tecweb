<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."shoppingCart.php";
    $params["title"] = "Carrello";
    $params["css"] = ["mainStyleSheet.css", "shoppingCartSheet.css"];
	$params["scriptjs"] = [ "mainPageScript.js" , "shoppingCartScript.js"];
    require_once(TEMPLATE."base.php");
?>
