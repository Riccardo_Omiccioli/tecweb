<?php
    use Database\Utente as Utente;

    require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");

    if(!isset($_SESSION)) {
        session_start();
    }
    if (isset($_SESSION["user"])){
		$user = Database\Utente::get($_SESSION["user"]);
        if($user->getSellerID($user->getID())) {
            $params["page"] = PAGE."addProduct.php";
            $params["title"] = "Aggiungi prodotto";
            $params["css"] = ["mainStyleSheet.css", "addProductSheet.css"];
            $params["scriptjs"] = [ "mainPageScript.js" , "addProductPageScript.js"];
            require_once(TEMPLATE."base.php");
		}
    }
?>
