<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."vendorNotifications.php";
    $params["title"] = "Notifications (vendor)";
    $params["css"] = ["mainStyleSheet.css", "vendorNotifications.css"];
	$params["scriptjs"] = [ "mainPageScript.js" , "notifications.js"];
    require_once(TEMPLATE."base.php");
?>
