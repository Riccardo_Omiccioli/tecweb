<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
	if(!isset($_SESSION)) {
        session_start();
    }
    if (isset($_SESSION["user"])){
		$user = Database\Utente::get($_SESSION["user"]);
        if($user->getSellerID($user->getID())) {
		    $params["page"] = PAGE."notificationVendor.php";
		    $params["title"] = "Notifiche Venditore";
		    $params["css"] = ["mainStyleSheet.css", "notificationsSheet.css"];
			$params["scriptjs"] = [ "mainPageScript.js" , "notification.js"];
		    require_once(TEMPLATE."base.php");
		} else {
		    $params["page"] = PAGE."notification.php";
		    $params["title"] = "Notifiche";
		    $params["css"] = ["mainStyleSheet.css", "notificationsSheet.css"];
			$params["scriptjs"] = [ "mainPageScript.js" , "notification.js"];
		    require_once(TEMPLATE."base.php");
    	}
	} else {
        header("location: login.php");
        exit;
	}
?>
