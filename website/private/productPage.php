<?php
    use Database\Utente as Utente;

    if (!isset($_SESSION)) {
		session_start();
	}
    if (isset($_SESSION["user"])){
		$user = Database\Utente::get($_SESSION["user"]);
    }
?>

<div>
	<img src="<?php echo PRODUCTIMAGE.$params["product"]->getImages()[0];?>" alt="immagine prodotto"/>
	<div>
		<section>
			<h1><?php echo $params["product"]->getName();?></h1>
			<p><?php foreach($params["product"]->getSummarySpecs() as $specs) {
				echo "$specs[0]: $specs[1]<br/>";
			}?></p>
			<p <?php if($params["product"]->getQuantity() == 0){echo "class=\"unavailable\"";}
				else {echo "class=\"available\"";}?>>Disponibili: <?php echo $params["product"]->getQuantity();?></p>
			<p>
				<span><?php if($params["product"]->getDiscount() != 0) {
						echo number_format($params["product"]->getPrice()/100, 2)."€";
					}?></span>
				<span><?php if($params["product"]->getDiscount() != 0) {
						echo "-".($params["product"]->getDiscount()*100)."%";
					}?></span>
				<span><?php echo number_format($params["product"]->getPrice()/100*(1-$params["product"]->getDiscount()), 2)."€";?></span>
	    	</p>
	    </section>
	    <div>
	    	<div>
				<button id="-">
					<img src="<?php echo ICON;?>subtract.svg" alt="-"/>
				</button>
				<input type="number" min="0" id="quantity" value="1">
	    		<button id="+">
					<img src="<?php echo ICON;?>add.svg" alt="+"/>
				</button>
			</div>

			<button id="carrello">
				Aggiungi al carrello
				<img src="<?php echo ICON;?>addToCart.svg" alt="shop"/>
			</button>
		</div>
		<p class="unavailable">Purtroppo il prodotto non è disponibile</p>
	</div>
	<section>
		<h2>Specifiche</h2>
		<table>
			<?php foreach($params["product"]->getSpecs() as $specs) {
				echo "<tr><th>$specs[0]</th><td>$specs[1]</td></tr>";
			}?>
		</table>
	</section>
	<?php
		if($params["product"]->getDescription() !== null) {
			echo "<section>
					<h2>Descrizione</h2>
					<p>".$params["product"]->getDescription()."</p>
				</section>";
		}
	?>
	<?php
		if(isset($user) && $user->getSellerID($user->getID())) {
			echo "<button id=\"modifica\">modifica prodotto</button>";
		}
	?>
</div>