<?php
	if(!isset($_SESSION)) {
		session_start();
	}
	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
	//require_once($_SERVER['DOCUMENT_ROOT']."/..".UTILS."functions.php")

?>

<!DOCTYPE html>
<html lang="it">
	<head>
		<meta charset="UTF-8"/>
		<meta name="description" content="Hardware2Unbox online shop site"/>
		<meta name="keywords" content="Computer, PC, shop"/>
		<meta name="author" content="Bragari Alexandru, Omiccioli Riccardo, Orlando Nicola"/>
		<base href="reference"/>
		<?php foreach ($params["css"] as $cssFile) : ?>
			<link type="text/css" rel="stylesheet" href="<?php echo CSS.$cssFile; ?>"/>
		<?php endforeach; ?>
		<title>
			Hardware2Unbox - <?php echo $params["title"];?>
		</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<?php foreach ($params["scriptjs"] as $jsFile) : ?>
			<script src="<?php echo JAVASCRIPT.$jsFile; ?>"></script>
		<?php endforeach; ?>
	</head>
	<body>
		<header>
			<div id="menu">
				<object data="<?php echo ICON;?>menu.svg" type="image/svg+xml">menu</object>
			</div>
            <a id="logo" href="/">
				<object id="logoObject" data="<?php echo ICON;?>logo.svg" type="image/svg+xml">logo</object>
			</a>
			<form>
				<input type="text" id="search_d" name="search" <?php if(isset($_GET["search"])){echo 'value="'.$_GET["search"].'"';} ?>><br><br>
			</form>
			<a id="account" href="user.php">
				<object id="accountObject" data="<?php echo ICON;?>account.svg" type="image/svg+xml">account</object>
			</a>
			<a href="notification.php">
				<object id="notification" data="<?php echo ICON;?>notification.svg" type="image/svg+xml">notifica</object>
			</a>
			<a href="shoppingCart.php">
				<object id="cart" data="<?php echo ICON;?>cart.svg" type="image/svg+xml">carrello</object>
			</a>
			<div id="search">
				<object data="<?php echo ICON;?>search.svg" type="image/svg+xml">cerca</object>
			</div>
		</header>
		<main>
			<form>
				<input type="text" id="search_m" name="search" <?php if(isset($_GET["search"])){echo 'value="'.$_GET["search"].'"';} ?>><br><br>
			</form>
			<aside>
				<ul>
					<?php foreach(Database\Products\Prodotto::getCategoryLinks() as $categoria): ?> 
						<li><a href="/productList.php?category=<?php echo $categoria["link"]; ?>" ><?php echo $categoria["name"]; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</aside>
			<?php require($params["page"]);?>
		</main>
		<footer>
			<p>info@hardware2unbox.shop (+39)0001122333 <a href="privacy.php">Privacy policy</a></p>
		</footer>
	</body>
</html>
