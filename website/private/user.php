<?php
use \Database\Utente as Utente;
use \Database\Indirizzo as Indirizzo;
    if (!isset($_SESSION)) {
		session_start();
	}
    if (isset($_SESSION["user"])){
		$user = Utente::get($_SESSION["user"]);
    }
    $address = Indirizzo::getUserAddress($user);
    $street = "";
    $number = "";
    $city = "";
    $zip = "";
    $notes = "";
    if ($address !== null) {
        $street = $address->getAddress();
        $number = $address->getStreetNumber();
        $city =   $address->getCity();
        $zip =    $address->getZipCode();
        $notes =  $address->getNotes() ?? "";
    }
?>

<div>
	<div>
		<button id="ordini">Ordini</button>
		<button id="logout">Logout</button>
		<?php
			if (isset($user) && $user->getSellerID($user->getID())) {
				echo "<button id=\"addProduct\">Aggiungi prodotto</button>";
			}
		?>
	</div>
    <h1>Modifica informazioni account</h1>
    <form id="userForm">
		<label for="nomeIn">nome</label>
		<input type="text" id="nomeIn" name="nome" value="<?php echo $user->getName()?>">
		<div>
			<div>
				<label for="indirizzoIn">indirizzo</label>
				<input type="text" id="indirizzoIn" name="indirizzo" value="<?php echo htmlspecialchars($street); ?>">
			</div>
        	<div>
				<label for="civicoIn">numero civico</label>
				<input type="text" id="civicoIn" name="civico" value="<?php echo htmlspecialchars($number); ?>">
			</div>
			<div>
				<label for="cittaIn">citta'</label>
				<input type="text" id="cittaIn" name="citta" value="<?php echo htmlspecialchars($city); ?>">
			</div>
			<div>
				<label for="CAPIn">CAP</label>
				<input type="text" id="CAPIn" name="CAP" value="<?php echo htmlspecialchars($zip); ?>">
			</div>
			<div>
				<label for="noteIn">note</label>
				<textarea id="noteIn" name="note" rows="3"><?php echo htmlspecialchars($notes); ?></textarea>
			</div>
		</div>
		<label for="emailIn">email</label>
        <input type="text" id="emailIn" name="email" value="<?php echo $user->getEmail()?>">
        <label for="passwordIn">Inserisci password attuale</label>
        <input type="password" id="passwordIn" name="password">
		<label for="newPasswordIn">Inserisci nuova password</label>
        <input type="password" id="newPasswordIn" name="newPassword">
		<label for="newRePasswordIn">Conferma nuova password</label>
        <input type="password" id="newRePasswordIn" name="newRePassword">
		<div>
			<button id="showPassword">Mostra password</button>
			<button id="save">Salva</button>
		</div>
		<p id="errorMessage"></p>
		<p id="successMessage"></p>
	</form>
</div>
