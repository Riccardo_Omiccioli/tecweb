<?php

class Util {

	const DATEFORMAT = "Y-m-d";
	const DATETIMEFORMAT = "Y-m-d H:i:s";
	public static function uuid() {
		$data = random_bytes(16);

		// Set version to 0100
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40);
		// Set bits 6-7 to 10
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80);

		// Output the 36 character UUID.
		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public static function dump($var) {
        ob_start();
        var_dump($var);
        $res = ob_get_contents();
        ob_end_clean();
        return $res;
    }

	public static function isUuid(String $str) {
		return preg_match("/[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}/", $str) === 1;
	}

	public static function formatSize(int $size, int $step, array $units) {
		$unit = 0;
		$nUnits = count($units);
		while ($size > $step && $unit < $nUnits-1) {
			$unit += 1;
			$size /= $step;
		}
		$size = round($size);
		return "$size $units[$unit]";
	}

}

?>
