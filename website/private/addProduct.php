<?php
    use Database\Products\FormFactorMobo as FormFactorMobo;
    use Database\Products\Prodotto as Prodotto;
    use Database\Products\Alimentatore as Alimentatore;
    use Database\Products\Chassis as Chassis;
    use Database\Products\Dissipatore as Dissipatore;
    use Database\Products\MemoriaDiMassa as MemoriaDiMassa;
    use Database\Products\Monitor as Monitor;
    use Database\Products\PastaTermica as PastaTermica;
    use Database\Products\Processore as Processore;
    use Database\Products\RAM as RAM;
    use Database\Products\SchedaEthernet as SchedaEthernet;
    use Database\Products\SchedaMadre as SchedaMadre;
    use Database\Products\SchedaVideo as SchedaVideo;
    use Database\Products\SchedaWifi as SchedaWifi;
    use Database\Products\Ventola as Ventola;

    if (isset($_GET['productID'])) {
        $product = Prodotto::get($_GET['productID'])[0];
        //var_dump($product->getDescription());
    }
    //value="<?php echo $product->getID();
?>

<div>
    <form id="productForm">
            <input type="hidden" id="idIn" name="ID" value="<?php if(isset($product)){echo $product->getID();}?>"><br>
            <label for="codiceIn">codice prodotto</label><br>
            <input type="text" id="codiceIn" name="code" value="<?php if(isset($product)){echo $product->getCode();}?>"><br>
            <label for="nomeIn">nome prodotto</label><br>
            <input type="text" id="nomeIn" name="name" value="<?php if(isset($product)){echo $product->getName();}?>"><br>
            <label for="descrizioneIn">descrizione prodotto</label><br>
            <textarea type="text" id="descrizioneIn" name="desc" rows="5"><?php if(isset($product)){echo $product->getDescription();}?></textarea><br>
            <label for="prezzoIn">prezzo prodotto</label><br>
            <input type="number" min="0" id="prezzoIn" name="price" value="<?php if(isset($product)){echo $product->getPrice();}?>"><br>
            <label for="scontoIn">sconto (%)</label><br>
            <input type="number" min="0" id="scontoIn" name="discount" value="<?php if(isset($product)){echo $product->getDiscount();}?>"><br>
            <label for="quantitaIn">quantita' in magazzino</label><br>
            <input type="number" min="0" id="quantitaIn" name="quantity" value="<?php if(isset($product)){echo $product->getQuantity();}?>"><br>
            <label for="coloreIn">colore prodotto</label><br>
            <input type="text" id="coloreIn" name="color" value="<?php if(isset($product)){echo $product->getColor();}?>"><br>
            <label for="tagIn">tag</label><br>
            <input type="text" id="tagIn" name="tag" value="<?php if(isset($product)){echo $product->getTag();}?>"><br>
            <label for="immagineIn">immagine</label><br>
            <input type="file" id="immagineIn" name="image" /><br>
            <label for="tipoIn">tipo prodotto</label><br>

            <select id="tipoIn" name="type">
                <option disabled <?php if(!isset($product)){echo "selected";}?> value></option>
                <option <?php if(isset($product) && $product->getType() == 1){echo "selected";}?> value="1">accessorio</option>
                <option <?php if(isset($product) && $product->getType() == 2){echo "selected";}?> value="2">alimentatore</option>
                <option <?php if(isset($product) && $product->getType() == 5){echo "selected";}?> value="5">cavo</option>
                <option <?php if(isset($product) && $product->getType() == 4){echo "selected";}?> value="4">chassis</option>
                <option <?php if(isset($product) && $product->getType() == 6){echo "selected";}?> value="6">dissipatore</option>
                <option <?php if(isset($product) && $product->getType() == 7){echo "selected";}?> value="7">memoria di massa</option>
                <option <?php if(isset($product) && $product->getType() == 8){echo "selected";}?> value="8">monitor</option>
                <option <?php if(isset($product) && $product->getType() == 10){echo "selected";}?> value="10">pasta termica</option>
                <option <?php if(isset($product) && $product->getType() == 3){echo "selected";}?> value="3">processore</option>
                <option <?php if(isset($product) && $product->getType() == 9){echo "selected";}?> value="9">RAM</option>
                <option <?php if(isset($product) && $product->getType() == 11){echo "selected";}?> value="11">scheda ethernet</option>
                <option <?php if(isset($product) && $product->getType() == 12){echo "selected";}?> value="12">scheda madre</option>
                <option <?php if(isset($product) && $product->getType() == 14){echo "selected";}?> value="14">scheda video</option>
                <option <?php if(isset($product) && $product->getType() == 15){echo "selected";}?> value="15">scheda WiFi</option>
                <option <?php if(isset($product) && $product->getType() == 16){echo "selected";}?> value="16">ventola</option>
            </select>
    </form>
    <form id="alimentatore">
        <label for="potenzaIn">potenza (W)</label><br>
        <input type="number" min="0" id="potenzaIn" name="power" value="<?php if(isset($product) && $product instanceof Alimentatore){echo $product->getPower();}?>"><br>
        <label for="efficienzaIn">classe efficienza</label><br>
        <select id="efficienzaIn" name="efficiency" selected="none">
            <option disabled selected value></option>
            <option value="standard">standard</option>
            <option value="plus">80+</option>
            <option value="bronze">80+ Bronze</option>
            <option value="silver">80+ Silver</option>
            <option value="gold">80+ Gold</option>
            <option value="platinum">80+ Platinum</option>
            <option value="titanium">80+ Titanium</option>
        </select>
        <label for="connettoriPCIEIn">numero connettori PCIE</label><br>
        <input type="number" min="0" id="connettoriPCIEIn" name="pcie" value="<?php if(isset($product) && $product instanceof Alimentatore){echo $product->getPCIeConnectors();}?>"><br>
        <label for="connettoriEPSIn">numero connettori EPS</label><br>
        <input type="number" min="0" id="connettoriEPSIn" name="eps" value="<?php if(isset($product) && $product instanceof Alimentatore){echo $product->getEPSConnectors();}?>"><br>
        <label for="connettoriPerifericheIn">numero connettori periferiche</label><br>
        <input type="number" min="0" id="connettoriPerifericheIn" name="misc" value="<?php if(isset($product) && $product instanceof Alimentatore){echo $product->getMiscConnectors();}?>"><br>
        <label for="profonditaAlimentatoreIn">profondita' (mm)</label><br>
        <input type="number" min="0" id="profonditaAlimentatoreIn" name="depth" value="<?php if(isset($product) && $product instanceof Alimentatore){echo $product->getDepth();}?>"><br>
    </form>
    <form id="chassis">
        <label for="formFactorChassisIn">form factor</label><br>
        <select id="formFactorChassisIn" name="formFactor" selected="none">
            <option disabled selected value></option>
            <?php
                foreach (FormFactorMobo::getAll() as $formFactor) {
                    $name = $formFactor->getName();
                    $value = $formFactor->getID();
                    echo "<option value=\"$value\">$name</option>";
                }
            ?>
        </select>
        <label for="lunghezzaIn">lunghezza (mm)</label><br>
        <input type="number" min="0" id="lunghezzaIn" name="length" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getLength();}?>"><br>
        <label for="larghezzaIn">larghezza (mm)</label><br>
        <input type="number" min="0" id="larghezzaIn" name="width" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getWidth();}?>"><br>
        <label for="altezzaIn">profondita' (mm)</label><br>
        <input type="number" min="0" id="altezzaIn" name="height" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getHeight();}?>"><br>
        <label for="slotVentoleIn">numero slot ventole</label><br>
        <input type="number" min="0" id="slotVentoleIn" name="fans" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getFans();}?>"><br>
        <label for="dissipatoreMaxIn">altezza dissipatore massima (mm)</label><br>
        <input type="number" min="0" id="dissipatoreMaxIn" name="coolerHeight" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getCoolerHeight();}?>"><br>
        <label for="profonditaAlimentatoreMaxIn">profondita' alimentatore massima (mm)</label><br>
        <input type="number" min="0" id="profonditaAlimentatoreMaxIn" name="psuDepth" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getPSUDepth();}?>"><br>
        <label for="lunghezzaGPUMaxIn">lunghezza scheda video massima (mm)</label><br>
        <input type="number" min="0" id="lunghezzaGPUMaxIn" name="gpulength" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getGPUlength();}?>"><br>
        <label for="slot525In">numero slot 5.25"</label><br>
        <input type="number" min="0" id="slot525In" name="slots525" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getSlots525();}?>"><br>
        <label for="slot35In">numero slot 3.5"</label><br>
        <input type="number" min="0" id="slot35In" name="slots35" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getSlots35();}?>"><br>
        <label for="slot25In">numero slot 2.5"</label><br>
        <input type="number" min="0" id="slot25In" name="slots25" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getSlots25();}?>"><br>
        <label for="slotPCIEIn">numero slot PCIE</label><br>
        <input type="number" min="0" id="slotPCIEIn" name="slotsPCIe" value="<?php if(isset($product) && $product instanceof Chassis){echo $product->getSlotsPCIe();}?>"><br>
    </form>
    <form id="dissipatore">
        <label for="TDPIn">TDP (W)</label><br>
        <input type="number" min="0" id="TDPIn" name="tdp" value="<?php if(isset($product) && $product instanceof Dissipatore){echo $product->getTDP();}?>"><br>
        <label for="numeroVentoleAriaIn">numero ventole installate</label><br>
        <input type="number" min="0" id="numeroVentoleAriaIn" name="fans" value="<?php if(isset($product) && $product instanceof Dissipatore){echo $product->getFans();}?>"><br>
        <label for="dimensioneVentoleAriaIn">dimensione ventole installate (mm)</label><br>
        <input type="number" min="0" id="dimensioneVentoleAriaIn" name="fanSize" value="<?php if(isset($product) && $product instanceof Dissipatore){echo $product->getFanSize();}?>"><br>
        <label for="altezzaDissipatoreIn">altezza dissipatore (mm)</label><br>
        <input type="number" min="0" id="altezzaDissipatoreIn" name="height" value="<?php if(isset($product) && $product instanceof Dissipatore){echo $product->getHeight();}?>"><br>
        <label for="altezzaRAMMAXIn">altezza massima RAM (mm)</label><br>
        <input type="number" min="0" id="altezzaRAMMAXIn" name="ramHeight" value="<?php if(isset($product) && $product instanceof Dissipatore){echo $product->getRamHeight();}?>"><br>
        <label for="lunghezzaRadiatoreIn">lunghezza radiatore (mm)</label><br>
        <input type="number" min="0" id="lunghezzaRadiatoreIn" name="radLength" value="<?php if(isset($product) && $product instanceof Dissipatore){echo $product->getRadLength();}?>"><br>
    </form>
    <form id="memoriadimassa">
        <label for="capacitaIn">capacita' (GB)</label><br>
        <input type="number" min="0" id="capacitaIn" name="capacity" value="<?php if(isset($product) && $product instanceof MemoriaDiMassa){echo $product->getCapacity();}?>"><br>
        <label for="dimensioneCacheIn">dimensione cache (MB)</label><br>
        <input type="number" min="0" id="dimensioneCacheIn" name="cacheSize" value="<?php if(isset($product) && $product instanceof MemoriaDiMassa){echo $product->getCacheSize();}?>"><br>
        <label for="velocitaMassimaScritturaIn">velocita' scrittura massima (MB/s)</label><br>
        <input type="number" min="0" id="velocitaMassimaScritturaIn" name="maxWriteSpeed" value="<?php if(isset($product) && $product instanceof MemoriaDiMassa){echo $product->getMaxWriteSpeed();}?>"><br>
        <label for="velocitaMassimaLetturaIn">velocita' lettura massima (MB/s)</label><br>
        <input type="number" min="0" id="velocitaMassimaLetturaIn" name="maxReadSpeed" value="<?php if(isset($product) && $product instanceof MemoriaDiMassa){echo $product->getMaxWriteSpeed();}?>"><br>
        <label for="interfacciaIn">interfaccia</label><br>
        <input type="text" id="interfacciaIn" name="interface" value="<?php if(isset($product) && $product instanceof MemoriaDiMassa){echo $product->getInterface();}?>"><br>
        <label for="consumoIn">consumo (W)</label><br>
        <input type="number" min="0" id="consumoIn" name="powerConsumption" value="<?php if(isset($product) && $product instanceof MemoriaDiMassa){echo $product->getPowerConsumption();}?>"><br>
        <label for="formFactorIn">form factor</label><br>
        <input type="text" id="formFactorIn" name="formFactor" value="<?php if(isset($product) && $product instanceof MemoriaDiMassa){echo $product->getFormFactor();}?>"><br>
        <label for="velocitaRotazioneIn">velocita' rotazione (rpm)</label><br>
        <input type="number" min="0" id="velocitaRotazioneIn" name="rotationSpeed" value="<?php if(isset($product) && $product instanceof MemoriaDiMassa){echo $product->getRotationSpeed();}?>"><br>
        <label for="tipoNANDIn">tipo NAND</label><br>
        <input type="text" id="tipoNANDIn" name="NANDType" value="<?php if(isset($product) && $product instanceof MemoriaDiMassa){echo $product->getNANDType();}?>"><br>
        <label for="TBWIn">TBW (TB)</label><br>
        <input type="number" min="0" id="TBWIn" name="TBW" value="<?php if(isset($product) && $product instanceof MemoriaDiMassa){echo $product->getTBW();}?>"><br>
    </form>
    <form id="ram">
        <label for="tipologiaIn">tipologia RAM</label><br>
        <input type="text" id="tipologiaIn" name="RAMtype" value="<?php if(isset($product) && $product instanceof RAM){echo $product->getRAMType();}?>"><br>
        <label for="numeroBanchiIn">numero banchi</label><br>
        <input type="number" min="0" id="numeroBanchiIn" name="stickNumber" value="<?php if(isset($product) && $product instanceof RAM){echo $product->getStickNumber();}?>"><br>
        <label for="dimensioneBanchiIn">dimensione banchi (GB)</label><br>
        <input type="number" min="0" id="dimensioneBanchiIn" name="stickDimension" value="<?php if(isset($product) && $product instanceof RAM){echo $product->getStickDimension();}?>"><br>
        <label for="frequenzaRAMIn">frequenza (MT/s)</label><br>
        <input type="number" min="0" id="frequenzaRAMIn" name="frequency" value="<?php if(isset($product) && $product instanceof RAM){echo $product->getFrequency();}?>"><br>
        <label for="CLIn">timing CL</label><br>
        <input type="number" min="0" id="CLIn" name="CL" value="<?php if(isset($product) && $product instanceof RAM){echo $product->getCL();}?>"><br>
        <label for="tRCDIn">timing RCD</label><br>
        <input type="number" min="0" id="tRCDIn" name="RCD" value="<?php if(isset($product) && $product instanceof RAM){echo $product->getRCD();}?>"><br>
        <label for="tRPIn">timing RP</label><br>
        <input type="number" min="0" id="tRPIn" name="RP" value="<?php if(isset($product) && $product instanceof RAM){echo $product->getRP();}?>"><br>
        <label for="tRASIn">timing RAS</label><br>
        <input type="number" min="0" id="tRASIn" name="RAS" value="<?php if(isset($product) && $product instanceof RAM){echo $product->getRAS();}?>"><br>
    </form>
    <form id="monitor">
        <label for="tecnologiaDisplayIn">tecnologia display</label><br>
        <input type="text" id="tecnologiaDisplayIn" name="displayTech" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getDisplayTech();}?>"><br>
        <label for="risoluzioneOrizzontaleIn">risoluzione orizzontale (px)</label><br>
        <input type="number" min="0" id="risoluzioneOrizzontaleIn" name="resW" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getResolution()[0];}?>"><br>
        <label for="risoluzioneVerticaleIn">risoluzione verticale (px)</label><br>
        <input type="number" min="0" id="risoluzioneVerticaleIn" name="resH" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getResolution()[1];}?>"><br>
        <label for="ratioIn">aspect ratio</label><br>
        <input type="text" id="ratioIn" name="aspect" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getAspectRatio();}?>"><br>
        <label for="dimensionePolliciIn">dimensione schermo (inch)</label><br>
        <input type="number" min="0" id="dimensionePolliciIn" name="diagonal" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getDiagonal();}?>"><br>
        <label for="frequenzaAggiornamentoIn">frequenza aggiornamento (Hz)</label><br>
        <input type="number" min="0" id="frequenzaAggiornamentoIn" name="rRate" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getRefreshRate();}?>"><br>
        <label for="frequenzaAggiornamentoMinimaIn">frequenza aggiornamento minima (Hz)</label><br>
        <input type="number" min="0" id="frequenzaAggiornamentoMinimaIn" name="rRateMin" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getMinimumRefreshRate();}?>"><br>
        <label for="tempoRispostaIn">tempo di risposta (ms)</label><br>
        <input type="number" min="0" id="tempoRispostaIn" name="resTime" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getResponseTime();}?>"><br>
        <label for="tecnologiaSyncIn">tecnologia sync</label><br>
        <input type="text" id="tecnologiaSyncIn" name="vrrTech" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getVRRTech();}?>"><br>
        <label for="speakersIn">speakers</label><br>
        <input type="text" id="speakersIn" name="speakers" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getSpeakers();}?>"><br>
        <label for="numeroConnettoriHDMIIn">numero connettori HDMI</label><br>
        <input type="number" min="0" id="numeroConnettoriHDMIIn" name="hdmiPlugs" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getHDMIPlugs();}?>"><br>
        <label for="numeroConnettoriDPIn">numero connettori DisplayPort</label><br>
        <input type="number" min="0" id="numeroConnettoriDPIn" name="dpPlugs" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getDPPlugs();}?>"><br>
        <label for="numeroConnettoriVGAIn">numero connettori VGA</label><br>
        <input type="number" min="0" id="numeroConnettoriVGAIn" name="vgaPlugs" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getVGAPlugs();}?>"><br>
        <label for="numeroConnettoriDVIIn">numero connettori DVI</label><br>
        <input type="number" min="0" id="numeroConnettoriDVIIn" name="dviPlugs" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getDVIPlugs();}?>"><br>
        <label for="numeroConnettoriUSBIn">numero connettori USB</label><br>
        <input type="number" min="0" id="numeroConnettoriUSBIn" name="usbPlugs" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getUSBPlugs();}?>"><br>
        <label for="numeroConnettoriCuffiaIn">numero connettori cuffia 3.5mm</label><br>
        <input type="number" min="0" id="numeroConnettoriCuffiaIn" name="headphonePlugs" value="<?php if(isset($product) && $product instanceof Monitor){echo $product->getHeadphonePlugs();}?>"><br>
    </form>
    <form id="pastatermica">
        <label for="quantitaPastaIn">quantita' pasta (g)</label><br>
        <input type="number" min="0" id="quantitaPastaIn" name="weight" value="<?php if(isset($product) && $product instanceof PastaTermica){echo $product->getWeight();}?>"><br>
        <label for="conduttivitaSpecificaIn">conduttivita' specifica (W/mK)</label><br>
        <input type="number" min="0" id="conduttivitaSpecificaIn" name="conductivity" value="<?php if(isset($product) && $product instanceof PastaTermica){echo $product->getConductivity();}?>"><br>
    </form>
    <form id="processore">
        <label for="socketIn">socket</label><br>
        <select id="socketIn" name="socket" selected="none">
            <option disabled selected value></option>
            <option value="AM3">AM3</option>
            <option value="AM4">AM4</option>
            <option value="2011">2011</option>
            <option value="LGA1151">LGA1151</option>
            <option value="LGA1155">LGA1155</option>
            <option value="LGA1156">LGA1156</option>
            <option value="LGA1200">LGA1200</option>
        </select>
        <label for="chipsetCPUIn">chipset</label><br>
        <select id="chipsetCPUIn" name="chipset" selected="none" multiple>
            <option disabled selected value></option>
            <option value="X470">X470</option>
            <option value="B450">B450</option>
            <option value="X570">X570</option>
            <option value="B550">B550</option>
            <option value="2011">2011</option>
            <option value="Z370">Z370</option>
            <option value="B360">B360</option>
            <option value="Z590">Z590</option>
            <option value="H470">H470</option>
        </select>
        <!-- SOCKET -->
        <label for="coreIn">numero core</label><br>
        <input type="number" min="0" id="coreIn" name="cores" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getCores();}?>"><br>
        <label for="threadIn">numero thread</label><br>
        <input type="number" min="0" id="threadIn" name="threads" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getThreads();}?>"><br>
        <label for="frequenzaCoreIn">frequenza core (MHz)</label><br>
        <input type="number" min="0" id="frequenzaCoreIn" name="baseClock" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getBaseClock();}?>"><br>
        <label for="frequenzaCoreMaxIn">frequenza core massima (MHz)</label><br>
        <input type="number" min="0" id="frequenzaCoreMaxIn" name="boostClick" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getBoostClock();}?>"><br>
        <label for="cacheL1In">dimensione cache L1 (MB)</label><br>
        <input type="number" min="0" id="cacheL1In" name="L1Cache" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getL1Cache();}?>"><br>
        <label for="cacheL2In">dimensione cache L2 (MB)</label><br>
        <input type="number" min="0" id="cacheL2In" name="L2Cache" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getL2Cache();}?>"><br>
        <label for="cacheL3In">dimensione cache L3 (MB)</label><br>
        <input type="number" min="0" id="cacheL3In" name="L3Cache" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getL3Cache();}?>"><br>
        <label for="architetturaIn">architettura</label><br>
        <input type="text" id="architetturaIn" name="architecture" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getArchitecture();}?>"><br>
        <label for="processoIn">processo produttivo</label><br>
        <input type="text" id="processoIn" name="process" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getProcess();}?>"><br>
        <label for="TDPCPUIn">TDP (W)</label><br>
        <input type="number" min="0" id="TDPCPUIn" name="tdp" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getTDP();}?>"><br>
        <label for="versionePCIEIn">versione PCIE</label><br>
        <input type="text" id="versionePCIEIn" name="pcieVer" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getPCIeVersion();}?>"><br>
        <label for="memoriaSupportataIn">tipo memoria supportata</label><br>
        <input type="text" id="memoriaSupportataIn" name="memoryType" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getMemoryType();}?>"><br>
        <label for="frequenzaMemoriaIn">frequenza memoria supportata (MHz)</label><br>
        <input type="number" min="0" id="frequenzaMemoriaIn" name="memoryClock" value="<?php if(isset($product) && $product instanceof Processore){echo $product->getMemoryClock();}?>"><br>
    </form>
    <form id="schedamadre">
        <label for="chipsetIn">chipset</label><br>
        <select id="chipsetIn" name="chipset" selected="none">
            <option disabled selected value></option>
            <option value="X470">X470</option>
            <option value="B450">B450</option>
            <option value="X570">X570</option>
            <option value="B550">B550</option>
            <option value="2011">2011</option>
            <option value="Z370">Z370</option>
            <option value="B360">B360</option>
            <option value="Z590">Z590</option>
            <option value="H470">H470</option>
        </select>
        <!-- FORM FACTOR -->
        <label for="slotM2In">numero slot M.2</label><br>
        <input type="number" min="0" id="slotM2In" name="slotM2" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getSlotM2();}?>"><br>
        <label for="slotSATAIn">numero prese SATA</label><br>
        <input type="number" min="0" id="slotSATAIn" name="connectorSATA" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getConnectorSATA();}?>"><br>
        <label for="slotRAMIn">numero slot RAM</label><br>
        <input type="number" min="0" id="slotRAMIn" name="slotRAM" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getSlotRAM();}?>"><br>
        <label for="slotPCIEx16In">numero slot PCIEx16</label><br>
        <input type="number" min="0" id="slotPCIEx16In" name="slotPCIEx16" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getSlotPCIEx16();}?>"><br>
        <label for="slotPCIIn">numero slot PCI</label><br>
        <input type="number" min="0" id="slotPCIIn" name="slotPCIEx1" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getSlotPCIEx1();}?>"><br>
        <label for="headerVentoleIn">numero header ventole</label><br>
        <input type="number" min="0" id="headerVentoleIn" name="fanHeader" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getFanHeader();}?>"><br>
        <label for="headerRGBIn">numero header RGB</label><br>
        <input type="number" min="0" id="headerRGBIn" name="RGBHeader" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getRGBHeader();}?>"><br>
        <label for="connettoriUSB2In">numero connettori USB 2</label><br>
        <input type="number" min="0" id="connettoriUSB2In" name="connectorUSB2" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getConnectorUSB2();}?>"><br>
        <label for="connettoriUSB3In">numero connettori USB 3</label><br>
        <input type="number" min="0" id="connettoriUSB3In" name="connectorUSB3" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getConnectorUSB3();}?>"><br>
        <label for="connettoriUSBc">numero connettori USB type C</label><br>
        <input type="number" min="0" id="connettoriUSBc" name="connectorUSBc" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getConnectorUSBc();}?>"><br>
        <label for="connettoriHDMIIn">numero connettori HDMI</label><br>
        <input type="number" min="0" id="connettoriHDMIIn" name="connectorHDMI" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getConnectorHDMI();}?>"><br>
        <label for="connettoriDisplayPortIn">numero connettori DisplayPort</label><br>
        <input type="number" min="0" id="connettoriDisplayPortIn" name="connectorDP" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getConnectorDP();}?>"><br>
        <label for="WiFiIn">Wifi</label><br>
        <input type="text" id="WiFiIn" name="wifi" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getWifi();}?>"><br>
        <label for="ethernetIn">velocita' ethernet massima (Mbps)</label><br>
        <input type="number" min="0" id="ethernetIn" name="ethernetSpeed" value="<?php if(isset($product) && $product instanceof SchedaMadre){echo $product->getEthernetSpeed();}?>"><br>
    </form>
    <form id="schedaethernet">
        <label for="velocitaSchedaEthernetIn">velocita' ethernet massima (Mbps)</label><br>
        <input type="number" min="0" id="velocitaSchedaEthernetIn" name="speed" value="<?php if(isset($product) && $product instanceof SchedaEthernet){echo $product->getSpeed();}?>"><br>
        <label for="connettoriSchedaEthernetIn">numero connettori ethernet</label><br>
        <input type="number" min="0" id="connettoriSchedaEthernetIn" name="ethernetPlugs" value="<?php if(isset($product) && $product instanceof SchedaEthernet){echo $product->getEthernetPlugs();}?>"><br>
        <label for="interfacciaSchedaEthernetIn">interfaccia scheda</label><br>
        <input type="text" id="interfacciaSchedaEthernetIn" name="interface" value="<?php if(isset($product) && $product instanceof SchedaEthernet){echo $product->getInterface();}?>"><br>
    </form>
    <form id="schedawifi">
        <label for="velocitaSchedaWifiIn">velocita' WiFi massima (Mbps)</label><br>
        <input type="number" min="0" id="velocitaSchedaWifiIn" name="speed" value="<?php if(isset($product) && $product instanceof SchedaWifi){echo $product->getSpeed();}?>"><br>
        <label for="numeroAntenneIn">numero antenne</label><br>
        <input type="number" min="0" id="numeroAntenneIn" name="antennasNumber" value="<?php if(isset($product) && $product instanceof SchedaWifi){echo $product->getAntennasNumber();}?>"><br>
        <label for="interfacciaSchedaWifiIn">interfaccia scheda</label><br>
        <input type="text" id="interfacciaSchedaWifiIn" name="interface" value="<?php if(isset($product) && $product instanceof SchedaWifi){echo $product->getInterface();}?>"><br>
        <label for="standardIn">standard</label><br>
        <input type="text" id="standardIn" name="standard" value="<?php if(isset($product) && $product instanceof SchedaWifi){echo $product->getStandard();}?>"><br>
        <label for="bluetoothIn">bluetooth</label><br>
        <input type="text" id="bluetoothIn" name="bluetooth" value="<?php if(isset($product) && $product instanceof SchedaWifi){echo $product->getBluetooth();}?>"><br>
    </form>
    <form id="schedavideo">
        <!-- GPU -->
        <label for="frequenzaBaseIn">frequenza base scheda video (MHz)</label><br>
        <input type="number" min="0" id="frequenzaBaseIn" name="baseClock" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getBaseClock();}?>"><br>
        <label for="frequenzaMaxIn">frequenza massima scheda video (MHz)</label><br>
        <input type="number" min="0" id="frequenzaMaxIn" name="maxClock" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getMaxClock();}?>"><br>
        <label for="frequenzaBoostIn">frequenza boost scheda video (MHz)</label><br>
        <input type="number" min="0" id="frequenzaBoostIn" name="boostClock" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getBoostClock();}?>"><br>
        <label for="frequenzaVRAMIn">frequenza memoria scheda video (MHz)</label><br>
        <input type="number" min="0" id="frequenzaVRAMIn" name="vramClock" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getVramClock();}?>"><br>
        <label for="dimensioneVRAMIn">dimenione memoria scheda video (GB)</label><br>
        <input type="number" min="0" id="dimensioneVRAMIn" name="vramSize" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getVramSize();}?>"><br>
        <label for="connessionePCIEIn">tipo connessione PCIE</label><br>
        <input type="text" id="connessionePCIEIn" name="pcieLanes" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getPCIeLanes();}?>"><br>
        <label for="larghezzaSlotPCIEIn">larghezza slot PCIE</label><br>
        <input type="text" id="larghezzaSlotPCIEIn" name="pcieSlots" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getPCIeSlots();}?>"><br>
        <label for="lunghezzaGPUIn">lunghezza scheda video (mm)</label><br>
        <input type="number" min="0" id="lunghezzaGPUIn" name="length" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getLength();}?>"><br>
        <label for="TDPGPUIn">TDP (W)</label><br>
        <input type="number" min="0" id="TDPGPUIn" name="tdp" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getTDP();}?>"><br>
        <label for="numeroVentoleGPUIn">numero ventole</label><br>
        <input type="number" min="0" id="numeroVentoleGPUIn" name="fans" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getFans();}?>"><br>
        <label for="connettori6PinIn">numero connettori 6 pin</label><br>
        <input type="number" min="0" id="connettori6PinIn" name="pcie6pin" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getPCIe6PinConnectors();}?>"><br>
        <label for="connettori8PinIn">numero connettori 8 pin</label><br>
        <input type="number" min="0" id="connettori8PinIn" name="pcie8pin" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getPCIe8PinConnectors();}?>"><br>
        <label for="connettori12PinIn">numero connettori 12 pin</label><br>
        <input type="number" min="0" id="connettori12PinIn" name="pcie12pin" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getPCIe12PinConnectors();}?>"><br>
        <label for="connettoriHDMIGPUIn">numero connettori HDMI</label><br>
        <input type="number" min="0" id="connettoriHDMIGPUIn" name="hdmiPlugs" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getHDMIPlugs();}?>"><br>
        <label for="connettoriDPGPUIn">numero connettori DisplayPort</label><br>
        <input type="number" min="0" id="connettoriDPGPUIn" name="dpPlugs" value="<?php if(isset($product) && $product instanceof SchedaVideo){echo $product->getDisplayPortPlugs();}?>"><br>
    </form>
    <form id="ventola">
        <label for="dimensioneVentolaIn">dimensione ventola (mm)</label><br>
        <input type="number" min="0" id="dimensioneVentolaIn" name="dimension" value="<?php if(isset($product) && $product instanceof Ventola){echo $product->getDimension();}?>"><br>
        <label for="connettoreVentolaIn">connettore</label><br>
        <input type="text" id="connettoreVentolaIn" name="connector" value="<?php if(isset($product) && $product instanceof Ventola){echo $product->getConnector();}?>"><br>
        <label for="tensioneVentolaIn">tensione alimentazione (V)</label><br>
        <input type="number" min="0" id="tensioneVentolaIn" name="voltage" value="<?php if(isset($product) && $product instanceof Ventola){echo $product->getVoltage();}?>"><br>
        <label for="potenzaVentolaIn">potenza massima (mW)</label><br>
        <input type="number" min="0" id="potenzaVentolaIn" name="maxPower" value="<?php if(isset($product) && $product instanceof Ventola){echo $product->getMaxPower();}?>"><br>
        <label for="rotazioneMaxIn">velocita' rotazione massima (rpm)</label><br>
        <input type="number" min="0" id="rotazioneMaxIn" name="maxSpeed" value="<?php if(isset($product) && $product instanceof Ventola){echo $product->getMaxSpeed();}?>"><br>
        <label for="rotazioneMinIn">velocita' rotazione minima (rpm)</label><br>
        <input type="number" min="0" id="rotazioneMinIn" name="minSpeed" value="<?php if(isset($product) && $product instanceof Ventola){echo $product->getMinSpeed();}?>"><br>
        <label for="flussoM3HIn">flusso aria (m^3/h)</label><br>
        <input type="number" min="0" id="flussoM3HIn" name="flow" value="<?php if(isset($product) && $product instanceof Ventola){echo $product->getFlow();}?>"><br>
        <label for="pressioneIn">pressione statica (mmH2O)</label><br>
        <input type="number" min="0" id="pressioneIn" name="pressure" value="<?php if(isset($product) && $product instanceof Ventola){echo $product->getPressure();}?>"><br>
        <label for="rumoreIn">rumore acustico (dB)</label><br>
        <input type="number" min="0" id="rumoreIn" name="noise" value="<?php if(isset($product) && $product instanceof Ventola){echo $product->getNoise();}?>"><br>
    </form>
    <button id="saveButton">Salva</button>
    <p id="errorMessage"></p>
	<p id="successMessage"></p>
</div>
