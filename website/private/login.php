<div>
    <h1>Effettua il login</h1>
    <form id="loginForm">
            <label for="loginEmail">email</label>
            <input type="text" id="loginEmail" name="loginEmail"/>
            <label for="loginPassword">password</label>
            <input type="password" id="loginPassword" name="loginPassword"/>
            <div>
                <button id="loginShowPassword">Mostra password</button>
                <input type="submit" id="loginSubmit" value="Accedi"/>
            </div>
    </form>
    <p id="loginErrorMessage"></p>
</div>
<div>
    <h1>Crea un nuovo account</h1>
    <form id="registerForm">
            <label for="registerEmail">email</label>
            <input type="text" id="registerEmail" name="registerEmail"/>
            <label for="registerPassword">password</label>
            <input type="password" id="registerPassword" name="registerPassword"/>
            <label for="registerRePassword">conferma password</label>
            <input type="password" id="registerRePassword" name="registerRePassword" />
            <div>
                <button id="registerShowPassword">Mostra password</button>
                <input type="submit" id="registerSubmit" value="Crea Account"/>
            </div>
    </form>
    <p id="registerErrorMessage"></p>
</div>
