<?php
use \Util as Util;
$re = "/^[a-zA-Z0-9]{8}/";
?>

<div>
	<ul>
		<?php
            foreach (Database\Ordine::getAll() as $ordine) {
                $id = $ordine->getID();
                preg_match($re, $id, $match);
				echo "<li><p id='$id'>".$match[0]." ".$ordine->getStatus()."</p><hr><div>";
				foreach ($ordine->getProducts() as $prodotto){
					echo "<div><section><p>".$prodotto[0]->getName()."</p><p>Quantità: ".$prodotto[1]."</p><p>Prezzo unitario: ".($prodotto[2]/100)."€</p></section></div>";
				}
				echo "<div><section><p>Prezzo totale: ".($ordine->getTotal()/100)."€</p></section><section><p>Data creazione: ".$ordine->getCreationDate()->format(Util::DATEFORMAT)."</p></section>";
                if ($ordine->getShippingDate() !== null) {
                    echo "<section><p>Data spedizione: ".$ordine->getShippingDate()->format(Util::DATEFORMAT)."</p></section>";
                }
                if ($ordine->getDeliveryDate() !== null) {
                    echo "<section><p>Data consegna: ".$ordine->getDeliveryDate()->format(Util::DATEFORMAT)."</p></section>";
                }
				echo "</div><div><select id='".$ordine->getID()."' selected='".$ordine->getStatus()."'><option value='In elaborazione'>In elaborazione</option><option value='Spedito'>Spedito</option><option value='Consegnato'>Consegnato</option></select></div></div></li>";
			}
		?>
	</ul>
</div>
