<div>
	<section>
		<h2>Privacy Policy</h2>
		<p><strong>Informativa ai sensi dell'art. 13 del Codice della Privacy</strong></p>
		<p><b>Ai sensi dell'articolo 13 del codice della D.Lgs. 196/2003, vi rendiamo le seguenti informazioni.</b></p>
		<p>Noi di <strong><span id="sito">hardware2unbox.shop</span></strong> riteniamo che la privacy dei nostri visitatori sia estremamente importante. Questo documento descrive dettagliatamente i tipi di informazioni personali raccolti e registrati dal nostro sito e come essi vengano utilizzati.</p>
		<h3>File di Registrazione (Log Files)</h3>
		<p>Come molti altri siti web, il nostro utilizza file di log. Questi file registrano semplicemente i visitatori del sito - di solito una procedura standard delle aziende di hosting e dei servizi di analisi degli hosting.</p>
		<p>Le informazioni contenute nei file di registro comprendono indirizzi di protocollo Internet (IP), il tipo di browser, Internet Service Provider (ISP), informazioni come data e ora, pagine referral, pagine d'uscita ed entrata o il numero di clic.</p>
		<p>Queste informazioni vengono utilizzate per analizzare le tendenze, amministrare il sito, monitorare il movimento degli utenti sul sito e raccogliere informazioni demografiche. Gli indirizzi IP e le altre informazioni non sono collegate a informazioni personali che possono essere identificate, dunque <strong>tutti i dati sono raccolti in forma assolutamente anonima</strong>.</p>
		<div id="cookies">
			<h3>Questo sito web utilizza i Cookies</h3>
			<p>I cookies sono piccoli file di testo che vengono automaticamente posizionati sul PC del navigatore all’interno del browser. Essi contengono informazioni di base sulla navigazione in Internet e grazie al browser vengono riconosciuti ogni volta che l’utente visita il sito.</p>
			<h3>Cookie Policy</h3>
			<p>Questo sito utilizza cookies, anche di terze parti, per migliorarne l’esperienza di navigazione, consentire a chi naviga di usufruire di eventuali servizi online e monitorare la navigazione nel sito.</p>
			<h3>Come disabilitare i Cookies</h3>
			<p>E’ possibile disabilitare i cookies direttamente dal browser utilizzato, accedendo alle impostazioni (preferenze oppure opzioni): questa scelta potrebbe limitare alcune funzionalità di navigazione del sito.</p>
			<h3>Gestione dei Cookies</h3>
			<p>I cookies utilizzati in questo sito possono rientrare nelle categorie descritte di seguito.</p>
			<section>
				<h4>Attività strettamente necessarie al funzionamento</h4>
				<p>Questi cookies hanno natura tecnica e permettono al sito di funzionare correttamente. Ad esempio, mantengono l’utente collegato durante la navigazione evitando che il sito richieda di collegarsi più volte per accedere alle pagine successive.</p>
			</section>
			<section>
				<h4>Attività di salvataggio delle preferenze</h4>
				<p>Questi cookie permettono di ricordare le preferenze selezionate dall’utente durante la navigazione, ad esempio, consentono di impostare la lingua.</p>
			</section>
		</div>
		<h3>Finalità del trattamento</h3>
		<section>
			<h4>I dati possono essere raccolti per una o più delle seguenti finalità:</h4>
			<p>fornire l'accesso ad aree riservate del Portale e di Portali/siti collegati con il presente.</p>
			<p>eseguire gli obblighi previsti da leggi o regolamenti;</p>
			<p>gestione contatti;</p>
		</section>
		<h3>Modalità del trattamento</h3>
		<section>
			<h4>I dati verranno trattati con le seguenti modalità:</h4>
			<p>raccolta dati con modalità single-opt, in apposito database;</p>
			<p>registrazione ed elaborazione su supporto cartaceo e/o magnetico;</p>
			<p>organizzazione degli archivi in forma prevalentemente automatizzata, ai sensi del Disciplinare Tecnico in materia di misure minime di sicurezza, Allegato B del Codice della Privacy. </p>
		</section>
		<h3>Natura obbligatoria</h3>
		<p>Tutti i dati richiesti sono obbligatori.</p>
		<section>
			<h4>Diritti dell'interessato</h4>
			<p>Ai sensi ai sensi dell'art. 7 (Diritto di accesso ai dati personali ed altri diritti) del Codice della Privacy, vi segnaliamo che i vostri diritti in ordine al trattamento dei dati sono:</p>
			<p>conoscere, mediante accesso gratuito l'esistenza di trattamenti di dati che possano riguardarvi;</p>
			<p>essere informati sulla natura e sulle finalità del trattamento.</p>
			<section>
				<h5>Ottenere a cura del titolare, senza ritardo:</h5>
				<p>la conferma dell'esistenza o meno di dati personali che vi riguardano, anche se non ancora registrati, e la comunicazione in forma intellegibile dei medesimi dati e della loro origine, nonché della logica e delle finalità su cui si basa il trattamento; la richiesta può essere rinnovata, salva l'esistenza di giustificati motivi, con intervallo non minore di novanta giorni;</p>
				<p>la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli di cui non è necessaria la conservazione in relazione agli scopi per i quali i dati sono stati raccolti o successivamente trattati;</p>
				<p>l'aggiornamento, la rettifica ovvero, qualora vi abbia interesse, l'integrazione dei dati esistenti;</p>
				<p>opporvi in tutto o in parte per motivi legittimi al trattamento dei dati personali che vi riguardano ancorché pertinenti allo scopo della raccolta;</p>
			</section>
		</section>
		<section>
			<h4>Vi segnaliamo che il titolare del trattamento ad ogni effetto di legge è:</h4>
			<p><b><span id="azienda">hardware2unbox</span></b></p>
			<p><span id="indirizzo">via dell'università 50</span></p>
			<p><span id="cap">47521</span> - <span id="citta">Cesena</span> (<span id="provincia">FC</span>)</p>
			<p>Tel/Fax: <span id="telefono">00000000000</span></p>
			<p>E-mail: <span id="email">info@hardware2unbox.shop</span></p>
		</section>
		<p>Per esercitare i diritti previsti all'art. 7 del Codice della Privacy ovvero per la cancellazione dei vostri dati dall'archivio, è sufficiente contattarci attraverso uno dei canali messi a disposizione.</p>
		<p>Tutti i dati sono protetti attraverso l'uso di antivirus, firewall e protezione attraverso password.</p>
		<h3>Informazioni per i bambini</h3>
		<p>Riteniamo importante assicurare una protezione aggiunta ai bambini online. Noi incoraggiamo i genitori e i tutori a trascorrere del tempo online con i loro figli per osservare, partecipare e/o monitorare e guidare la loro attività online. Noi non raccogliamo dati personali di minori. Se un genitore o un tutore crede che il nostro sito abbia nel suo database le informazioni personali di un bambino, vi preghiamo di contattarci immediatamente (utilizzando la mail fornita) e faremo di tutto per rimuovere tali informazioni il più presto possibile.</p>
		<p>Questa politica sulla privacy si applica solo alle nostre attività online ed è valida per i visitatori del nostro sito web e per quanto riguarda le informazioni condivise e/o raccolte. Questa politica non si applica a qualsiasi informazione raccolta in modalità offline o tramite canali diversi da questo sito web.</p>
		<h3>Consenso</h3>
		<p>Usando il nostro sito web, acconsenti alla nostra politica sulla privacy e accetti i suoi termini. Se desideri ulteriori informazioni o hai domande sulla nostra politica sulla privacy non esitare a contattarci.</p>
</div>