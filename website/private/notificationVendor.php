<?php
use \Util as Util;
?>

<div>
	<ul>
		<?php
            if(isset($_SESSION['user'])){
                $sellerNotifications = \Database\Notifica::getSellerNotifications();
				foreach ($sellerNotifications as $notifica) {
					if($notifica->isUnread()){
						echo "<li><p id='".$notifica->getID()."' class='unread'>".$notifica->getTitle()."</p><hr>";
					} else {
						echo "<li><p id='".$notifica->getID()."'>".$notifica->getTitle()."</p><hr>";
					}
					echo "<div>";
					echo "<section><p>".$notifica->getText()."</p></section>";
					echo "<section><p>Data: ".$notifica->getCreationTime()->format(\Util::DATEFORMAT)."</p></section>";
					echo "</div></li>";
				}
            } else {
				//non capisco perché qua non posso mettere PAGE.login.php, mi indirizza a quella in private wtf?
				echo "<a href='login.php'>Procedi al login per visualizzare le notifiche</a>";
			}
		?>
	</ul>
</div>
