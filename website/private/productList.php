
<div>
    <div>
    	<!-- filtri ricerca & ordinamenti -->
    	<div>
    		<button><img src="<?php echo ICON;?>filter.svg" alt="Filter icon"/> </button>
    		<p>Filtri ricerca</p>
    	</div>
    	<!-- in unguno dei due div bisogna aggiungere l'elemento che compare solo se si clicca sul bottone-->
    	<div>
    		<button><img src="<?php echo ICON;?>order.svg" alt="Order icon"/> </button>
    		<p>Ordinamento</p>
    	</div>
    </div>
    <nav>
    	<input type="checkbox" id="A" name="A" value="A">
    	<label for="A">A</label>
    	<input type="checkbox" id="B" name="B" value="B">
    	<label for="B">B</label>
    </nav>
    <nav>
    	<input type="radio" name="A" value="A"> A
    	<input type="radio" name="B" value="A"> B
    	<input type="radio" name="C" value="C"> C
    </nav>
    <ul>
        <?php
            if (isset($_GET["search"])) {
                foreach(Database\Products\Prodotto::getProductsBySearch($_GET["search"]) as $product) {
                    $card = new \Display\ProductCard($product[0]);
                    echo $card;
                }
            } else if (isset($_GET["category"])) {
                foreach(Database\Products\Prodotto::getProductsByType($_GET["category"]) as $product) {
                    $card = new \Display\ProductCard($product[0]);
                    echo $card;
                }
            } else {
                foreach(Database\Products\Prodotto::getNewEntries() as $product) {
                    $card = new \Display\ProductCard($product[0]);
                    echo $card;
                }
            }
        ?>
    </ul>
</div>