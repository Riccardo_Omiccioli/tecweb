<div>
	<h1>Prodotti in Evidenza</h1>
	<ul>
		<?php foreach(Database\Products\Prodotto::getNewEntries() as $product) {
			$card = new \Display\ProductCard($product[0]);
			echo $card;
		} ?>
	</ul>
</div>
<div>
	<h1>Prodotti in offerta</h1>
	<ul>
		<?php foreach(Database\Products\Prodotto::getDeals() as $product) {
			$card = new \Display\ProductCard($product[0]);
			echo $card;
		} ?>
	</ul>
</div>
