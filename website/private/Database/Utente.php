<?php

namespace Database;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;

use \Util as Util;

class Utente {

	private ?string $idUser = null;
	private ?string $verificationCode = null;
	private ?string $name = null;
	private string $email;
	private string $password;

	private ?int $customerID = null;
	private ?int $sellerID = null;

	private ?array $orders = null; /* ordini == notifiche per l'utente normale, mentre per il venditore notifiche contiente ordine */
	private ?array $notifications = null;
	private int $activeNotifications;

	public function __construct(?string $id, ?string $verificationCode, string $email, string $password, ?string $name, ?int $customerID, ?int $sellerID){
		$this->setID($id);
		$this->setEmail($email);
		$this->setPassword($password);
		$this->setName($name);
		$this->setCustomerID($customerID);
		$this->setSellerID($sellerID);
	}

	public function setID(?string $id) {
		$this->idUser = $id;
	}

	public function getID(){
		return $this->idUser;
	}

	// public function setVerificationCode(?string verificationCode) {}

	public function setName(?string $name){
		if (strlen($name) > 100 ) {
			throw new \LengthException("Nome troppo lungo");
		}
		$this->name = $name;
	}

	public function getName(){
		return $this->name;
	}

	public function setEmail(string $email){
		if (strlen($email) > 320) {
			throw new \LengthException("Email troppo lunga");
		}
		$this->email = $email;
	}
	public function getEmail(){
		return $this->email;
	}

	public function setPassword(string $password){
		if (strlen($password) > 255) {
			throw new \LengthException("Password troppo lunga");
		}
		$this->password = $password;
	}
	public function getPassword(){
		return $this->password;
	}

	public function setCustomerID(?int $id) {
		$this->customerID = $id;
	}
	public function getCustomerID() {
		return $this->customerID;
	}

	public function setSellerID(?int $id) {
		$this->sellerID = $id;
	}
	public function getSellerID() {
		return $this->sellerID;
	}

	public static function getCustomer(int $customerID){
		$query = "SELECT `Utente`.*, `Cliente`.`idCliente`, `Dipendente`.`idDipendente` " .
		"FROM `Utente` LEFT JOIN `Cliente` on `Utente`.`idUtente` = `Cliente`.`Utente_idUtente` LEFT JOIN `Dipendente` on `Utente`.`idUtente` = `Dipendente`.`Utente_idUtente` " .
		"WHERE `Cliente`.`idCliente` = ?;";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param("i", $customerID);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		return new Utente(...$result->fetch_row());
	}

	public function save() {
		$this->idUser = $this->idUser ?? Util::uuid();
		$db = DatabaseWriter::get();
		$db->transaction();
		$stmt = $db->prepare("INSERT INTO Utente VALUES (?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `verificaEmail`=VALUES(`verificaEmail`), `email`=VALUES(`email`), `password`=VALUES(`password`), `nome`=VALUES(`nome`);");
		$stmt->bind_param("sssss", $this->idUser, $this->verificationCode, $this->email, $this->password, $this->name);
		$stmt->execute();
		$stmt->close();
		if ($this->customerID === null) {
			$stmt = $db->prepare("INSERT INTO `Cliente` VALUES (DEFAULT, ?) ON DUPLICATE KEY UPDATE `idCliente`=`idCliente`;");
			$stmt->bind_param("s", $this->idUser);
			$stmt->execute();
			$stmt->close();
			$this->setCustomerID($db->lastID());
		}
		$db->commit();
		return $this->idUser;
	}

	public static function get($id) {
		$query = "SELECT `Utente`.*, `Cliente`.`idCliente`, `Dipendente`.`idDipendente` " .
		"FROM `Utente` LEFT JOIN `Cliente` on `Utente`.`idUtente` = `Cliente`.`Utente_idUtente` LEFT JOIN `Dipendente` on `Utente`.`idUtente` = `Dipendente`.`Utente_idUtente` " .
		"WHERE `Utente`.`idUtente` = ?;";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param("s", $id);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$row = $result->fetch_row();
        $user = $row ? new Utente(...$row) : null;
		return $user;
	}

	public function getOrders(){
		//TODO
		/* query che restituisce gli ordine per questo utente */
		$this->orders = [];
		$result = null;
		foreach ($result->fetch_all(MYSQLI_NUM) as $row) {
			$this->orders[] = $row[0];
		}
		/* non so se ha senso ordinarli al contrario o come vengono restituiti dalla query in generale */
		return $this->orders;
	}

	/*incremento notifiche
	public function incActiveNotifications(){
		$this->activeNotifications = $this->activeNotifications + 1;
	}
	*/

	public static function login(String $email, String $password) {
		$db = DatabaseReader::get();
		$stmt = $db->prepare("SELECT `Utente`.`idUtente`, `Utente`.`password` FROM `Utente` WHERE `Utente`.`email` = ?;");
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		if ($result->num_rows == 0) {
			return null;
		}
		$row = $result->fetch_assoc();
		if (password_verify($password, $row["password"])) {
			return $row["idUtente"];
		}
		return null;
	}
}

?>
