<?php

namespace Database;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use \Database\DatabaseReader as DatabaseReader;
use \Database\DatabaseWriter as DatabaseWriter;
use \Database\Utente as Utente;
use \Database\Products\Prodotto as Prodotto;
use \DateTime as DateTime;
use \Util as Util;

class Ordine {

	private ?String $id;
	private Utente $customer;
	private Indirizzo $address;
	private String $status;
	private DateTime $orderCreationDate;
	private ?DateTime $orderShippingDate = null;
	private ?DateTime $orderDeliveryDate = null;

	private ?array $products = null;

	public function __construct( ?string $id , int $customerID, int $addressID, String $status, String $creationDate, ?String $shippingDate, ?String $deliveryDate){
		$this->id = $id;
		$this->setCustomer($customerID);
		$this->setAddress($addressID);
		$this->setStatus($status);
		$this->setCreationDate($creationDate);
		$this->setShippingDate($shippingDate);
		$this->setDeliveryDate($deliveryDate);
	}


	public function getID(){
		return $this->id;
	}

	public function setCustomer(int $customerID){
		//TODO è sempre il cliente che costruisce ordini quindi in teoria non dovrebbero esserci problemi qua
		
		$this->customer = Utente::getCustomer($customerID);
	}
	public function getCustomer(){
		return $this->customer;
	}


	public function setAddress(int $addressID){
		$this->address = Indirizzo::get($addressID);
	}
	public function getAddress(){
		return $this->address;
	}

	public function setStatus(string $status){
		if ( ($status != "In elaborazione" ) && ($status != "Consegnato") && ($status != "Spedito") ) {
			throw new \Exception("Stato non valido.");
		}
		$this->status = $status;
	}
	public function getStatus(){
		return $this->status;
	}

	public function setCreationDate(String $date){
		$this->orderCreationDate = new DateTime($date);
	}
	public function getCreationDate(){
		return $this->orderCreationDate;
	}

	public function setShippingDate(?String $date){
		$this->orderShippingDate = $date ? new DateTime($date) : null;
	}
	public function getShippingDate(){
		return $this->orderShippingDate;
	}

	public function setDeliveryDate(?String $date){
		$this->orderDeliveryDate = $date ? new DateTime($date) : null;
	}
	public function getDeliveryDate(){
		return $this->orderDeliveryDate;
	}

	public function setProducts(array $products) {
		// [[Prodotto, quantità int, prezzo int]]
		$this->products = $products;
	}
	public function getProducts() {
		return $this->products;
	}

	public function getTotal() {
		if ($this->products === null) {
			throw new Exception("Prodotti dell'ordine non inizializzati");
		}
		$p = 0;
		foreach ($this->products as $product) {
			$p += $product[1] * $product[2];
		}
		return $p;
	}

	public static function get(String $id) {
		$db = DatabaseReader::get();
		$stmt = $db->prepare("SELECT * FROM `Ordine` JOIN `Ordine_has_Prodotto` ON `Ordine`.`idOrdine` = `Ordine_has_Prodotto`.`Ordine_idOrdine` WHERE `Ordine`.`idOrdine` = ?;");
		$stmt->bind_param("s", $id);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$products = $result->fetch_all(MYSQLI_NUM);
		$firstRow = $products[0];
		array_splice($firstRow, 7);
		$order = new Ordine(...$firstRow);
		foreach ($products as &$product) {
			array_splice($product, 0, 8);
			$product[0] = Prodotto::get($product[0])[0];
		}
		$order->setProducts($products);
		return $order;
	}

	public static function getAll() {
		$db = DatabaseReader::get();
		$ids = $db->query("SELECT `idOrdine` FROM `Ordine` ORDER BY `dataCreazioneOrdine` DESC;")->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach($ids as $id) {
			$returns[] = Ordine::get($id[0]);
		}
		return $returns;
	}

	public static function getUserOrders(String|Utente $utente) {
		if (!($utente instanceof Utente)) {
			$utente = Utente::get($utente);
		}
		$id = $utente->getCustomerID();
		$db = DatabaseReader::get();

		$stmt = $db->prepare("SELECT `idOrdine` from `Ordine` WHERE `Ordine`.`Cliente_idCliente` = ? ORDER BY `Ordine`.`dataCreazioneOrdine` DESC");
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$returns = [];
		foreach ($result->fetch_all(MYSQLI_NUM) as $row) {
			$returns[] = Ordine::get($row[0]);
		}

		return $returns;
	}

	public function save() {
		$db = DatabaseWriter::get();
		$db->transaction();
		$addressID = $this->address->getID();
		$customerID = $this->customer->getCustomerID();
		$new = false;
		if ($this->id === null) { // new order
			$this->id = Util::uuid();
			$new = true;
		}
		if ($new) {
			$addressID = $this->address->clone();
		}
		$creationDate = $this->orderCreationDate->format(Util::DATETIMEFORMAT);
		$shippingDate = $this->orderShippingDate;
		if ($shippingDate !== null) {
			$shippingDate = $shippingDate->format(Util::DATETIMEFORMAT);
		}
		$deliveryDate = $this->orderDeliveryDate;
		if ($deliveryDate !== null) {
			$deliveryDate = $deliveryDate->format(Util::DATETIMEFORMAT);
		}
		$stmt = $db->prepare("INSERT INTO `Ordine` VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `Cliente_idCliente`=VALUES(`Cliente_idCliente`), " .
			"`Indirizzo_idIndirizzo`=VALUES(`Indirizzo_idIndirizzo`), `stato`=VALUES(`stato`), `dataCreazioneOrdine`=VALUES(`dataCreazioneOrdine`), " .
			"`dataSpedizione`=VALUES(`dataSpedizione`), `dataConsegna`=VALUES(`dataConsegna`);");
		$stmt->bind_param("siissss", $this->id, $customerID, $addressID, $this->status,$creationDate, $shippingDate, $deliveryDate);
		$stmt->execute();
		$stmt->close();

		//add products
		$values = [];
		$query = "INSERT INTO Ordine_has_Prodotto VALUES ";
		$queryValues = [];
		$params = [];
		if ($new) {
			foreach($this->products as $product) {
				$values[] = $this->id;
				$values[] = $product[0]->getID();
				$values[] = $product[1];
				$values[] = $product[2];
				$params[] = "ssii";
                $queryValues[] = "(?, ?, ?, ?)";
           	}
            $query .= implode(", ", $queryValues);
            error_log($query);
            $stmt = $db->prepare($query);
            $stmt->bind_param(implode("", $params), ...$values);
            $stmt->execute();
            $stmt->close();
		}


		$db->commit();
		return $this->id;
	}
}
?>
