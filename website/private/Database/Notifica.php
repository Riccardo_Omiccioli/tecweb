<?php

namespace Database;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use \Database\DatabaseReader as DatabaseReader;
use \Database\DatabaseWriter as DatabaseWriter;
use \Database\Utente as Utente;
use \Database\Ordine as Ordine;
use \Database\Products\Prodotto as Prodotto;
use \Util as Util;
use \DateTime as DateTime;

class Notifica {

	private ?String $id;
	private String $title;
	private String $text;
	private ?Utente $user = null;
	private bool $new;
	private DateTime $creationTime;

	public function __construct(?String $id, String $title, String $text, ?String $userID, bool $new, ?String $creationTime) {
		$this->setID($id);
		$this->setTitle($title);
		$this->setText($text);
		$this->setUser($userID);
		$this->setNew($new);
		$this->setCreationTime($creationTime);
	}

	public function setID(?string $id) {
		$this->id = $id;
	}
	public function getID(){
		return $this->id;
	}

	public function setTitle(String $title) {
		if (strlen($title) > 255) {
			throw new Exception("Titolo notifica troppo lungo");
		}
		$this->title = $title;
	}
	public function getTitle() {
		return $this->title;
	}

	public function setText(String $text) {
		if (strlen($text) > 65535) {
			throw new Exception("Testo notifica troppo lungo (come?)");
		}
		$this->text = $text;
	}
	public function getText() {
		return $this->text;
	}

	public function hasUser() {
		return $this->user !== null;
	}
	public function setUser(?String $userID) {
		if ($userID !== null) {
			$this->user = Utente::get($userID);
		}
	}
	public function getUser(): Utente {
		return $this->user;
	}

	public function setNew(bool $new) {
		$this->new = $new;
	}
	public function isUnread() {
		return $this->new;
	}

	public function setCreationTime(?String $creationTime) {
		$this->creationTime = new DateTime($creationTime);
	}
	public function getCreationTime() {
		return $this->creationTime;
	}

	
	public function save() {
		$this->id = $this->id ?? Util::uuid();
		$db = DatabaseWriter::get();

		$stmt = $db->prepare("INSERT INTO `Notifica` VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `idNotifica`=VALUES(`idNotifica`), `titolo`=VALUES(`titolo`), `testo`=VALUES(`testo`), " .
			"`Utente_idUtente`=VALUES(`Utente_idUtente`), `nuova`=VALUES(`nuova`), `dataCreazione`=VALUES(`dataCreazione`);");
		$date = $this->creationTime->format(Util::DATETIMEFORMAT);
		$userID = $this->hasUser() ? $this->user->getID() : null;
		$stmt->bind_param("ssssis", $this->id, $this->title, $this->text, $userID, $this->new, $date);
		$stmt->execute();
		$stmt->close();

		return $this->id;
	}


	public static function get(String $id) {
		$db = DatabaseReader::get();
		$stmt = $db->prepare("SELECT * FROM `Notifica` WHERE `idNotifica` = ?");
		$stmt->bind_param("s", $id);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
        $row = $result->fetch_row();
        return $row ? new Notifica(...$row) : null;
    }

    public static function readAll(Utente|String $user) {
        if ($user instanceof Utente) {
            $user = $user->getID();
        }
        $db = DatabaseWriter::get();
        $stmt = $db->prepare("UPDATE `Notifica` SET `nuova` = 0 WHERE `Utente_idUtente` = ?;");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $stmt->close();
    }

	public static function getUserNotifications(Utente|String $user) {
		if ($user instanceof Utente) {
			$user = $user->getID();
		}
		$db = DatabaseReader::get();
		$stmt = $db->prepare("SELECT * FROM `Notifica` WHERE `Utente_idUtente` = ? ORDER BY `dataCreazione` DESC");
		$stmt->bind_param("s", $user);
		$stmt->execute();
		$result = $stmt->get_result();

		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $row) {
			$returns[] = new Notifica(...$row);
		}
		return $returns;
	}

	public static function getUserUnreadNotificationCount(Utente|String $user) {
		if ($user instanceof Utente) {
			$user = $user->getID();
		}
		$db = DatabaseReader::get();
		$stmt = $db->prepare("SELECT COUNT(*) FROM `Notifica` WHERE `Utente_idUtente` = ? AND `nuova` > 0");
		$stmt->bind_param("s", $user);
		$stmt->execute();
		$result = $stmt->get_result();
		return ($result->fetch_row()[0]);
	}

	public static function getSellerNotifications() {
		$db = DatabaseReader::get();
		$result = $db->query("SELECT * FROM `Notifica` WHERE `Utente_idUtente` IS NULL ORDER BY `dataCreazione` DESC");

		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $row) {
			$returns[] = new Notifica(...$row);
		}
		return $returns;
	}

	public static function getSellerUnreadNotificationCount() {
		$db = DatabaseReader::get();
		$result = $db->query("SELECT COUNT(*) FROM `Notifica` WHERE `Utente_idUtente` IS NULL AND `nuova` > 0");
		return ($result->fetch_row()[0]);
	}

}
?>
