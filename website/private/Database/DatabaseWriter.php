<?php

namespace Database;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

class DatabaseWriter extends DatabaseReader {

	private static ?DatabaseWriter $dbw = null;

	private bool $transacting = false;

	private function __construct(String $username, String $password) {
		parent::__construct($username, $password);
	}

	public function superTransaction() {
		$this->transacting = true;
		$this->transaction();
	}
	public function transaction() {
		$this->connection->begin_transaction();
	}

	public function superCommit() {
		$this->transacting = false;
		$this->commit();
	}
	public function commit() {
		if (!$this->transacting) {
			$this->connection->commit();
		}
	}

	public function rollback() {
		$this->connection->rollback();
		$this->transacting = false;
	}

	public function lastID() {
		return $this->connection->insert_id;
	}

	public static function get() {
		if (self::$dbw === null) {
			$credentials = \Credentials::getWriterCredentials();
			self::$dbw = new DatabaseWriter(...$credentials);
		}
		return self::$dbw;
	}
}
?>
