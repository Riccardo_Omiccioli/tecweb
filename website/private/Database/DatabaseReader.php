<?php

namespace Database;

require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

class DatabaseReader {

	const DB_DBNAME   = "h2uDB";
	const DB_IP       = "localhost";
	const DB_PORT     = 3306;

	protected $connection;

	private static ?DatabaseReader $dbr = null;

	protected function __construct(?String $username, ?String $password) {
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$this->connection = new \mysqli(static::DB_IP, $username, $password, static::DB_DBNAME);
		$this->connection->options(MYSQLI_OPT_INT_AND_FLOAT_NATIVE, true);
	}


	public function prepare(String $stmt) {
		return $this->connection->prepare($stmt);
	}

	public function query(String $query) {
		return $this->connection->query($query);
	}


	public static function get() {
		if (self::$dbr === null) {
			$credentials = \Credentials::getReaderCredentials();
			self::$dbr = new DatabaseReader(...$credentials);
		}
		return self::$dbr;
	}
}
?>
