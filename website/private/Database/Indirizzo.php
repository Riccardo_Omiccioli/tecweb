<?php

namespace Database;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use \Database\DatabaseReader as DatabaseReader;
use \Database\DatabaseWriter as DatabaseWriter;

class Indirizzo {
	private ?int $id;
	private Utente $customer;
	private String $address;
	private int $streetNumber;
	private String $city;
	private String $zipCode;
	private ?String $notes;

	public function __construct(?int $id, int $customerID, String $address, int $streetNumber, String $city, String $zipCode, ?String $notes) {
		$this->setID($id);
		$this->setCustomer(Utente::getCustomer($customerID));
		$this->setAddress($address);
		$this->setStreetNumbeR($streetNumber);
		$this->setCity($city);
		$this->setZipCode($zipCode);
		$this->setNotes($notes);
	}

	public function setID(?int $id) {
		$this->id = $id;
	}
	public function getID() {
		return $this->id;
	}

	public function setCustomer(Utente $user) {
		$this->customer = $user;
	}
	public function getCustomer() {
		return $this->customer;
	}

	public function setAddress(String $address) {
		if (strlen($address) > 255) {
			throw new \Exception("L'indirizzo è troppo lungo");
		}
		$this->address = $address;
	}
	public function getAddress() {
		return $this->address;
	}

	public function setStreetNumber(int $number) {
		if ($number <= 0) {
			throw new \Exception("Il numero civico è negativo");
		}
		$this->streetNumber = $number;
	}
	public function getStreetNumber() {
		return $this->streetNumber;
	}

	public function setCity(String $city) {
		if (strlen($city) > 45) {
			throw new \Exception("La città è troppo lunga");
		}
		$this->city = $city;
	}
	public function getCity() {
		return $this->city;
	}

	public function setZipCode(String $zipCode) {
		if (strlen($zipCode) != 5) {
			throw new \Exception("Il CAP non è di 5 cifre");
		}
		$this->zipCode = $zipCode;
	}
	public function getZipCode() {
		return $this->zipCode;
	}

	public function setNotes(?String $notes) {
		if ($notes !== null && strlen($notes) > 255) {
			throw new \Exception("Le note aggiuntive sono troppo lunghe");
		}
		$this->notes = $notes;
	}
	public function getNotes() {
		return $this->notes;
	}


	public function save() {
		$db = DatabaseWriter::get();

		$stmt = $db->prepare("INSERT INTO `Indirizzo` VALUES (COALESCE(?,DEFAULT(`idIndirizzo`)), ?, ?, ?, ?, ?, ?) " . 
			"ON DUPLICATE KEY UPDATE `Cliente_idCliente`=VALUES(`Cliente_idCliente`), `indirizzo`=VALUES(`indirizzo`), `numeroCivico`=VALUES(`numeroCivico`), `citta`=VALUES(`citta`), " .
			"`CAP`=VALUES(`CAP`), `note`=VALUES(`note`);");
		$customerID = $this->customer->getCustomerID();
		$stmt->bind_param("iisisss", $this->id, $customerID, $this->address, $this->streetNumber, $this->city, $this->zipCode, $this->notes);
		$stmt->execute();
		$stmt->close();

		if ($this->id === null) {
			$this->id = $db->lastID();
		}

		return $this->id;
	}

	public function clone() {
		$this->id = null;
		return $this->save();
	}
	
	public static function get(int $id) {
		$db = DatabaseReader::get();

		$stmt = $db->prepare("SELECT * FROM `Indirizzo` WHERE `idIndirizzo` = ?;");
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();

		return new Indirizzo(...$result->fetch_row());
	}

	public static function getUserAddress(Utente|String $user) {
		if (!($user instanceof Utente)) {
			$user = Utente::get($user);
		}
		$customerID = $user->getCustomerID();
		$db = DatabaseReader::get();
		$stmt = $db->prepare("SELECT * FROM `Indirizzo` WHERE `Cliente_idCliente` = ? ORDER BY `idIndirizzo` ASC LIMIT 1;");
		$stmt->bind_param("i", $customerID);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$res = $result->fetch_row();
		if ($res === null) {
			return null;
		}
		return new Indirizzo(...$res);
	}
}
