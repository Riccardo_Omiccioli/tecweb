<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class SchedaEthernet extends Prodotto {

	private int $speed;
	private int $ethernetPlugs;
	private String $interface;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $speed, int $ethernetPlugs, String $interface) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($speed, $ethernetPlugs, $interface);
	}


	public function setSpecs(int $speed, int $ethernetPlugs, String $interface) {
		$this->setSpeed($speed);
		$this->setEthernetPlugs($ethernetPlugs);
		$this->setInterface($interface);
	}

	public function setSpeed(int $speed) {
		if ($speed <= 0) {
			throw new \OutOfBoundsException("La velocità della scheda Ethernet dev'essere superiore a 0Mbps");
		}
		$this->speed = $speed;
	}
	public function getSpeed() {
		return $this->speed;
	}

	public function setEthernetPlugs(int $ethernetPlugs) {
		if ($ethernetPlugs <= 0) {
			throw new \OutOfBoundsException("La scheda Ethernet deve avere più di 0 prese Ethernet");
		}
		$this->ethernetPlugs = $ethernetPlugs;
	}
	public function getEthernetPlugs() {
		return $this->ethernetPlugs;
	}

	public function setInterface(String $interface) {
		if (strlen($interface) > 45) {
			throw new \LengthException("L'interfaccia della scheda di rete è troppo lunga");
		}
		$this->interface = $interface;
	}
	public function getInterface() {
		return $this->interface;
	}

	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Velocità", $this->speed . " Mbps"],
			["Numero connettori ethernet", $this->ethernetPlugs],
			["Interfaccia", $this->interface]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Velocità", $this->speed . " Mbps"],
			["Numero connettori ethernet", $this->ethernetPlugs],
			["Interfaccia", $this->interface]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO SchedaEthernet VALUES (?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `velocita`=VALUES(`velocita`), `numeroConnettoriEthernet`=VALUES(`numeroConnettoriEthernet`), `interfaccia`=VALUES(`interfaccia`);");
		$stmt->bind_param("siis", $id, $this->speed, $this->ethernetPlugs, $this->interface);
		$stmt->execute();
		$stmt->close();
		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `SchedaEthernet`.`velocita`, `SchedaEthernet`.`numeroConnettoriEthernet`, `SchedaEthernet`.`interfaccia` FROM `Prodotto` JOIN `SchedaEthernet` ON `Prodotto`.`idProdotto` = `SchedaEthernet`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new SchedaEthernet(...$row);
		}
		return $returns;
	}

}

?>
