<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class Ventola extends Prodotto {

	private int $dimension;
	private String $connector;
	private int $voltage;
    private int $maxPower;
    private int $maxSpeed;
    private int $minSpeed;
    private int $flow;
    private int $pressure;
    private int $noise;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $dimension, String $connector,  int $voltage,  int $maxPower,  int $maxSpeed,  int $minSpeed,  int $flow,  int $pressure,  int $noise) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($dimension, $connector, $voltage, $maxPower, $maxSpeed, $minSpeed, $flow, $pressure, $noise);
	}

	public function setSpecs(int $dimension, String $connector, int $voltage, int $maxPower, int $maxSpeed, int $minSpeed, int $flow, int $pressure, int $noise) {
		$this->setDimension($dimension);
		$this->setConnector($connector);
		$this->setVoltage($voltage);
        $this->setMaxPower($maxPower);
        $this->setMaxSpeed($maxSpeed);
        $this->setMinSpeed($minSpeed);
        $this->setFlow($flow);
        $this->setPressure($pressure);
        $this->setNoise($noise);
	}

	public function setDimension(int $dimension) {
		if ($dimension <= 0) {
			throw new \OutOfBoundsException("La dimensione della ventola non può essere negativa");
		}
		$this->dimension = $dimension;
	}
	public function getDimension() {
		return $this->dimension;
	}

	public function setConnector(String $connector) {
		if (strlen($connector) > 45) {
			throw new \LengthException("Il nome connettore è troppo lungo");
		}
		$this->connector = $connector;
	}
	public function getConnector() {
		return $this->connector;
	}

    public function setVoltage(int $voltage) {
		if ($voltage <= 0) {
			throw new \OutOfBoundsException("La tensione della ventola non può essere negativa");
		}
		$this->voltage = $voltage;
	}
	public function getVoltage() {
		return $this->voltage;
	}

    public function setMaxPower(int $maxPower) {
		if ($maxPower <= 0) {
			throw new \OutOfBoundsException("La potenza della ventola non può essere negativa");
		}
		$this->maxPower = $maxPower;
	}
	public function getMaxPower() {
		return $this->maxPower;
	}

    public function setMaxSpeed(int $maxSpeed) {
		if ($maxSpeed <= 0) {
			throw new \OutOfBoundsException("La velocità massima della ventola non può essere negativa");
		}
		$this->maxSpeed = $maxSpeed;
	}
	public function getMaxSpeed() {
		return $this->maxSpeed;
	}

    public function setMinSpeed(int $minSpeed) {
		if ($minSpeed <= 0) {
			throw new \OutOfBoundsException("La velocità minima della ventola non può essere negativa");
		}
		$this->minSpeed = $minSpeed;
	}
	public function getMinSpeed() {
		return $this->minSpeed;
	}

    public function setFlow(int $flow) {
		if ($flow <= 0) {
			throw new \OutOfBoundsException("Il flusso della ventola non può essere negativo");
		}
		$this->flow = $flow;
	}
	public function getFlow() {
		return $this->flow;
	}

    public function setPressure(int $pressure) {
		if ($pressure <= 0) {
			throw new \OutOfBoundsException("La pressione della ventola non può essere negativa");
		}
		$this->pressure = $pressure;
	}
	public function getPressure() {
		return $this->pressure;
	}

    public function setNoise(int $noise) {
		if ($noise <= 0) {
			throw new \OutOfBoundsException("Il rumore della ventola non può essere negativo");
		}
		$this->noise = $noise;
	}
	public function getNoise() {
		return $this->noise;
	}

	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Dimensione", $this->dimension . " mm"],
			["Connettore", $this->connector],
			["Tensione", round($this->voltage/1000, 1) . " V"],
			["Velocità", $this->maxSpeed . " rpm"]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Dimensione", $this->dimension . " mm"],
			["Connettore", $this->connector],
			["Tensione", $this->voltage . " mV"],
			["Potenza massima", $this->maxPower . " W"],
			["Velocità massima", $this->maxSpeed . " rpm"],
			["Velocità minima", $this->minSpeed . " rpm"],
			["Flusso aria", $this->flow . " m^3"],
			["Pressione statica", $this->pressure . " µmH2O"],
			["Rumore acustico", $this->noise . " m dB/A"]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO Ventola VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `dimensione`=VALUES(`dimensione`), `connettore`=VALUES(`connettore`), `tensioneAlimentazione`=VALUES(`tensioneAlimentazione`), " .
            "`potenzaMassima`=VALUES(`potenzaMassima`), `velocitaRotazioneMax`=VALUES(`velocitaRotazioneMax`), `velocitaRotazioneMin`=VALUES(`velocitaRotazioneMin`), " .
            "`flussoAriaM3`=VALUES(`flussoAriaM3`), `pressioneStatica`=VALUES(`pressioneStatica`), `rumoreAcustico`=VALUES(`rumoreAcustico`);");
		$stmt->bind_param("sisiiiiiii", $id, $this->dimension, $this->connector, $this->voltage, $this->maxPower, $this->maxSpeed, $this->minSpeed, $this->flow, $this->pressure, $this->noise);
		$stmt->execute();
		$stmt->close();
		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `Ventola`.`dimensione`, `Ventola`.`connettore`, `Ventola`.`tensioneAlimentazione`, `Ventola`.`potenzaMassima`, `Ventola`.`velocitaRotazioneMax`, `Ventola`.`velocitaRotazioneMin`, " .
		"`Ventola`.`flussoAriaM3`, `Ventola`.`pressioneStatica`, `Ventola`.`rumoreAcustico` FROM `Prodotto` JOIN `Ventola` ON `Prodotto`.`idProdotto` = `Ventola`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new Ventola(...$row);
		}
		return $returns;
	}

}

?>
