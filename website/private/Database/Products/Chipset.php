<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\Products\Processore as Processore;

class Chipset {

	private int $id;
	private String $name;
	private ?array $cpus = null;
	private array $cpuIDs;

	private static array $chipsets;

	private function __construct(int $id, String $name, array $cpuIDs) {
		$this->id = $id;
		$this->name = $name;
		$this->cpuIDs = $cpuIDs;
	}

	public function getName() {
		return $this->name;
	}

	public function getID() {
		return $this->id;
	}

	public function getCPUs() {
		if ($this->cpus === null) {
			$this->cpus = Processore::get($this->cpuIDs);
		}
		return $this->cpus;
	}

	public static function get(int|Chipset $id) {
		if ($id instanceof Chipset) {
			return $id;
		}
		if (!isset(static::$chipsets[$id])) {
			$db = DatabaseReader::get();
			$stmt = $db->prepare("SELECT * FROM Chipset LEFT JOIN Chipset_has_Processore ON `Chipset`.`idChipset` = `Chipset_has_Processore`.`Chipset_idChipset` WHERE `idChipset` = ?;");
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$result = $stmt->get_result()->fetch_all(MYSQLI_NUM);
			$stmt->close();
			$ids = [];
			foreach($result as $row) {
				$ids[] = $row[3];
			}
			static::$chipsets[$id] = new Chipset($result[0][0], $result[0][1], $ids);
		}
		return static::$chipsets[$id];
	}

	public static function getAllNames() {
		$db = DatabaseReader::get();
		return $db->query("SELECT * FROM Chipset;")->fetch_all(MYSQLI_ASSOC);
	}

}


?>
