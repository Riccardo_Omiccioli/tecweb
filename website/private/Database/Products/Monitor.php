<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class Monitor extends Prodotto {

	private String $displayTech;
	private int $resolutionWidth;
	private int $resolutionHeight;
	private String $aspectRatio;
	private float $diagonal;
	private int $refreshRate;
	private ?int $refreshRateMin;
	private int $responseTime;
	private ?String $vrrTech;
	private ?String $speakers;
	private int $hdmiPlugs;
	private int $dpPlugs;
	private int $vgaPlugs;
	private int $dviPlugs;
	private int $usbPlugs;
	private int $headphonePlugs;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								String $displayTech, int $resW, int $resH, String $aspect, float $diagonal, int $rRate, ?int $rRateMin, int $resTime, ?String $vrrTech, ?String $speakers,
								int $hdmiPlugs, int $dpPlugs, int $vgaPlugs, int $dviPlugs, int $usbPlugs, int $headphonePlugs) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($displayTech, $resW, $resH, $aspect, $diagonal, $rRate, $rRateMin, $resTime, $vrrTech, $speakers, $hdmiPlugs, $dpPlugs, $vgaPlugs, $dviPlugs, $usbPlugs, $headphonePlugs);
	}


	public function setSpecs(String $displayTech, int $resW, int $resH, String $aspect, float $diagonal, int $rRate, ?int $rRateMin, int $resTime, ?String $vrrTech, ?String $speakers,
							 int $hdmiPlugs, int $dpPlugs, int $vgaPlugs, int $dviPlugs, int $usbPlugs, int $headphonePlugs) {
		$this->setDisplayTech($displayTech);
		$this->setResolution($resW, $resH);
		$this->setAspectRatio($aspect);
		$this->setDiagonal($diagonal);
		$this->setRefreshRate($rRate);
		$this->setMinimumRefreshRate($rRateMin);
		$this->setResponseTime($resTime);
		$this->setVRRTech($vrrTech);
		$this->setSpeakers($speakers);
		$this->setHDMIPlugs($hdmiPlugs);
		$this->setDPPlugs($dpPlugs);
		$this->setVGAPlugs($vgaPlugs);
		$this->setDVIPlugs($dviPlugs);
		$this->setUSBPlugs($usbPlugs);
		$this->setHeadphonePlugs($headphonePlugs);
	}

	public function setDisplayTech(String $displayTech) {
		if (strlen($displayTech) >= 45) {
			throw new \OutOfBoundsException("La tecnologia del pannello del monitor è troppo lunga");
		}
		$this->displayTech = $displayTech;
	}
	public function getDisplayTech() {
		return $this->displayTech;
	}

	public function setResolution(int $resW, int $resH) {
		if ($resW <= 0 || $resH <= 0) {
			throw new \OutOfBoundsException("La risoluzione del monitor dev'essere positiva");
		}
		$this->resolutionWidth = $resW;
		$this->resolutionHeight = $resH;
	}
	public function getResolution() {
		return [$this->resolutionWidth, $this->resolutionHeight];
	}

	public function setAspectRatio(String $aspect) {
		if (strlen($aspect) >= 45) {
			throw new \OutOfBoundsException("L'aspect ratio del monitor è troppo lungo");
		}
		$this->aspectRatio = $aspect;
	}
	public function getAspectRatio() {
		return $this->aspectRatio;
	}

	public function setDiagonal(float $d) {
		if ($d <= 0) {
			throw new \OutOfBoundsException("La diagonale del monitor dev'essere maggiore di 0\"");
		}
		$this->diagonal = $d;
	}
	public function getDiagonal() {
		return $this->diagonal;
	}

	public function setRefreshRate(int $rRate) {
		if ($rRate <= 0) {
			throw new \OutOfBoundsException("La frequenza di aggiornamento del monitor dev'essere maggiore di 0hz");
		}
		$this->refreshRate = $rRate;
	}
	public function getRefreshRate() {
		return $this->refreshRate;
	}

	public function setMinimumRefreshRate(?int $mRRate) {
		if ($mRRate !== null && $mRRate <= 0) {
			throw new \OutOfBoundsException("La frequenza di aggiornamento minima del monitor dev'essere maggiore di 0hz");
		}
		$this->refreshRateMin = $mRRate;
	}
	public function getMinimumRefreshRate() {
		return $this->refreshRateMin;
	}

	public function setResponseTime($rTime) {
		if ($rTime <= 0) {
			throw new \OutOfBoundsException("Il tempo di risposta del monitor dev'essere superiore a 0ms");
		}
		$this->responseTime = $rTime;
	}
	public function getResponseTime() {
		return $this->responseTime;
	}

	public function hasVRR() {
		return $this->vrrTech !== null && $this->refreshRateMin !== null;
	}

	public function setVRRTech(?String $vrrTech) {
		if ($vrrTech !== null && strlen($vrrTech) >= 45) {
			throw new \OutOfBoundsException("La tecnologia VRR del monitor è troppo lunga");
		}
		$this->vrrTech = $vrrTech;
	}
	public function getVRRTech() {
		return $this->vrrTech;
	}

	public function setSpeakers(?String $speakers) {
		if ($speakers !== null && strlen($speakers) >= 45) {
			throw new \OutOfBoundsException("La stringa degli altoparlanti del monitor è troppo lunga");
		}
		$this->speakers = $speakers;
	}
	public function getSpeakers() {
		return $this->speakers;
	}

	public function setHDMIPlugs($hdmiPlugs) {
		if ($hdmiPlugs < 0) {
			throw new \OutOfBoundsException("Le prese HDMI del monitor devono essere almeno 0");
		}
		$this->hdmiPlugs = $hdmiPlugs;
	}
	public function getHDMIPlugs() {
		return $this->hdmiPlugs;
	}

	public function setDPPlugs($dpPlugs) {
		if ($dpPlugs < 0) {
			throw new \OutOfBoundsException("Le prese DisplayPort del monitor devono essere almeno 0");
		}
		$this->dpPlugs = $dpPlugs;
	}
	public function getDPPlugs() {
		return $this->dpPlugs;
	}

	public function setVGAPlugs($vgaPlugs) {
		if ($vgaPlugs < 0) {
			throw new \OutOfBoundsException("Le prese VGA del monitor devono essere almeno 0");
		}
		$this->vgaPlugs = $vgaPlugs;
	}
	public function getVGAPlugs() {
		return $this->vgaPlugs;
	}

	public function setDVIPlugs($dviPlugs) {
		if ($dviPlugs < 0) {
			throw new \OutOfBoundsException("Le prese DVI del monitor devono essere almeno 0");
		}
		$this->dviPlugs = $dviPlugs;
	}
	public function getDVIPlugs() {
		return $this->dviPlugs;
	}

	public function setUSBPlugs($usbPlugs) {
		if ($usbPlugs < 0) {
			throw new \OutOfBoundsException("Le prese USB del monitor devono essere almeno 0");
		}
		$this->usbPlugs = $usbPlugs;
	}
	public function getUSBPlugs() {
		return $this->usbPlugs;
	}

	public function setHeadphonePlugs($headphonePlugs) {
		if ($headphonePlugs < 0) {
			throw new \OutOfBoundsException("Le prese jack 3.5mm del monitor devono essere almeno 0");
		}
		$this->headphonePlugs = $headphonePlugs;
	}
	public function getHeadphonePlugs() {
		return $this->headphonePlugs;
	}

	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Tecnologia display", $this->displayTech],
			["Risoluzione", $this->resolutionWidth . "x" . $this->resolutionHeight . " px"],
			["Dimensione", $this->diagonal . "\" (" . $this->aspectRatio . ")"],
			["Frequenza frame", $this->refreshRate . " Hz"]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSpecs();
		$localSpecs = [
			["Tecnologia display", $this->displayTech],
			["Risoluzione", $this->resolutionWidth . "x" . $this->resolutionHeight . " px (" . $this->aspectRatio . ")"],
			["Dimensione", $this->diagonal . "\""],
			["Frequenza aggiornamento", $this->refreshRate . " Hz"],
			["Frequenza aggiornamento minima", $this->refreshRateMin . " Hz"],
			["Tempo di risposta", $this->responseTime . " ms"],
			["Tecnologia sync", $this->vrrTech],
			["Speakers", $this->speakers],
			["Connettori HDMI", $this->hdmiPlugs],
			["Connettori Display Port", $this->dpPlugs],
			["Connettori VGA", $this->vgaPlugs],
			["Connettori DVI", $this->dviPlugs],
			["Connettori USB", $this->usbPlugs],
			["Connettori cuffia", $this->headphonePlugs],
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO Monitor VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `tecnologiaDisplay`=VALUES(`tecnologiaDisplay`), `risoluzioneOrizzontale`=VALUES(`risoluzioneOrizzontale`), `risoluzioneVerticale`=VALUES(`risoluzioneVerticale`), " .
			"`aspectRatio`=VALUES(`aspectRatio`), `dimensionePollici`=VALUES(`dimensionePollici`), `frequenzaAggiornamento`=VALUES(`frequenzaAggiornamento`), " .
			"`frequenzaAggiornamentoMinima`=VALUES(`frequenzaAggiornamentoMinima`), `tempoRisposta`=VALUES(`tempoRisposta`), `tecnologiaSync`=VALUES(`tecnologiaSync`), `speakers`=VALUES(`speakers`), " .
			"`numeroConnettoriHDMI`=VALUES(`numeroConnettoriHDMI`), `numeroConnettoriDisplayPort`=VALUES(`numeroConnettoriDisplayPort`), `numeroConnettoriVGA`=VALUES(`numeroConnettoriVGA`), " .
			"`numeroConnettoriDVI`=VALUES(`numeroConnettoriDVI`), `numeroConnettoriUSB`=VALUES(`numeroConnettoriUSB`), `numeroConnettoriCuffia`=VALUES(`numeroConnettoriCuffia`);");
		$stmt->bind_param("ssiisdiiissiiiiii", $id, $this->displayTech, $this->resolutionWidth, $this->resolutionHeight, $this->aspectRatio, $this->diagonal, $this->refreshRate, $this->refreshRateMin,
							$this->responseTime, $this->vrrTech, $this->speakers, $this->hdmiPlugs, $this->dpPlugs, $this->vgaPlugs, $this->dviPlugs, $this->usbPlugs, $this->headphonePlugs);
		$stmt->execute();
		$stmt->close();
		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `Monitor`.`tecnologiaDisplay`, `Monitor`.`risoluzioneOrizzontale`, `Monitor`.`risoluzioneVerticale`, `Monitor`.`aspectRatio`, `Monitor`.`dimensionePollici`, ".
			"`Monitor`.`frequenzaAggiornamento`, `Monitor`.`frequenzaAggiornamentoMinima`, `Monitor`.`tempoRisposta`, `Monitor`.`tecnologiaSync`, `Monitor`.`speakers`," .
			"`Monitor`.`numeroConnettoriHDMI`, `Monitor`.`numeroConnettoriDisplayPort`, `Monitor`.`numeroConnettoriVGA`, `Monitor`.`numeroConnettoriDVI`," .
			"`Monitor`.`numeroConnettoriUSB`, `Monitor`.`numeroConnettoriCuffia` FROM `Prodotto` JOIN `Monitor` ON `Prodotto`.`idProdotto` = `Monitor`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new Monitor(...$row);
		}
		return $returns;
	}

}

?>
