<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class Alimentatore extends Prodotto {

	private int $power;
	private String $efficiency;
	private int $pcieConnectors;
	private int $epsConnectors;
	private int $miscConnectors;
	private int $depth;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $power, String $efficiency, int $pcie, int $eps, int $misc, int $depth) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($power, $efficiency, $pcie, $eps, $misc, $depth);
	}


	public function setSpecs(int $power, String $efficiency, int $pcie, int $eps, int $misc, int $depth) {
		$this->setPower($power);
		$this->setEfficiency($efficiency);
		$this->setPCIeConnectors($pcie);
		$this->setEPSConnectors($eps);
		$this->setMiscConnectors($misc);
		$this->setDepth($depth);
	}

	public function setPower(int $power) {
		if ($power <= 0) {
			throw new \OutOfBoundsException("La potenza dell'alimentatore dev'essere superiore a 0W");
		}
		$this->power = $power;
	}
	public function getPower() {
		return $this->power;
	}

	public function setEfficiency(String $efficiency) {
		if (strlen($efficiency) > 45) {
			throw new \LengthException("Efficienza dell'alimentatore troppo lunga");
		}
		$this->efficiency = $efficiency;
	}
	public function getEfficiency() {
		return $this->efficiency;
	}

	public function setPCIeConnectors(int $pcieConnectors) {
		if ($pcieConnectors < 0) {
			throw new \OutOfBoundsException("Il numero di connettori PCIe dell'alimentatore non può essere negativo");
		}
		$this->pcieConnectors = $pcieConnectors;
	}
	public function getPCIeConnectors() {
		return $this->pcieConnectors;
	}

	public function setEPSConnectors(int $epsConnectors) {
		if ($epsConnectors < 0) {
			throw new \OutOfBoundsException("Il numero di connettori EPS dell'alimentatore non può essere negativo");
		}
		$this->epsConnectors = $epsConnectors;
	}
	public function getEPSConnectors() {
		return $this->epsConnectors;
	}

	public function setMiscConnectors(int $miscConnectors) {
		if ($miscConnectors < 0) {
			throw new \OutOfBoundsException("Il numero di connettori miscellanei dell'alimentatore non può essere negativo");
		}
		$this->miscConnectors = $miscConnectors;
	}
	public function getMiscConnectors() {
		return $this->miscConnectors;
	}

	public function setDepth(int $depth) {
		if ($depth <= 0) {
			throw new \OutOfBoundsException("L'alimentatore dev'essere più profondo di 0mm");
		}
		$this->depth = $depth;
	}
	public function getDepth() {
		return $this->depth;
	}

	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Potenza massima", $this->power . " W"],
			["Classe efficienza", $this->efficiency]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSpecs();
		$localSpecs = [
			["Potenza massima", $this->power . " W"],
			["Classe efficienza", $this->efficiency],
			["Connettori PCIE", $this->pcieConnectors],
			["Connettori EPS", $this->epsConnectors],
			["Connettori periferiche", $this->miscConnectors],
			["Profondità", $this->depth . " mm"],
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO Alimentatore VALUES (?, ?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `potenzaMassima`=VALUES(`potenzaMassima`), `classeEfficienza`=VALUES(`classeEfficienza`), `numeroConnettoriPCIE`=VALUES(`numeroConnettoriPCIE`), " .
			"`numeroConnettoriEPS`=VALUES(`numeroConnettoriEPS`), `numeroConnettoriPeriferiche`=VALUES(`numeroConnettoriPeriferiche`), `profondita`=VALUES(`profondita`);");
		$stmt->bind_param("sisiiii", $id, $this->depth, $this->efficiency, $this->pcieConnectors, $this->epsConnectors, $this->miscConnectors, $this->depth);
		$stmt->execute();
		$stmt->close();
		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `Alimentatore`.`potenzaMassima`, `Alimentatore`.`classeEfficienza`, `Alimentatore`.`numeroConnettoriPCIE`, `Alimentatore`.`numeroConnettoriEPS`, " .
			"`Alimentatore`.`numeroConnettoriPeriferiche`, `Alimentatore`.`profondita` FROM `Prodotto` JOIN `Alimentatore` ON `Prodotto`.`idProdotto` = `Alimentatore`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new Alimentatore(...$row);
		}
		return $returns;
	}

}

?>
