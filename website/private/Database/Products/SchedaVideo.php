<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;
use Database\Products\GPU as GPU;

class SchedaVideo extends Prodotto {

	private GPU $gpu;
	private ?int $baseClock;
	private ?int $maxClock;
	private ?int $boostClock;
	private ?int $vramClock;
	private int $vramSize;
	private String $pcieLanes;
	private int $pcieSlots;
	private int $length;
	private int $tdp;
	private int $fans;
	private int $pcie6pin;
	private int $pcie8pin;
	private int $pcie12pin;
	private int $hdmiPlugs;
	private int $dpPlugs;


	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $gpuID, ?int $baseClock, ?int $maxClock, ?int $boostClock, ?int $vramClock, int $vramSize, String $pcieLanes, int $pcieSlots, int $length, int $tdp,
								int $fans, int $pcie6pin, int $pcie8pin, int $pcie12pin, int $hdmiPlugs, int $dpPlugs) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($gpuID, $baseClock, $maxClock, $boostClock, $vramClock, $vramSize, $pcieLanes, $pcieSlots, $length, $tdp, $fans, $pcie6pin, $pcie8pin, $pcie12pin, $hdmiPlugs, $dpPlugs);
	}


	public function setSpecs(?int $gpuID, ?int $baseClock, ?int $maxClock, ?int $boostClock, ?int $vramClock, int $vramSize, String $pcieLanes, int $pcieSlots, int $length, int $tdp,
							 int $fans, int $pcie6pin, int $pcie8pin, int $pcie12pin, int $hdmiPlugs, int $dpPlugs) {
		$this->setGPU(GPU::get($gpuID));
		$this->setBaseClock($baseClock);
		$this->setMaxClock($maxClock);
		$this->setBoostClock($boostClock);
		$this->setVramClock($vramClock);
		$this->setVramSize($vramSize);
		$this->setPCIeLanes($pcieLanes);
		$this->setPCIeSlots($pcieSlots);
		$this->setLength($length);
		$this->setTDP($tdp);
		$this->setFans($fans);
		$this->setPCIe6PinConnectors($pcie6pin);
		$this->setPCIe8PinConnectors($pcie8pin);
		$this->setPCIe12PinConnectors($pcie12pin);
		$this->setHDMIPlugs($hdmiPlugs);
		$this->setDisplayPortPlugs($dpPlugs);
	}

	public function setGPU(GPU $gpu) {
		$this->gpu = $gpu;
	}
	public function getGPU() {
		return $this->gpu;
	}

	public function setBaseClock(?int $baseClock) {
		if ($baseClock !== null && $baseClock <= 0) {
			throw new \LengthException("Il clock base della scheda video dev'essere superiore a 0MHz");
		}
		$this->baseClock = $baseClock;
	}
	public function getBaseClock() {
		return $this->baseClock ?? $this->gpu->getBaseClock();
	}

	public function setMaxClock(?int $maxClock) {
		if ($maxClock !== null && $maxClock <= 0) {
			throw new \LengthException("Il clock massimo della scheda video dev'essere superiore a 0MHz");
		}
		$this->maxClock = $maxClock;
	}
	public function getMaxClock() {
		return $this->maxClock ?? $this->gpu->getMaxClock();
	}

	public function setBoostClock(?int $boostClock) {
		if ($boostClock !== null && $boostClock <= 0) {
			throw new \LengthException("Il clock boost della scheda video dev'essere superiore a 0MHz");
		}
		$this->boostClock = $boostClock;
	}
	public function getBoostClock() {
		return $this->boostClock ?? $this->gpu->getBoostClock();
	}

	public function setVramClock(?int $vramClock) {
		if ($vramClock !== null && $vramClock <= 0) {
			throw new \LengthException("Il clock della memoria video dev'essere superiore a 0MHz");
		}
		$this->vramClock = $vramClock;
	}
	public function getVramClock() {
		return $this->vramClock ?? $this->gpu->getVramClock();
	}

	public function setVramSize(int $vramSize) {
		if ($vramSize <= 0) {
			throw new \OutOfBoundsException("La memoria video dev'essere superiore a 0GB");
		}
		$this->vramSize = $vramSize;
	}
	public function getVramSize() {
		return $this->vramSize;
	}

	public function setPCIeLanes(String $pcieLanes) {
		if (strlen($pcieLanes) > 4) {
			throw new \LengthException("La stringa delle lane PCIe della scheda video è troppo lunga");
		}
		$this->pcieLanes = $pcieLanes;
	}
	public function getPCIeLanes() {
		return $this->pcieLanes;
	}

	public function setPCIeSlots(int $pcieSlots) {
		if ($pcieSlots <= 0) {
			throw new \OutOfBoundsException("Il numero di slot PCIe della scheda video dev'essere maggiore di zero");
		}
		$this->pcieSlots = $pcieSlots;
	}
	public function getPCIeSlots() {
		return $this->pcieSlots;
	}

	public function setLength(int $length) {
		if ($length <= 0) {
			throw new \OutOfBoundsException("La lunghezza della scheda video dev'essere maggiore di 0mm");
		}
		$this->length = $length;
	}
	public function getLength() {
		return $this->length;
	}

	public function setTDP(int $tdp) {
		if ($tdp <= 0) {
			throw new \OutOfBoundsException("Il TDP della scheda video dev'essere maggiore di 0W");
		}
		$this->tdp = $tdp;
	}
	public function getTDP() {
		return $this->tdp;
	}	

	public function setFans(int $fans) {
		if ($fans < 0) {
			throw new \OutOfBoundsException("Il numero di ventole della scheda video dev'essere positivo");
		}
		$this->fans = $fans;
	}
	public function getFans() {
		return $this->fans;
	}

	public function setPCIe6PinConnectors(int $pcie6pin) {
		if ($pcie6pin < 0) {
			throw new \OutOfBoundsException("Il numero di connettori PCIe a 6 pin della scheda video non può essere negativo");
		}
		$this->pcie6pin = $pcie6pin;
	}
	public function getPCIe6PinConnectors() {
		return $this->pcie6pin;
	}

	public function setPCIe8PinConnectors(int $pcie8pin) {
		if ($pcie8pin < 0) {
			throw new \OutOfBoundsException("Il numero di connettori PCIe a 8 pin della scheda video non può essere negativo");
		}
		$this->pcie8pin = $pcie8pin;
	}
	public function getPCIe8PinConnectors() {
		return $this->pcie8pin;
	}

	public function setPCIe12PinConnectors(int $pcie12pin) {
		if ($pcie12pin < 0) {
			throw new \OutOfBoundsException("Il numero di connettori PCIe a 12 pin della scheda video non può essere negativo");
		}
		$this->pcie12pin = $pcie12pin;
	}
	public function getPCIe12PinConnectors() {
		return $this->pcie12pin;
	}

	public function setHDMIPlugs(int $hdmiPlugs) {
		if ($hdmiPlugs < 0) {
			throw new \OutOfBoundsException("Il numero di prese HDMI della scheda video non può essere negativo");
		}
		$this->hdmiPlugs = $hdmiPlugs;
	}
	public function getHDMIPlugs() {
		return $this->hdmiPlugs;
	}

	public function setDisplayPortPlugs(int $dpPlugs) {
		if ($dpPlugs < 0) {
			throw new \OutOfBoundsException("Il numero di prese DisplayPort della scheda video non può essere negativo");
		}
		$this->dpPlugs = $dpPlugs;
	}
	public function getDisplayPortPlugs() {
		return $this->dpPlugs;
	}



	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$vramType = $this->gpu->getVramType();
		$localSpecs = [
			["Frequenza", $this->getMaxClock() . " MHz"],
			["Memoria video", "$this->vramSize GB"],
			["TDP", "$this->tdp W"],
			["Prese", "$this->hdmiPlugs HDMI, $this->dpPlugs DP"]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSummarySpecs();

		$localSpecs = [
			["Frequenza base", $this->getBaseClock() . " MHz"],
			["Frequenza massima", $this->getMaxClock() . " MHz"]
		];

		if ($this->getMaxClock() !== $this->getBoostClock()) {
			$localSpecs[] = ["Frequenza boost", $this->getBoostClock() . " MHz"];
		}

		$pcieConnectors = [];
		if ($this->pcie6pin > 0) {
			$pcieConnectors[] = $this->pcie6pin . "× 6-pin";
		}
		if ($this->pcie8pin > 0) {
			$pcieConnectors[] = $this->pcie8pin . "× 8-pin";
		}
		if ($this->pcie12pin > 0) {
			$pcieConnectors[] = $this->pcie12pin . "× 12-pin";
		}
		$plugs = [];
		if ($this->hdmiPlugs > 0) {
			$plugs[] = $this->hdmiPlugs . "× HDMI";
		}
		if ($this->dpPlugs > 0) {
			$plugs[] = $this->dpPlugs . "× DisplayPort";
		}

		$localSpecs2 = [
			["Memoria video", $this->vramSize . " GB " . $this->gpu->getVramType()],
			["Frequenza memoria video", $this->getVramClock() . " MHz"],
			["Connessione PCIe", $this->gpu->getPCIeVersion() . " " . $this->pcieLanes],
			["Larghezza slot PCIe", $this->getPCIeSlots()],
			["Lunghezza", $this->length . " mm"],
			["TDP", $this->tdp . " W"],
			["Numero ventole", $this->getFans()],
			["Connettori alimentazione", implode(", ", $pcieConnectors)],
			["Prese", implode(", ", $plugs)]
		];

		return array_merge($superSpecs, $localSpecs, $localSpecs2);
	}


	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();
		$gpuID = $this->gpu->getID();
		$stmt = $db->prepare("INSERT INTO SchedaVideo VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `Prodotto_idProdotto`=VALUES(`Prodotto_idProdotto`), `GPU_idGPU`=VALUES(`GPU_idGPU`), `frequenzaBase`=VALUES(`frequenzaBase`), " .
			"`frequenzaMassima`=VALUES(`frequenzaMassima`), `frequenzaBoost`=VALUES(`frequenzaBoost`), `frequenzaMemoria`=VALUES(`frequenzaMemoria`), `dimensioneMemoria`=VALUES(`dimensioneMemoria`), " .
			"`connessionePCIE`=VALUES(`connessionePCIE`), `larghezzaSlotPCIE`=VALUES(`larghezzaSlotPCIE`), `lunghezza`=VALUES(`lunghezza`), `TDP`=VALUES(`TDP`), `numeroVentole`=VALUES(`numeroVentole`), " .
			"`numeroPrese6PIN`=VALUES(`numeroPrese6PIN`), `numeroPrese8PIN`=VALUES(`numeroPrese8PIN`), `numeroPrese12PIN`=VALUES(`numeroPrese12PIN`), `numeroConnettoriHDMI`=VALUES(`numeroConnettoriHDMI`), " .
			"`numeroConnettoriDIsplayPort`=VALUES(`numeroConnettoriDIsplayPort`);");
		$stmt->bind_param("siiiiiisiiiiiiiii", $id, $gpuID, $this->baseClock, $this->maxClock, $this->boostClock, $this->vramClock, $this->vramSize, $this->pcieLanes, $this->pcieSlots,
							$this->length, $this->tdp, $this->fans, $this->pcie6pin, $this->pcie8pin, $this->pcie12pin, $this->hdmiPlugs, $this->dpPlugs);
		$stmt->execute();
		$stmt->close();
		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `SchedaVideo`.`GPU_idGPU`, `SchedaVideo`.`frequenzaBase`, `SchedaVideo`.`frequenzaMassima`, `SchedaVideo`.`frequenzaBoost`, `SchedaVideo`.`frequenzaMemoria`, " .
		"`SchedaVideo`.`dimensioneMemoria`, `SchedaVideo`.`connessionePCIE`, `SchedaVideo`.`larghezzaSlotPCIE`, `SchedaVideo`.`lunghezza`, `SchedaVideo`.`TDP`, `SchedaVideo`.`numeroVentole`, " .
		"`SchedaVideo`.`numeroPrese6PIN`, `SchedaVideo`.`numeroPrese8PIN`, `SchedaVideo`.`numeroPrese12PIN`, `SchedaVideo`.`numeroConnettoriHDMI`, `SchedaVideo`.`numeroConnettoriDisplayPort` " .
		"FROM `Prodotto` JOIN `SchedaVideo` ON `Prodotto`.`idProdotto` = `SchedaVideo`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new SchedaVideo(...$row);
		}
		return $returns;
	}

}

?>
