<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class Cavo extends Prodotto {

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);

	}

	public function getSummarySpecs() {
		$parentSpecs = parent::getSummarySpecs();
		$localSpecs = [];
		if ($this->getColor() !== null) {
			$localSpecs[] = ["Colore", $this->getColor()];
		}
		return array_merge($parentSpecs, $localSpecs);
	}

	public function getSpecs() {
		return parent::getSpecs();
	}



	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO Cavo VALUES (?) ON DUPLICATE KEY UPDATE `Prodotto_idProdotto`=`Prodotto_idProdotto`;");
		$stmt->bind_param("s", $id);
		$stmt->execute();
		$stmt->close();
		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.* FROM `Prodotto` JOIN `Cavo` ON `Prodotto`.`idProdotto` = `Cavo`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new Cavo(...$row);
		}
		return $returns;
	}

}

?>
