<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class SchedaWiFi extends Prodotto {

	private int $speed;
	private int $antennasNumber;
	private String $interface;
    private String $standard;
    private String $bluetooth;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $speed, int $antennasNumber, String $interface, String $standard, String $bluetooth) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($speed, $antennasNumber, $interface, $standard, $bluetooth);
	}

	public function setSpecs(int $speed, int $antennasNumber, String $interface, String $standard, String $bluetooth) {
		$this->setSpeed($speed);
		$this->setAntennasNumber($antennasNumber);
		$this->setInterface($interface);
        $this->setStandard($standard);
        $this->setBluetooth($bluetooth);
	}

	public function setSpeed(int $speed) {
		if ($speed <= 0) {
			throw new \OutOfBoundsException("La velocità della scheda WiFi dev'essere superiore a 0Mbps");
		}
		$this->speed = $speed;
	}
	public function getSpeed() {
		return $this->speed;
	}

	public function setAntennasNumber(int $antennasNumber) {
		if ($antennasNumber <= 0) {
			throw new \OutOfBoundsException("Il numero delle antenne non può essere negativo");
		}
		$this->antennasNumber = $antennasNumber;
	}
	public function getAntennasNumber() {
		return $this->antennasNumber;
	}

	public function setInterface(String $interface) {
		if (strlen($interface) > 45) {
			throw new \LengthException("L'interfaccia della scheda di rete è troppo lunga");
		}
		$this->interface = $interface;
	}
	public function getInterface() {
		return $this->interface;
	}

    public function setStandard(String $standard) {
		if (strlen($standard) > 45) {
			throw new \LengthException("Lo standard inserito è troppo lungo");
		}
		$this->standard = $standard;
	}
	public function getStandard() {
		return $this->standard;
	}

    public function setBluetooth(String $bluetooth) {
		if (strlen($bluetooth) > 45) {
			throw new \LengthException("Bluetooth inserito è troppo lungo");
		}
		$this->bluetooth = $bluetooth;
	}
	public function getBluetooth() {
		return $this->bluetooth;
	}

	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Velocità", $this->speed . " Mbps"],
			["Standard", $this->standard],
			["Bluetooth", $this->bluetooth]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Velocità", $this->speed . " Mbps"],
			["Numero antenne", $this->antennasNumber],
			["Interfaccia", $this->interface],
			["Standard", $this->standard],
			["Bluetooth", $this->bluetooth]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO SchedaWiFi VALUES (?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `velocita`=VALUES(`velocita`), `numeroAntenne`=VALUES(`numeroAntenne`), `interfaccia`=VALUES(`interfaccia`), `standard`=VALUES(`standard`), `bluetooth`=VALUES(`bluetooth`);");
		$stmt->bind_param("siisss", $id, $this->speed, $this->antennasNumber, $this->interface, $this->standard, $this->bluetooth);
		$stmt->execute();
		$stmt->close();
		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `SchedaWiFi`.`velocita`, `SchedaWiFi`.`numeroAntenne`, `SchedaWiFi`.`interfaccia`, `SchedaWiFi`.`standard`, `SchedaWiFi`.`bluetooth` FROM `Prodotto` JOIN `SchedaWiFi` ON `Prodotto`.`idProdotto` = `SchedaWiFi`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new SchedaWiFi(...$row);
		}
		return $returns;
	}

}

?>
