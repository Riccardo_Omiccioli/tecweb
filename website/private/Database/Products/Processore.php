<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;
use Database\Products\Socket as Socket;

class Processore extends Prodotto {

	private int $cores;
	private int $threads;
	private int $baseClock;
	private int $boostClock;
	private int $L1Cache;
	private int $L2Cache;
	private int $L3Cache;
	private String $architecture;
	private String $process;
	private int $tdp;
	private String $pcieVer;
	private String $memoryType;
	private int $memoryClock;
	private Socket $socket;

	private array $chipsets;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $cores, int $threads, int $baseClock, int $boostClock, int $L1Cache, int $L2Cache, int $L3Cache, String $architecture, String $process, int $tdp, 
								String $pcieVer, String $memoryType, int $memoryClock, int $socketID) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($cores, $threads, $baseClock, $boostClock, $L1Cache, $L2Cache, $L3Cache, $architecture, $process, $tdp, $pcieVer, $memoryType, $memoryClock, $socketID);
	}


	public function setSpecs(int $cores, int $threads, int $baseClock, int $boostClock, int $L1Cache, int $L2Cache, int $L3Cache, String $architecture, String $process, int $tdp, 
							 String $pcieVer, String $memoryType, int $memoryClock, int $socketID) {
		$this->setCores($cores);
		$this->setThreads($threads);
		$this->setBaseClock($baseClock);
		$this->setBoostClock($boostClock);
		$this->setL1Cache($L1Cache);
		$this->setL2Cache($L2Cache);
		$this->setL3Cache($L3Cache);
		$this->setArchitecture($architecture);
		$this->setProcess($process);
		$this->setTDP($tdp);
		$this->setPCIeVersion($pcieVer);
		$this->setMemoryType($memoryType);
		$this->setMemoryClock($memoryClock);
		$this->setSocket(Socket::get($socketID));
	}

	public function setCores(int $cores) {
		if ($cores <= 0) {
			throw new \OutOfBoundsException("I core del processore devono essere superiori a 0");
		}
		$this->cores = $cores;
	}
	public function getCores() {
		return $this->cores;
	}

	public function setThreads(int $threads) {
		if ($threads <= 0) {
			throw new \OutOfBoundsException("I thread del processore devono essere superiori a 0");
		}
		$this->threads = $threads;
	}
	public function getThreads() {
		return $this->threads;
	}

	public function setBaseClock(int $baseClock) {
		if ($baseClock <= 0) {
			throw new \OutOfBoundsException("Il clock base del processore dev'essere superiore a 0MHz");
		}
		$this->baseClock = $baseClock;
	}
	public function getBaseClock() {
		return $this->baseClock;
	}

	public function setBoostClock(int $boostClock) {
		if ($boostClock <= 0) {
			throw new \OutOfBoundsException("Il clock massimo del processore dev'essere superiore a 0MHz");
		}
		$this->boostClock = $boostClock;
	}
	public function getBoostClock() {
		return $this->boostClock;
	}

	public function setL1Cache(int $L1Cache) {
		if ($L1Cache <= 0) {
			throw new \OutOfBoundsException("La cache L1 del processore dev'essere superiore a 0KB");
		}
		$this->L1Cache = $L1Cache;
	}
	public function getL1Cache() {
		return $this->L1Cache;
	}

	public function setL2Cache(int $L2Cache) {
		if ($L2Cache <= 0) {
			throw new \OutOfBoundsException("La cache L2 del processore dev'essere superiore a 0KB");
		}
		$this->L2Cache = $L2Cache;
	}
	public function getL2Cache() {
		return $this->L2Cache;
	}

	public function setL3Cache(int $L3Cache) {
		if ($L3Cache <= 0) {
			throw new \OutOfBoundsException("La cache L3 del processore dev'essere superiore a 0KB");
		}
		$this->L3Cache = $L3Cache;
	}
	public function getL3Cache() {
		return $this->L3Cache;
	}

	public function setArchitecture(String $architecture) {
		if (strlen($architecture) > 45) {
			throw new \LengthException("L'architettura del processore è troppo lunga");
		}
		$this->architecture = $architecture;
	}
	public function getArchitecture() {
		return $this->architecture;
	}

	public function setProcess(String $process) {
		if (strlen($process) > 45) {
			throw new \LengthException("Il processo produttivo del processore è troppo lungo");
		}
		$this->process = $process;
	}
	public function getProcess() {
		return $this->process;
	}

	public function setTDP(int $tdp) {
		if ($tdp <= 0) {
			throw new \OutOfBoundsException("Il TDP del processore dev'essere maggiore di 0W");
		}
		$this->tdp = $tdp;
	}
	public function getTDP() {
		return $this->tdp;
	}

	public function setPCIeVersion(String $pcieVer) {
		if (strlen($pcieVer) > 45) {
			throw new \LengthException("La versione PCIe del processore è troppo lunga");
		}
		$this->pcieVer = $pcieVer;
	}
	public function getPCIeVersion() {
		return $this->pcieVer;
	}

	public function setMemoryType(String $memoryType) {
		if (strlen($memoryType) > 45) {
			throw new \LengthException("Il tipo di RAM del processore è troppo lungo");
		}
		$this->memoryType = $memoryType;
	}
	public function getMemoryType() {
		return $this->memoryType;
	}

	public function setMemoryClock(int $memoryClock) {
		if ($memoryClock <= 0) {
			throw new \OutOfBoundsException("Il clock del controller della memoria del processore dev'essere maggiore di 0MHz");
		}
		$this->memoryClock = $memoryClock;
	}
	public function getMemoryClock() {
		return $this->memoryClock;
	}

	public function setSocket(Socket $socket) {
		$this->socket = $socket;
	}
	public function getSocket() {
		return $this->socket;
	}


	public function setChipsets(array $chipsets) {
		$this->chipsets = [];
		foreach ($chipsets as $id) {
			$chipset = Chipset::get($id);
			if ($chipset === null) {
				throw new Exception("Invalid ID or chipset");
			}
			$this->chipsets[] = $chipset;
		}
	}
	public function getChipsets() {
		return $this->chipsets;
	}

	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Numero core", $this->cores],
			["Numero threads", $this->threads],
			["Frequenza", sprintf("%.1f/%.1f GHz", $this->baseClock/1000, $this->boostClock/1000)],
			["Cache L3", $this->L3Cache . " MB"]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Numero core", $this->cores],
			["Numero threads", $this->threads],
			["Frequenza base", $this->baseClock . " MHz"],
			["Frequenza massima", $this->boostClock . " MHz"],
			["Cache L1", $this->L1Cache . " MB"],
			["Cache L2", $this->L2Cache . " MB"],
			["Cache L3", $this->L3Cache . " MB"],
			["Architettura", $this->architecture],
			["Processo produttivo", $this->process],
			["TDP", $this->tdp . " W"],
			["Versione PCIE", $this->pcieVer],
			["Tipo memoria", $this->memoryType],
			["Frequenza memoria", $this->memoryClock . " MHz"],
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();
		$socketID = $this->socket->getID();
		$stmtStr = "INSERT INTO Processore VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " .
		    "ON DUPLICATE KEY UPDATE `numeroCore`=VALUES(`numeroCore`), `numeroThread`=VALUES(`numeroThread`), `frequenzaCore`=VALUES(`frequenzaCore`), `frequenzaCoreMassima`=VALUES(`frequenzaCoreMassima`), " .
		    "`cacheL1`=VALUES(`cacheL1`), `cacheL2`=VALUES(`cacheL2`), `cacheL3`=VALUES(`cacheL3`), `architettura`=VALUES(`architettura`), `processoProduttivo`=VALUES(`processoProduttivo`), " .
		    "`TDP`=VALUES(`TDP`), `versionePCIE`=VALUES(`versionePCIE`), `tipoMemoria`=VALUES(`tipoMemoria`), `frequenzaMemoria`=VALUES(`frequenzaMemoria`), `Socket_idSocket`=VALUES(`Socket_idSocket`)";
		$stmt = $db->prepare($stmtStr);
		$stmt->bind_param("siiiiiiississii", $id, $this->cores, $this->threads, $this->baseClock, $this->boostClock, $this->L1Cache, $this->L2Cache, $this->L3Cache, $this->architecture, $this->process,
						   $this->tdp, $this->pcieVer, $this->memoryType, $this->memoryClock, $socketID);
		$stmt->execute();
		$stmt->close();

		$stmt = $db->prepare("DELETE FROM Chipset_has_Processore WHERE Processore_idProdotto = ?;");
		$stmt->bind_param("s", $id);
		$stmt->execute();
		$stmt->close();

		$values = [];
		$params = "";
		$stmtStr = "INSERT INTO Chipset_has_Processore VALUES ";
		$valuesStr = [];
		foreach ($this->chipsets as $key => $chipset) {
			$last = $key !== count($this->chipsets);
			$values[] = $chipset->getID();
			$values[] = $id;
			$valuesStr[] = "(?, ?)";
			$params .= "is";
		}

		$stmt = $db->prepare($stmtStr . implode(", ", $valuesStr));
		$stmt->bind_param($params, ...$values);
		$stmt->execute();
		$stmt->close();

		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `Processore`.`numeroCore`, `Processore`.`numeroThread`, `Processore`.`frequenzaCore`, `Processore`.`frequenzaCoreMassima`, " .
		"`Processore`.`cacheL1`, `Processore`.`cacheL2`, `Processore`.`cacheL3`, `Processore`.`architettura`, `Processore`.`processoProduttivo`, `Processore`.`TDP`, " .
		"`Processore`.`versionePCIE`, `Processore`.`tipoMemoria`, `Processore`.`frequenzaMemoria`, `Processore`.`Socket_idSocket` " .
		"FROM `Prodotto` JOIN `Processore` ON `Prodotto`.`idProdotto` = `Processore`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$db = DatabaseReader::get();
		$stmt = $db->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$cpu = new Processore(...$row);
			$stmt = $db->prepare("SELECT `Chipset_has_Processore`.`Chipset_idChipset` FROM `Chipset_has_Processore` WHERE `Chipset_has_Processore`.`Processore_idProdotto` = ?;");
			$id = $cpu->getID();
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$result = $stmt->get_result();
			$rows = $result->fetch_all(MYSQLI_NUM);
			$chipsets = [];
			foreach($rows as $row) {
				$chipsets[] = $row[0];
			}
			$cpu->setChipsets($chipsets);
			$returns[] = $cpu;
		}
		return $returns;
	}

}

?>
