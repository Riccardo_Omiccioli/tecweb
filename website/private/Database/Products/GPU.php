<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;

class GPU {

	private int $id;
	private String $name;
	private int $baseClock;
	private int $maxClock;
	private int $boostClock; 
	private String $vram;
	private int $vramClock;
	private int $vramBus;
	private String $pcieVer;
	private String $process;
	private int $tdp;

	private static array $gpus;

	private function __construct($data) {
		$this->id = $data['idGPU'];
		$this->name = $data['nome'];
		$this->baseClock = $data['frequenzaBase'];
		$this->maxClock = $data['frequenzaMassima'];
		$this->boostClock = $data['frequenzaBoost'];
		$this->vram = $data['tipoMemoriaVideo'];
		$this->vramClock = $data['frequenzaMemoria'];
		$this->vramBus = $data['larghezzaBusMemoria'];
		$this->pcieVer = $data['versionePCIE'];
		$this->process = $data['processoProduttivo'];
		$this->tdp = $data['TDP'];
	}

	public function getID() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getBaseClock() {
		return $this->baseClock;
	}

	public function getMaxClock() {
		return $this->maxClock;
	}

	public function getBoostClock() {
		return $this->boostClock;
	}

	public function getVramType() {
		return $this->vram;
	}

	public function getVramClock() {
		return $this->vramClock;
	}

	public function getVramBusWidth() {
		return $this->vramBus;
	}

	public function getPCIeVersion() {
		return $this->pcieVer;
	}

	public function getProcessTechnology() {
		return $this->process;
	}

	public function getTDP() {
		return $this->tdp;
	}


	public static function get(int $id) {
		if (!isset(static::$gpus[$id])) {
			$db = DatabaseReader::get();
			$stmt = $db->prepare("SELECT * FROM GPU WHERE `idGPU` = ?;");
			$stmt->bind_param("i", $id);
			$stmt->execute();
			$result = $stmt->get_result()->fetch_assoc();
			$stmt->close();
			static::$gpus[$id] = new GPU($result);
		}
		return static::$gpus[$id];
	}


}

?>
