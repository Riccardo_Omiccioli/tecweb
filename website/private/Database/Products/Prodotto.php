<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;

use \Util as Util;
use \DateTime as DateTime;

class Prodotto {

	static $types;

	private ?String   $idProduct;
	private String    $code;
	private String    $name;
	private ?String   $description;
	private int       $price;
	private int       $quantity;
	private ?String   $color;
	private String    $tag;
	private int       $type;
	private float     $discount;
	private DateTime  $insertDate;

	private ?array  $images = null;

	protected function __construct(?String $ID, String $code, String $name, ?String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date) {
		$this->idProduct = $ID;
		$this->setCode($code);
		$this->setName($name);
		$this->setDescription($desc);
		$this->setPrice($price);
		$this->setQuantity($quantity);
		$this->setColor($color);
		$this->setTag($tag);
		$this->setType($type);
		$this->setDiscount($discount);
		$this->setInsertDate($date);
	}
    
    public function setID(?String $id) {
        $this->idProduct = $id;
    }
	public function getID() {
		return $this->idProduct;
	}

	public function setCode(String $code) {
		if (strlen($code) > 255) {
			throw new \LengthException("Codice del prodotto troppo lungo");
		}
		$this->code = $code;
	}
	public function getCode() {
		return $this->code;
	}

	public function setName(String $name) {
		if (strlen($name) > 255) {
			throw new \LengthException("Nome del prodotto troppo lungo");
		}
		$this->name = $name;
	}
	public function getName() {
		return $this->name;
	}

	public function setDescription(?String $description) {
		$this->description = $description;
	}
	public function getDescription() {
		return $this->description;
	}

	public function setPrice(int $price) {
		if ($price <= 0) {
			throw new \OutOfBoundsException("Prezzo del prodotto nullo o negativo");
		}
		$this->price = $price;
	}
	public function getPrice() {
		return $this->price;
	}

	public function setQuantity(String $quantity) {
		if ($quantity < 0) {
			throw new \OutOfBoundsException("Quantità del prodotto negativa");
		}
		$this->quantity = $quantity;
	}
	public function getQuantity() {
		return $this->quantity;
	}

	public function setColor(?String $color) {
		if (strlen($color) > 45) {
			throw new \LengthException("Colore del prodotto troppo lungo");
		}
		$this->color = $color;
	}
	public function getColor() {
		return $this->color;
	}

	public function setTag(String $tag) {
		if (strlen($tag) > 45) {
			throw new \LengthException("Tag del prodotto troppo lungo");
		}
		$this->tag = $tag;
	}
	public function getTag() {
		return $this->tag;
	}

	public function setType(int $type) {
		//TODO: check if type exists
		$this->type = $type;
	}
	public function getType() {
		return $this->type;
	}

	public function setDiscount(float $discount) {
		if ($discount < 0 || $discount > 100) {
			throw new \OutOfBoundsException("Sconto minore di 0% o maggiore di 100%");
		}
		$this->discount = $discount;
	}
	public function getDiscount() {
		return $this->discount;
	}

	public function setInsertDate(String $date) {
		//TODO: check if $date is date
		$this->insertDate = new DateTime($date);
	}
	public function getInsertDate() {
		return $this->date;
	}


	public function setImages(array $images) {
		$this->images = $images;
	}
	public function getImages() {
		if ($this->images === null && $this->idProduct !== null) {
			$db = DatabaseReader::get();
			$stmt = $db->prepare("SELECT `ImmagineProdotto`.`link` FROM `ImmagineProdotto` WHERE `ImmagineProdotto`.`Prodotto_idProdotto` = ? ORDER BY `ImmagineProdotto`.`idImmagineProdotto` ASC;");
			$stmt->bind_param("s", $this->idProduct);
			$stmt->execute();
			$result = $stmt->get_result();
			$stmt->close();

			$this->images = [];
			foreach ($result->fetch_all(MYSQLI_NUM) as $row) {
				$this->images[] = $row[0];
			}
		}
		return $this->images;
	}


	public function getFullProduct() {
		//TODO: return the full product, looking for its specialization
	}

	public function getSummarySpecs() {
		//return the specs that should be shown in the product card
		return [];
	}

	public function getSpecs() {
		return [
			["Codice",	$this->code],
			["Nome",	$this->name],
			["Colore",	$this->color]
		];
	}

	protected function save() {
		// A Product can't be saved to the database by itself; the more specific product (processor, motherboard, air cooler) should handle saving. USE MYSQL TRANSACTIONS. A partial save is awful to deal with.
		// parent::save() should be used by the "next" classes.
		// This returns the product ID and should use REPLACE INTO ("update or create").
		// INSERT INTO * ON DUPLICATE KEY UPDATE is viable, but _all_ columns need to be specified in the update statement. Except for the ID. Probably safer, more tedious to write. Much more tedious to write.
		$this->idProduct = $this->idProduct ?? Util::uuid();
        $db = DatabaseWriter::get();
        $dateStr = $this->insertDate->format(Util::DATETIMEFORMAT);
		$stmt = $db->prepare("INSERT INTO Prodotto VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `codice`=VALUES(`codice`), `nome`=VALUES(`nome`), `descrizione`=VALUES(`descrizione`), " .
			"`prezzo`=VALUES(`prezzo`), `quantita`=VALUES(`quantita`), `colore`=VALUES(`colore`), `tag`=VALUES(`tag`), `Tipo_Prodotto_idTipo`=VALUES(`Tipo_Prodotto_idTipo`), " .
			"`sconto`=VALUES(`sconto`);");
		$stmt->bind_param("ssssiissids", $this->idProduct, $this->code, $this->name, $this->description, $this->price, $this->quantity, $this->color, $this->tag, $this->type, $this->discount, $dateStr);
		$stmt->execute();
		$stmt->close();

		$stmt = $db->prepare("DELETE FROM `ImmagineProdotto` WHERE `ImmagineProdotto`.`Prodotto_idProdotto` = ?;");
		$stmt->bind_param("s", $this->idProduct);
		$stmt->execute();
		$stmt->close();

		$values = [];
		$params = "";
		$stmtStr = "INSERT INTO `ImmagineProdotto` VALUES ";
		$i = 0;
		foreach ($this->getImages() as $key => $image) {
			$last = $key !== count($this->images);
			$values[] = $i;
			$values[] = $image;
			$values[] = $this->idProduct;
			$stmtStr .= "(?, ?, ?)" . ($last ? ";" : ", ");
			$params .= "iss";
			$i++;
		}
		$stmt = $db->prepare($stmtStr);
		$stmt->bind_param($params, ...$values);
		$stmt->execute();
		$stmt->close();

		return $this->idProduct;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto in Prodotto::get");
		}

		$db = DatabaseReader::get();
		$stmt = $db->prepare("SELECT `Tipo_Prodotto`.`nomeTabella` FROM Prodotto JOIN Tipo_Prodotto ON `Prodotto`.`Tipo_Prodotto_idTipo` = `Tipo_Prodotto`.`idTipo` WHERE idProdotto = ?");
		$stmt->bind_param("s", $ids[0]);
		$stmt->execute();
		$result = $stmt->get_result();

		if ($result->num_rows == 0) {
			throw new Exception("Prodotto ${id} non trovato");
		}

		if ($result->num_rows > 1) {
			throw new Exception("Excuse me wtf");
		}

		$type = $result->fetch_array(MYSQLI_NUM)[0];
		$stmt->close();
		$class = "Database\\Products\\$type";
		return $class::get($ids);
	}

	public static function getCategoryLinks() {
		$db = DatabaseReader::get();
		$result = $db->query("SELECT `Tipo_Prodotto`.`idTipo` as `link`, `Tipo_Prodotto`.`nomeTipo` as `name` from `Tipo_Prodotto` ORDER BY `Tipo_Prodotto`.`nomeTipo`;");
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public static function getProductClassName(int $id) {
		$db = DatabaseReader::get();
		$stmt = $db->prepare("SELECT `Tipo_Prodotto`.`nomeTabella` FROM `Tipo_Prodotto` WHERE `idTipo` = ?");
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		return $result->fetch_row()[0];
	}

	public static function getNewEntries() {
		$db = DatabaseReader::get();
		$result = $db->query("SELECT `idProdotto` from `Prodotto` ORDER BY `dataInserimento` DESC, `idProdotto` DESC LIMIT 10;");
		$rows = $result->fetch_all(MYSQLI_NUM);
		$products = [];
		foreach($rows as $row) {
			$products[] = Prodotto::get($row[0]);
		}
		return $products;
	}

	public static function getDeals() {
		$db = DatabaseReader::get();
		$result = $db->query("SELECT `idProdotto` from `Prodotto` ORDER BY `sconto` DESC, `idProdotto` DESC LIMIT 10;");
		$rows = $result->fetch_all(MYSQLI_NUM);
		$products = [];
		foreach($rows as $row) {
			$products[] = Prodotto::get($row[0]);
		}
		return $products;
	}

	public static function getProductsByType(int $id){
		$db = DatabaseReader::get();
		$stmt = $db->prepare("SELECT `idProdotto` from `Prodotto` WHERE `Tipo_Prodotto_idTipo` = ? ORDER BY `dataInserimento` DESC, `idProdotto` DESC LIMIT 20;");
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$result = $stmt->get_result();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$stmt->close();
		$products = [];
		foreach($rows as $row) {
			$products[] = Prodotto::get($row[0]);
		}
		return $products;
	}

	public static function getProductsBySearch(string $search){
		$search = str_replace(" ", "%", "%".$search."%");
		$db = DatabaseReader::get();
		$stmt = $db->prepare("SELECT `idProdotto` from `Prodotto` WHERE `tag` LIKE ? OR `nome` LIKE ? ORDER BY `dataInserimento` DESC, `idProdotto` DESC LIMIT 20;");
		$stmt->bind_param("ss", $search, $search);
		$stmt->execute();
		$result = $stmt->get_result();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$stmt->close();
		$products = [];
		foreach($rows as $row) {
			$products[] = Prodotto::get($row[0]);
		}
		return $products;
	}

}

?>
