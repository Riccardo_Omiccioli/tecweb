<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class PastaTermica extends Prodotto {

	private int $weight;
	private int $conductivity;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $weight, int $conductivity) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($weight, $conductivity);
	}


	public function setSpecs(int $weight, int $conductivity) {
		$this->setWeight($weight);
		$this->setConductivity($conductivity);
	}

	public function setWeight(int $weight) {
		if ($weight < 0) {
			throw new \OutOfBoundsException("La quantità di pasta termica non può essere negativa");
		}
		$this->weight = $weight;
	}
	public function getWeight() {
		return $this->weight;
	}

	public function setConductivity(int $conductivity) {
		if ($conductivity < 0) {
			throw new \OutOfBoundsException("La conduttività non può essere negativa");
		}
		$this->conductivity = $conductivity;
	}
	public function getConductivity() {
		return $this->conductivity;
	}

	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Quantità", $this->weight . " g"],
			["Conduttività", $this->conductivity . " W/(mK)"]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Quantità", $this->weight . " g"],
			["Conduttività", $this->conductivity . " W/(mK)"]
		];
		return array_merge($superSpecs, $localSpecs);
	}


	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();

		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO PastaTermica VALUES (?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `quantitaPasta`=VALUES(`quantitaPasta`), `conduttivitaSpecifica`=VALUES(`conduttivitaSpecifica`);");
		$stmt->bind_param("sii", $id, $this->weight, $this->conductivity);
		$stmt->execute();
		$stmt->close();

		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `PastaTermica`.`quantitaPasta`, `PastaTermica`.`conduttivitaSpecifica` FROM `Prodotto` JOIN `PastaTermica` ON `Prodotto`.`idProdotto` = `PastaTermica`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new PastaTermica(...$row);
		}
		return $returns;
	}

}

?>
