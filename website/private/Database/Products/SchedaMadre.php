<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use \Database\DatabaseReader as DatabaseReader;
use \Database\DatabaseWriter as DatabaseWriter;
use \Database\Products\Prodotto as Prodotto;
use \Database\Products\Chipset as Chipset;
use \Database\Products\Socket as Socket;
use \Database\Products\FormFactorMobo as FormFactorMobo;

class SchedaMadre extends Prodotto {

    private Chipset $chipset;
    private FormFactorMobo $formFactor;
	private int $slotM2;
    private int $connectorSATA;
    private int $slotRAM;
    private int $slotPCIEx16;
    private int $slotPCIEx1;
    private int $fanHeader;
	private int $RGBHeader;
    private int $connectorUSB2;
	private int $connectorUSB3;
	private int $connectorUSBc;
    private int $connectorHDMI;
    private int $connectorDP;
	private String $wifi;
    private int $ethernetSpeed;
    private Socket $socket;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $chipset, int $formFactor, int $slotM2, int $connectorSATA, int $slotRAM, int $slotPCIEx16,  int $slotPCIEx1,
                                int $fanHeader, int $RGBHeader, int $connectorUSB2, int $connectorUSB3, int $connectorUSBc, int $connectorHDMI, int $connectorDP,
                                String $wifi, int $ethernetSpeed, int $socketID) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($chipset, $formFactor, $slotM2, $connectorSATA, $slotRAM, $slotPCIEx16, $slotPCIEx1, $fanHeader, $RGBHeader, $connectorUSB2, $connectorUSB3,
						$connectorUSBc, $connectorHDMI, $connectorDP, $wifi, $ethernetSpeed, $socketID);
	}

	public function setSpecs(int $chipset, int $formFactor, int $slotM2, int $connectorSATA, int $slotRAM, int $slotPCIEx16,  int $slotPCIEx1,
                            int $fanHeader, int $RGBHeader, int $connectorUSB2, int $connectorUSB3, int $connectorUSBc, int $connectorHDMI, int $connectorDP,
                            String $wifi, int $ethernetSpeed, int $socketID) {
        $this->setChipset(Chipset::get($chipset));
		$this->setFormFactor(FormFactorMobo::get($formFactor));
		$this->setSlotM2($slotM2);
		$this->setConnectorSATA($connectorSATA);
        $this->setSlotRAM($slotRAM);
        $this->setSlotPCIEx16($slotPCIEx16);
        $this->setSlotPCIEx1($slotPCIEx1);
        $this->setFanHeader($fanHeader);
        $this->setRGBHeader($RGBHeader);
        $this->setConnectorUSB2($connectorUSB2);
        $this->setConnectorUSB3($connectorUSB3);
        $this->setConnectorUSBc($connectorUSBc);
        $this->setConnectorHDMI($connectorHDMI);
        $this->setConnectorDP($connectorDP);
        $this->setWifi($wifi);
	$this->setEthernetSpeed($ethernetSpeed);
	$this->setSocket(Socket::get($socketID));
	}

    public function setChipset(Chipset $chipset) {
        $this->chipset = $chipset;
    }
    public function getChipset() {
        return $this->chipset;
    }

	public function setFormFactor(FormFactorMobo $formFactor) {
		$this->formFactor = $formFactor;
	}
	public function getFormFactor() {
		return $this->formFactor;
	}

	public function setSlotM2(int $slotM2) {
		if ($slotM2 < 0) {
			throw new \OutOfBoundsException("Il numero degli slot M.2 non può essere negativo");
		}
		$this->slotM2 = $slotM2;
	}
	public function getSlotM2() {
		return $this->slotM2;
	}

    public function setConnectorSATA(int $connectorSATA) {
		if ($connectorSATA < 0) {
			throw new \OutOfBoundsException("Il numero dei connettori SATA non può essere negativo");
		}
		$this->connectorSATA = $connectorSATA;
	}
	public function getConnectorSATA() {
		return $this->connectorSATA;
	}

    public function setSlotRAM(int $slotRAM) {
		if ($slotRAM < 0) {
			throw new \OutOfBoundsException("Il numero degli slot RAM non può essere negativo");
		}
		$this->slotRAM = $slotRAM;
	}
	public function getSlotRAM() {
		return $this->slotRAM;
	}

    public function setSlotPCIEx16(int $slotPCIEx16) {
		if ($slotPCIEx16 < 0) {
			throw new \OutOfBoundsException("Il numero degli slot PCIEx16 non può essere negativo");
		}
		$this->slotPCIEx16 = $slotPCIEx16;
	}
	public function getSlotPCIEx16() {
		return $this->slotPCIEx16;
	}

    public function setSlotPCIEx1(int $slotPCIEx1) {
		if ($slotPCIEx1 < 0) {
			throw new \OutOfBoundsException("Il numero degli slot PCIEx1 non può essere negativo");
		}
		$this->slotPCIEx1 = $slotPCIEx1;
	}
	public function getSlotPCIEx1() {
		return $this->slotPCIEx1;
	}

    public function setFanHeader(int $fanHeader) {
		if ($fanHeader < 0) {
			throw new \OutOfBoundsException("Il numero degli header ventole non può essere negativo");
		}
		$this->fanHeader = $fanHeader;
	}
	public function getFanHeader() {
		return $this->fanHeader;
	}

    public function setRGBHeader(int $RGBHeader) {
		if ($RGBHeader < 0) {
			throw new \OutOfBoundsException("Il numero degli header RGB non può essere negativo");
		}
		$this->RGBHeader = $RGBHeader;
	}
	public function getRGBHeader() {
		return $this->RGBHeader;
	}

    public function setConnectorUSB2(int $connectorUSB2) {
		if ($connectorUSB2 < 0) {
			throw new \OutOfBoundsException("Il numero dei connettori USB2 non può essere negativo");
		}
		$this->connectorUSB2 = $connectorUSB2;
	}
	public function getConnectorUSB2() {
		return $this->connectorUSB2;
	}

    public function setConnectorUSB3(int $connectorUSB3) {
		if ($connectorUSB3 < 0) {
			throw new \OutOfBoundsException("Il numero dei connettori USB3 non può essere negativo");
		}
		$this->connectorUSB3 = $connectorUSB3;
	}
	public function getConnectorUSB3() {
		return $this->connectorUSB3;
	}

    public function setConnectorUSBc(int $connectorUSBc) {
		if ($connectorUSBc < 0) {
			throw new \OutOfBoundsException("Il numero dei connettori USBc non può essere negativo");
		}
		$this->connectorUSBc = $connectorUSBc;
	}
	public function getConnectorUSBc() {
		return $this->connectorUSBc;
	}

    public function setConnectorHDMI(int $connectorHDMI) {
		if ($connectorHDMI < 0) {
			throw new \OutOfBoundsException("Il numero dei connettori HDMI non può essere negativo");
		}
		$this->connectorHDMI = $connectorHDMI;
	}
	public function getConnectorHDMI() {
		return $this->connectorHDMI;
	}

    public function setConnectorDP(int $connectorDP) {
		if ($connectorDP < 0) {
			throw new \OutOfBoundsException("Il numero dei connettori DP non può essere negativo");
		}
		$this->connectorDP = $connectorDP;
	}
	public function getConnectorDP() {
		return $this->connectorDP;
	}

    public function setWifi(String $wifi) {
        if (strlen($wifi) > 45) {
            throw new \OutOfBoundsException("Valore wifi troppo lungo");
        }
        $this->wifi = $wifi;
    }
    public function getWifi() {
        return $this->wifi;
    }

    public function setSocket(Socket $socket) {
        $this->socket = $socket;
    }
    public function getSocket() {
        return $this->socket;
    }

    public function setEthernetSpeed(int $ethernetSpeed) {
		if ($ethernetSpeed < 0) {
			throw new \OutOfBoundsException("La velocità ethernet non può essere negativa");
		}
		$this->ethernetSpeed = $ethernetSpeed;
	}
	public function getEthernetSpeed() {
		return $this->ethernetSpeed;
	}

	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Chipset", $this->chipset->getName()],
			["Form factor", $this->formFactor->getName()],
			["Numero slot RAM", $this->slotRAM],
			["Wifi", $this->wifi]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Chipset", $this->chipset->getName()],
			["Form factor", $this->formFactor->getName()],
			["Numero slot M.2", $this->slotM2],
			["Numero connettori SATA", $this->connectorSATA],
			["Numero slot RAM", $this->slotRAM],
			["Numero slot PCIex16", $this->slotPCIEx16],
			["Numero slot PCIex1", $this->slotPCIEx1],
			["Numero header ventole", $this->fanHeader],
			["Numero header RGB", $this->RGBHeader],
			["Numero connettori USB 2", $this->connectorUSB2],
			["Numero connettori USB 3", $this->connectorUSB3],
			["Numero connettori USB c", $this->connectorUSBc],
			["Numero connettori HDMI", $this->connectorHDMI],
			["Numero connettori Display Port", $this->connectorDP],
			["Wifi", $this->wifi],
			["Velocità ethernet", $this->ethernetSpeed . " Mbps"]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();
		$chipsetID = $this->chipset->getID();
		$formFactorID = $this->formFactor->getID();
		$socketID = $this->socket->getID();
		$stmt = $db->prepare("INSERT INTO SchedaMadre VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `Chipset_idChipset`=VALUES(`Chipset_idChipset`), `FormFactorMobo_idFormFactor`=VALUES(`FormFactorMobo_idFormFactor`), " .
			"`numeroSlotM2`=VALUES(`numeroSlotM2`), `numeroPreseSATA`=VALUES(`numeroPreseSATA`), `numeroSlotRAM`=VALUES(`numeroSlotRAM`), " .
            "`numeroSlotPCIEx16`=VALUES(`numeroSlotPCIEx16`), `numeroSlotPCIEx1`=VALUES(`numeroSlotPCIEx1`), `numeroHeaderVentole`=VALUES(`numeroHeaderVentole`), `numeroHeaderRGB`=VALUES(`numeroHeaderRGB`), " .
            "`preseUSB2`=VALUES(`preseUSB2`), `preseUSB3`=VALUES(`preseUSB3`), `preseUSBc`=VALUES(`preseUSBc`), `preseHDMI`=VALUES(`preseHDMI`), `preseDisplayPort`=VALUES(`preseDisplayPort`), " .
            "`WiFi`=VALUES(`WiFi`), `velocitaEthernet`=VALUES(`velocitaEthernet`);");
		$stmt->bind_param("siiiiiiiiiiiiiisii", $id, $chipsetID, $formFactorID, $this->slotM2, $this->connectorSATA, $this->slotRAM, $this->slotPCIEx16, $this->slotPCIEx1,
                                            $this->fanHeader, $this->RGBHeader, $this->connectorUSB2, $this->connectorUSB3, $this->connectorUSBc, $this->connectorHDMI, $this->connectorDP,
                                             $this->wifi, $this->ethernetSpeed, $socketID);
		$stmt->execute();
		$stmt->close();

		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `SchedaMadre`.`Chipset_idChipset`, `SchedaMadre`.`FormFactorMobo_idFormFactor`, `SchedaMadre`.`numeroSlotM2`, `SchedaMadre`.`numeroPreseSATA`, `SchedaMadre`.`numeroSlotRAM`, `SchedaMadre`.`numeroSlotPCIEx16`, `SchedaMadre`.`numeroSlotPCIEx1`, ".
		"`SchedaMadre`.`numeroHeaderVentole`, `SchedaMadre`.`numeroHeaderRGB`, `SchedaMadre`.`preseUSB2`, `SchedaMadre`.`preseUSB3`, `SchedaMadre`.`preseUSBc`, `SchedaMadre`.`preseHDMI`, `SchedaMadre`.`preseDisplayPort`, ".
		"`SchedaMadre`.`WiFi`, `SchedaMadre`.`velocitaEthernet`, `SchedaMadre`.`Socket_idSocket` FROM `Prodotto` JOIN `SchedaMadre` ON `Prodotto`.`idProdotto` = `SchedaMadre`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new SchedaMadre(...$row);
		}
		return $returns;
	}

}

?>
