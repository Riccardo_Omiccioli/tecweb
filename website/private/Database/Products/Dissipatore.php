<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class Dissipatore extends Prodotto {

	private int $tdp;
	private int $fans;
	private ?int $fanSize;
	private int $height;
	private ?int $ramHeight;
	private ?int $radLength;

	private array $sockets;

	public function __construct(?String $ID, String $code, String $name, ?String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $tdp, int $fans, ?int $fanSize, int $height, ?int $ramHeight, ?int $radLength) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($tdp, $fans, $fanSize, $height, $ramHeight, $radLength);
	}


	public function setSpecs(int $tdp, int $fans, ?int $fanSize, int $height, ?int $ramHeight, ?int $radLength) {
		$this->setTDP($tdp);
		$this->setFans($fans);
		$this->setFanSize($fanSize);
		$this->setHeight($height);
		$this->setRamHeight($ramHeight);
		$this->setRadLength($radLength);
	}

	public function setTDP(int $tdp) {
		if ($tdp <= 0) {
			throw new \OutOfBoundsException("Il TDP del dissipatore dev'essere superiore a 0W");
		}
		$this->tdp = $tdp;
	}
	public function getTDP() {
		return $this->tdp;
	}

	public function setFans(int $fans) {
		if ($fans < 0) {
			throw new \OutOfBoundsException("Il numero di ventole del dissipatore non può essere negativo");
		}
		$this->fans = $fans;
	}
	public function getFans() {
		return $this->fans;
	}

	public function hasFanSize() {
		return $this->fanSize !== null;
	}
	public function setFanSize(?int $fanSize) {
		if ($fanSize !== null && $fanSize <= 0) {
			throw new \OutOfBoundsException("Le ventole del dissipatore devono avere raggio maggiore di 0mm");
		}
		$this->fanSize = $fanSize;
	}
	public function getFanSize() {
		return $this->fanSize;
	}

	public function setHeight(int $height) {
		if ($height <= 0) {
			throw new \OutOfBoundsException("Il dissipatore deve avere un'altezza maggiore di 0mm");
		}
		$this->height = $height;
	}
	public function getHeight() {
		return $this->height;
	}

	public function hasRamHeight() {
		return $this->ramHeight !== null;
	}
	public function setRamHeight(?int $ramHeight) {
		if ($ramHeight !== null && $ramHeight <= 0) {
			throw new \OutOfBoundsException("Il dissipatore deve avere un'altezza della ram maggiore di 0mm");
		}
		$this->ramHeight = $ramHeight;
	}
	public function getRamHeight() {
		return $this->ramHeight;
	}

	public function hasRadLength() {
		return $this->radLength !== null;
	}
	public function setRadLength(?int $radLength) {
		if ($radLength !== null && $radLength <= 0) {
			throw new \OutOfBoundsException("Il dissipatore deve avere una lunghezza maggiore di 0mm");
		}
		$this->radLength = $radLength;
	}
	public function getRadLength() {
		return $this->radLength;
	}


	public function setSockets(array $IDs) {
		$this->sockets = [];
		foreach ($IDs as $id) {
			$socket = Socket::get($id);
			if ($socket === null) {
				throw new Exception("Invalid ID or socket");
			}
			$this->sockets[] = $socket;
		}
	}
	public function getSockets() {
		return $this->sockets;
	}


	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["TDP", $this->tdp . " W"],
			["Altezza", $this->height . " mm"]
		];
		if ($this->hasRadLength()) {
			$localSpecs[] = ["Lunghezza radiatore", $this->radLength . " mm"];
		}
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSpecs();
		$localSpecs = [
			["TDP", $this->tdp . " W"],
			["Ventole", $this->fans]
		];
		if ($this->hasFanSize()) {
			$localSpecs[] = ["Dimensione ventole", $this->fanSize];
		}
		$localSpecs[] = ["Altezza", $this->height . " mm"];
		if ($this->hasRamHeight()) {
			$localSpecs[] = ["Altezza massima RAM", $this->ramHeight . " mm"];
		}
		if ($this->hasRadLength()) {
			$localSpecs[] = ["Lunghezza radiatore", $this->radLength . " mm"];
		}
		return array_merge($superSpecs, $localSpecs);
	}


	public function save() {
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO Dissipatore VALUES (?, ?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `TDP`=VALUES(`TDP`), `numeroVentole`=VALUES(`numeroVentole`), `dimensioneVentole`=VALUES(`dimensioneVentole`), " .
			"`altezza`=VALUES(`altezza`), `altezzaMassimaRAM`=VALUES(`altezzaMassimaRAM`), `lunghezzaRadiatore`=VALUES(`lunghezzaRadiatore`);");
		$stmt->bind_param("siiiiii", $id, $this->tdp, $this->fans, $this->fanSize, $this->height, $this->ramHeight, $this->radLength);
		$stmt->execute();
		$stmt->close();

		$stmt = $db->prepare("DELETE FROM Dissipatore_has_Socket WHERE Dissipatore_Prodotto_idProdotto = ?;");
		$stmt->bind_param("s", $id);
		$stmt->execute();
		$stmt->close();

		$values = [];
		$params = "";
		$stmtStr = "INSERT INTO Dissipatore_has_Socket VALUES ";
		$valuesStr = [];
		foreach ($this->sockets as $key => $socket) {
			$values[] = $socket->getID();
			$values[] = $id;
			$valuesStr[] = "(?, ?)";
			$params .= "is";
		}
		$stmt = $db->prepare($stmtStr . implode(", ", $valuesStr));
		$stmt->bind_param($params, ...$values);
		$stmt->execute();
		$stmt->close();
		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$db = DatabaseReader::get();
		$query = "SELECT `Prodotto`.*, `Dissipatore`.`TDP`, `Dissipatore`.`numeroVentole`, `Dissipatore`.`dimensioneVentole`, `Dissipatore`.`altezza`, `Dissipatore`.`altezzaMassimaRAM`, " .
			"`Dissipatore`.`lunghezzaRadiatore` FROM `Prodotto` JOIN `Dissipatore` ON `Prodotto`.`idProdotto` = `Dissipatore`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = $db->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$cooler = new Dissipatore(...$row);
			$stmt = $db->prepare("SELECT `Dissipatore_has_Socket`.`Socket_idSocket` FROM `Dissipatore_has_Socket` WHERE `Dissipatore_has_Socket`.`Dissipatore_Prodotto_idProdotto` = ?;");
			$coolerID = $cooler->getID();
			$stmt->bind_param("s", $coolerID);
			$stmt->execute();
			$result = $stmt->get_result();
			$rows = $result->fetch_all(MYSQLI_NUM);
			$sockets = [];
			foreach($rows as $row) {
				$sockets[] = $row[0];
			}
			$cooler->setSockets($sockets);
			$returns[] = $cooler;
		}
		return $returns;
	}
}

?>
