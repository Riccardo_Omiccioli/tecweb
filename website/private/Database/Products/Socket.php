<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\Products\Dissipatore as Dissipatore;

class Socket {

	private int $id;
	private String $name;
	private ?array $coolerIDs = null;

	private static ?array $sockets = null;

	private function __construct(int $id, String $name, ?array $coolerIDs) {
		$this->id = $id;
		$this->name = $name;
		$this->coolerIDs = $coolerIDs;
	}

	public function getName() {
		return $this->name;
	}

	public function getID() {
		return $this->id;
	}

	public function getCoolerIDs() {
		if ($this->cpus === null) {
			$this->cpus = Dissipatore::get($this->coolerIDs);
		}
		return $this->cpus;
	}

	public static function get(int $id) {
		if (static::$sockets === null) {
			static::getAll();
		}
		return static::$sockets[$id];
	}

	public static function getAll() {
		if (static::$sockets === null) {
			static::$sockets = [];
			$db = DatabaseReader::get();
			$result = $db->query("SELECT * FROM Socket;");
			foreach($result->fetch_all(MYSQLI_NUM) as $row) {
				static::$sockets[$row[0]] = new Socket($row[0], $row[1], null);
			}
		} 
		return static::$sockets;
	}

}


?>
