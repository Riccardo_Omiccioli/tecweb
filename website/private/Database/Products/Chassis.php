<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class Chassis extends Prodotto {

	private FormFactorMobo $formFactor;
	private int $length;
	private int $width;
	private int $height;
	private int $fans;
	private int $coolerHeight;
	private int $psuDepth;
	private int $gpuLength;
	private int $slots525;
	private int $slots35;
	private int $slots25;
	private int $slotsPCIe;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $formFactor, int $length, int $width, int $height, int $fans, int $coolerHeight, int $psuDepth, int $gpuLength,
								int $slots525, int $slots35, int $slots25, int $slotsPCIe) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($formFactor, $length, $width, $height, $fans, $coolerHeight, $psuDepth, $gpuLength, $slots525, $slots35, $slots25, $slotsPCIe);
	}

	public function setSpecs(int $formFactor, int $length, int $width, int $height, int $fans, int $coolerHeight, int $psuDepth, int $gpuLength, int $slots525, int $slots35, int $slots25, int $slotsPCIe) {
		$this->setFormFactor(FormFactorMobo::get($formFactor));
		$this->setLength($length);
		$this->setWidth($width);
		$this->setHeight($height);
		$this->setFans($fans);
		$this->setCoolerHeight($coolerHeight);
		$this->setPSUDepth($psuDepth);
		$this->setGPULength($gpuLength);
		$this->setSlots525($slots525);
		$this->setSlots35($slots35);
		$this->setSlots25($slots25);
		$this->setSlotsPCIe($slotsPCIe);
	}

	public function setFormFactor(FormFactorMobo $formFactor) {
		//TODO: check if valid
		$this->formFactor = $formFactor;
	}
	public function getFormFactor() {
		return $this->formFactor;
	}

	public function setLength(int $length) {
		if ($length <= 0) {
			throw new \OutOfBoundsException("Il case deve avere una lunghezza maggiore di 0mm (potenzialmente anche 10, ma intanto facciamo 0)");
		}
		$this->length = $length;
	}
	public function getLength() {
		return $this->length;
	}

	public function setWidth(int $width) {
		if ($width <= 0) {
			throw new \OutOfBoundsException("Il case deve avere una larghezza maggiore di 0mm");
		}
		$this->width = $width;
	}
	public function getWidtn() {
		return $this->width;
	}

	public function setHeight(int $height) {
		if ($height <= 0) {
			throw new \OutOfBoundsException("Il case deve avere un'altezza maggiore di 0mm");
		}
		$this->height = $height;
	}
	public function getHeight() {
		return $this->height;
	}

	public function setFans(int $fans) {
		if ($fans < 0) {
			throw new \OutOfBoundsException("Il case deve avere almeno 0 slot per le ventole");
		}
		$this->fans = $fans;
	}
	public function getFans() {
		return $this->fans;
	}

	public function setCoolerHeight(int $coolerHeight) {
		if ($coolerHeight <= 0) {
			throw new \OutOfBoundsException("Il case deve permettere un dissipatore più alto di 0mm");
		}
		$this->coolerHeight = $coolerHeight;
	}
	public function getCoolerHeight() {
		return $this->coolerHeight;
	}

	public function setPSUDepth(int $psuDepth) {
		if ($psuDepth <= 0) {
			throw new \OutOfBoundsException("Il case deve permettere un alimentatore più profondo di 0mm");
		}
		$this->psuDepth = $psuDepth;
	}
	public function getPSUDepth() {
		return $this->psuDepth;
	}

	public function setGPULength(int $gpuLength) {
		if ($gpuLength <= 0) {
			throw new \OutOfBoundsException("Il case permettere una GPU più lunga di 0mm");
		}
		$this->gpuLength = $gpuLength;
	}
	public function getGPULength() {
		return $this->gpuLength;
	}

	public function setSlots525(int $slots525) {
		if ($slots525 < 0) {
			throw new \OutOfBoundsException("Il numero di slot 5.25 non può essere negativo");
		}
		$this->slots525 = $slots525;
	}
	public function getSlots525() {
		return $this->slots525;
	}

	public function setSlots35(int $slots35) {
		if ($slots35 < 0) {
			throw new \OutOfBoundsException("Il numero di slot 3.5 non può essere negativo");
		}
		$this->slots35 = $slots35;
	}
	public function getSlots35() {
		return $this->slots35;
	}

	public function setSlots25(int $slots25) {
		if ($slots25 < 0) {
			throw new \OutOfBoundsException("Il numero di slot 2.5 non può essere negativo");
		}
		$this->slots25 = $slots25;
	}
	public function getSlots25() {
		return $this->slots25;
	}

	public function setSlotsPCIe(int $slotsPCIe) {
		if ($slotsPCIe < 0) {
			throw new \OutOfBoundsException("Il numero di slot PCI Express non può essere negativo");
		}
		$this->slotsPCIe = $slotsPCIe;
	}
	public function getSlotsPCIe() {
		return $this->slotsPCIe;
	}

	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Form factor", $this->formFactor->getName()],
			["Lunghezza", $this->length . " mm"],
			["Larghezza", $this->width . " mm"],
			["Altezza", $this->height . " mm"]
		];
		if ($this->getColor() !== null) {
			$localSpecs[] = ["Colore", $this->getColor()];
		}
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSpecs();
		$localSpecs = [
			["Form factor", $this->formFactor->getName()],
			["Lunghezza", $this->length . " mm"],
			["Larghezza", $this->width . " mm"],
			["Altezza", $this->height . " mm"],
			["Numero ventole", $this->fans],
			["Altezza massima dissipatore", $this->coolerHeight . " mm"],
			["Lunghezza massima alimentatore", $this->psuDepth . " mm"],
			["Lunghezza massima scheda video", $this->gpuLength . " mm"],
			["Numero slot 5.25\"", $this->slots525],
			["Numero slot 3.5\"", $this->slots35],
			["Numero slot 2.5\"", $this->slots25],
			["Numero slot PCIe", $this->slotsPCIe]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();
		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO Chassis VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `FormFactorMobo_idFormFactor`=VALUES(`FormFactorMobo_idFormFactor`), `lunghezza`=VALUES(`lunghezza`), `larghezza`=VALUES(`larghezza`), `altezza`=VALUES(`altezza`), " .
			"`numeroSlotVentole`=VALUES(`numeroSlotVentole`), `altezzaDissipatoreMax`=VALUES(`altezzaDissipatoreMax`), `profonditaAlimentatoreMax`=VALUES(`profonditaAlimentatoreMax`), " .
			"`lunghezzaSchedaVideoMax`=VALUES(`lunghezzaSchedaVideoMax`), `numeroSlot5.25`=VALUES(`numeroSlot5.25`), `numeroSlot3.5`=VALUES(`numeroSlot3.5`), `numeroSlot2.5`=VALUES(`numeroSlot2.5`), ". 
			"`numeroSlotPCIe`=VALUES(`numeroSlotPCIe`);");
		$formFactorID = $this->formFactor->getID();
		$stmt->bind_param("siiiiiiiiiiii", $id, $formFactorID, $this->length, $this->width, $this->height, $this->fans, $this->coolerHeight, $this->psuDepth, $this->gpuLength,
											$this->slots525, $this->slots35, $this->slots25, $this->slotsPCIe);
		$stmt->execute();
		$stmt->close();
		$db->commit();

		return $id;
	}


	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `Chassis`.`FormFactorMobo_idFormFactor`, `Chassis`.`lunghezza`, `Chassis`.`larghezza`, `Chassis`.`altezza`, `Chassis`.`numeroSlotVentole`, " .
				 "`Chassis`.`altezzaDissipatoreMax`, `Chassis`.`profonditaAlimentatoreMax`, `Chassis`.`lunghezzaSchedaVideoMax`, `Chassis`.`numeroSlot5.25`, `Chassis`.`numeroSlot3.5`, " .
				 "`Chassis`.`numeroSlot2.5`, `Chassis`.`numeroSlotPCIe` " .
				 "FROM `Prodotto` JOIN `Chassis` ON `Prodotto`.`idProdotto` = `Chassis`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new Chassis(...$row);
		}
		return $returns;
	}
}

?>
