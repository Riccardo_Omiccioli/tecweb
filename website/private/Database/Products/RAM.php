<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class RAM extends Prodotto {

	private String $RAMtype;
	private int $stickNumber;
	private int $stickDimension;
    private int $frequency;
	private int $CL;
	private int $RCD;
    private int $RP;
    private int $RAS;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								String $RAMtype, int $stickNumber, int $stickDimension, int $frequency, int $CL, int $RCD,  int $RP,  int $RAS) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($RAMtype, $stickNumber, $stickDimension, $frequency, $CL, $RCD, $RP, $RAS);
	}

	public function setSpecs(String $RAMtype, int $stickNumber, int $stickDimension, int $frequency, int $CL, int $RCD, int $RP, int $RAS) {
        $this->setRAMType($RAMtype);
		$this->setStickNumber($stickNumber);
		$this->setStickDimension($stickDimension);
		$this->setFrequency($frequency);
        $this->setCL($CL);
        $this->setRCD($RCD);
        $this->setRP($RP);
        $this->setRAS($RAS);
	}

    public function setRAMType(String $RAMtype) {
        if (strlen($RAMtype) > 45) {
            throw new \OutOfBoundsException("Valore tipo RAM troppo lungo");
        }
        $this->RAMtype = $RAMtype;
    }
    public function getRAMType() {
        return $this->RAMtype;
    }

	public function setStickNumber(int $stickNumber) {
		if ($stickNumber < 0) {
			throw new \OutOfBoundsException("Il numero banchi non può essere negativo");
		}
		$this->stickNumber = $stickNumber;
	}
	public function getStickNumber() {
		return $this->stickNumber;
	}

	public function setStickDimension(int $stickDimension) {
		if ($stickDimension < 0) {
			throw new \OutOfBoundsException("La dimensione dei banchi non può essere negativa");
		}
		$this->stickDimension = $stickDimension;
	}
	public function getStickDimension() {
		return $this->stickDimension;
	}

    public function setFrequency(int $frequency) {
		if ($frequency < 0) {
			throw new \OutOfBoundsException("La frequenza non può essere negativa");
		}
		$this->frequency = $frequency;
	}
	public function getFrequency() {
		return $this->frequency;
	}

    public function setCL(int $CL) {
		if ($CL < 0) {
			throw new \OutOfBoundsException("Il valore CL non può essere negativo");
		}
		$this->CL = $CL;
	}
	public function getCL() {
		return $this->CL;
	}

    public function setRCD(int $RCD) {
		if ($RCD < 0) {
			throw new \OutOfBoundsException("Il valore RCD non può essere negativo");
		}
		$this->RCD = $RCD;
	}
	public function getRCD() {
		return $this->RCD;
	}

    public function setRP(int $RP) {
		if ($RP < 0) {
			throw new \OutOfBoundsException("Il valore RP non può essere negativo");
		}
		$this->RP = $RP;
	}
	public function getRP() {
		return $this->RP;
	}

    public function setRAS(int $RAS) {
		if ($RAS < 0) {
			throw new \OutOfBoundsException("Il valore RAS non può essere negativo");
		}
		$this->RAS = $RAS;
	}
	public function getRAS() {
		return $this->RAS;
	}

	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Tipologia", $this->RAMtype],
			["Numero banchi", $this->stickNumber],
			["Dimensione banchi", $this->stickDimension . " GB"],
			["Frequenza", $this->frequency . " MHz"],
			["Timing CL", $this->CL]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Tipologia", $this->RAMtype],
			["Numero banchi", $this->stickNumber],
			["Dimensione banchi", $this->stickDimension . " GB"],
			["Frequenza", $this->frequency . " MHz"],
			["Timing CL", $this->CL],
			["Timing RCD", $this->RCD],
			["Timing RP", $this->RP],
			["Timing RAS", $this->RAS]
		];
		return array_merge($superSpecs, $localSpecs);
	}

	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();

		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO RAM VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `tipologia`=VALUES(`tipologia`), `numeroBanchi`=VALUES(`numeroBanchi`), `dimensioneBanchi`=VALUES(`dimensioneBanchi`), " .
			"`frequenza`=VALUES(`frequenza`), `timingCL`=VALUES(`timingCL`), `timingtRCD`=VALUES(`timingtRCD`), `timingtRP`=VALUES(`timingtRP`), `timingtRAS`=VALUES(`timingtRAS`);");
		$stmt->bind_param("ssiiiiiii", $id, $this->RAMtype, $this->stickNumber, $this->stickDimension, $this->frequency, $this->CL, $this->RCD, $this->RP, $this->RAS);
		$stmt->execute();
		$stmt->close();

		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$query = "SELECT `Prodotto`.*, `RAM`.`tipologia`, `RAM`.`numeroBanchi`, `RAM`.`dimensioneBanchi`, `RAM`.`frequenza`, `RAM`.`timingCL`, `RAM`.`timingtRCD`, " .
		"`RAM`.`timingtRP`, `RAM`.`timingtRAS` FROM `Prodotto` JOIN `RAM` ON `Prodotto`.`idProdotto` = `RAM`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = DatabaseReader::get()->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$returns[] = new RAM(...$row);
		}
		return $returns;
	}

}

?>
