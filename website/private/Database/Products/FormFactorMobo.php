<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;

class FormFactorMobo {

	private int $id;
	private String $name;

	private static ?array $formFactorMobos = null;

	private function __construct($data) {
		$this->id = $data['idFormFactor'];
		$this->name = $data['nome'];
	}

	public function getName() {
		return $this->name;
	}

	public function getID() {
		return $this->id;
	}

	public static function get(int $id) {
		if (static::$formFactorMobos === null) {
			static::getAll();
		}
		return static::$formFactorMobos[$id];
	}

	public static function getAll() {
		if (static::$formFactorMobos === null) {
			static::$formFactorMobos = [];
			$db = DatabaseReader::get();
			$result = $db->query("SELECT * FROM FormFactorMobo ORDER BY idFormFactor ASC;");
			foreach($result->fetch_all(MYSQLI_ASSOC) as $row) {
				static::$formFactorMobos[$row['idFormFactor']] = new FormFactorMobo($row);
			}
		} 
		return static::$formFactorMobos;
	}

}

?>
