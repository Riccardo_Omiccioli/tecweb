<?php

namespace Database\Products;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;
use Database\Products\Prodotto as Prodotto;

class MemoriaDiMassa extends Prodotto {

	private int $capacity;
	private int $cacheSize;
	private int $maxWriteSpeed;
    private int $maxReadSpeed;
	private String $interface;
	private int $powerConsumption;
    private String $formFactor;
    private ?int $rotationSpeed;
    private ?String $NANDType;
    private ?int $TBW;

	public function __construct(?String $ID, String $code, String $name, String $desc, int $price, int $quantity, ?String $color, String $tag, int $type, float $discount, String $date,
								int $capacity, int $cacheSize, int $maxWriteSpeed, int $maxReadSpeed, String $interface, int $powerConsumption, String $formFactor, ?int $rotationSpeed,
								?String $nandType, ?int $tbw) {
		parent::__construct($ID, $code, $name, $desc, $price, $quantity, $color, $tag, $type, $discount, $date);
		$this->setSpecs($capacity, $cacheSize, $maxWriteSpeed, $maxReadSpeed, $interface, $powerConsumption, $formFactor, $rotationSpeed, $nandType, $tbw);
	}


	public function setSpecs(int $capacity, int $cacheSize, int $maxWriteSpeed, int $maxReadSpeed, String $interface, int $powerConsumption, String $formFactor, ?int $rotationSpeed,
								?String $nandType, ?int $tbw) {
		$this->setCapacity($capacity);
		$this->setCacheSize($cacheSize);
		$this->setMaxWriteSpeed($maxWriteSpeed);
		$this->setMaxReadSpeed($maxReadSpeed);
        $this->setInterface($interface);
        $this->setPowerConsumption($powerConsumption);
        $this->setFormFactor($formFactor);
        $this->setRotationSpeed($rotationSpeed);
        $this->setNANDType($nandType);
        $this->setTBW($tbw);
	}

	public function setCapacity(int $capacity) {
		if ($capacity < 0) {
			throw new \OutOfBoundsException("La capacità non può essere negativa");
		}
		$this->capacity = $capacity;
	}
	public function getCapacity() {
		return $this->capacity;
	}

	public function setCacheSize(int $cacheSize) {
		if ($cacheSize < 0) {
			throw new \OutOfBoundsException("La capacità della cache non può essere negativa");
		}
		$this->cacheSize = $cacheSize;
	}
	public function getCacheSize() {
		return $this->cacheSize;
	}

	public function setMaxWriteSpeed(int $maxWriteSpeed) {
		if ($maxWriteSpeed < 0) {
			throw new \OutOfBoundsException("La velocità di scrittura non può essere negativa");
		}
		$this->maxWriteSpeed = $maxWriteSpeed;
	}
	public function getMaxWriteSpeed() {
		return $this->maxWriteSpeed;
	}

	public function setMaxReadSpeed(int $maxReadSpeed) {
		if ($maxReadSpeed < 0) {
			throw new \OutOfBoundsException("La velocità di lettura non può essere negativa");
		}
		$this->maxReadSpeed = $maxReadSpeed;
	}
	public function getMaxReadSpeed() {
		return $this->maxReadSpeed;
	}

	public function setInterface(String $interface) {
		if (strlen($interface) > 45) {
			throw new \OutOfBoundsException("Valore interfaccia troppo lungo");
		}
		$this->interface = $interface;
	}
	public function getInterface() {
		return $this->interface;
	}

	public function setPowerConsumption(int $powerConsumption) {
		if ($powerConsumption < 0) {
			throw new \OutOfBoundsException("Il consumo non può essere negativo");
		}
		$this->powerConsumption = $powerConsumption;
	}
	public function getPowerConsumption() {
		return $this->powerConsumption;
	}

	public function setFormFactor(String $formFactor) {
		if (strlen($formFactor) > 45) {
			throw new \OutOfBoundsException("Valore form factor troppo lungo");
		}
		$this->formFactor = $formFactor;
	}
	public function getFormFactor() {
		return $this->formFactor;
	}

	public function hasRotationSpeed() {
		return $this->rotationSpeed !== null;
	}
	public function setRotationSpeed(?int $rotationSpeed) {
		if ($rotationSpeed !== null && $rotationSpeed < 0) {
			throw new \OutOfBoundsException("La velocità di rotazione non può essere negativa");
		}
		$this->rotationSpeed = $rotationSpeed;
	}
	public function getRotationSpeed() {
		return $this->rotationSpeed;
	}

	public function setNANDType(?String $NANDType) {
		if ($NANDType !== null && strlen($NANDType) > 45) {
			throw new \OutOfBoundsException("Valore tipo NAND troppo lungo");
		}
		$this->NANDType = $NANDType;
	}
	public function getNANDType() {
		return $this->NANDType;
	}

    public function setTBW(?int $TBW) {
		if ($TBW !== null && $TBW < 0) {
			throw new \OutOfBoundsException("Il TBW non può essere negativo");
		}
		$this->TBW = $TBW;
	}
	public function getTBW() {
		return $this->TBW;
	}



	public function getSummarySpecs() {
		$superSpecs = parent::getSummarySpecs();
		$localSpecs = [
			["Capacità", \Util::formatSize($this->capacity, 1000, ["GB", "TB"])],
			["velocità di lettura", $this->maxReadSpeed . " MB/s"],
			["Form factor", $this->formFactor]
		];
		if ($this->hasRotationSpeed()) {
			$localSpecs[] = ["Velocità rotazione", $this->rotationSpeed . " rpm"];
		} else {
			$localSpecs[] = ["NVMe", $this->interface === "NVMe" ? "Sì" : "No"];
		}
		return array_merge($superSpecs, $localSpecs);
	}

	public function getSpecs() {
		$superSpecs = parent::getSpecs();
		$localSpecs = [
			["Capacità", \Util::formatSize($this->capacity, 1000, ["GB", "TB"])],
			["Dimensione cache", \Util::formatSize($this->cacheSize, 1000, ["MB", "GB"])],
			["Velocità di lettura", $this->maxReadSpeed . " MB/s"],
			["Velocità di scrittura", $this->maxWriteSpeed . " MB/s"],
			["Interfaccia", $this->interface],
			["Consumo", $this->powerConsumption . " W"],
			["Form factor", $this->formFactor]
		];
		if ($this->hasRotationSpeed()) {
			$localSpecs[] = ["Velocità rotazione", $this->rotationSpeed . " rpm"];
		} else {
			$localSpecs[] = ["Tipo di NAND", $this->NANDType];
			$localSpecs[] = ["TBW", $this->TBW . " TB"];
		}
		return array_merge($superSpecs, $localSpecs);
	}


	public function save() {
		//$this->idProduct = $this->idProduct ?? Util::uuid();
		$db = DatabaseWriter::get();

		$db->transaction();

		$id = parent::save();

		$stmt = $db->prepare("INSERT INTO MemoriaDiMassa VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " .
			"ON DUPLICATE KEY UPDATE `capacita`=VALUES(`capacita`), `dimensioneCache`=VALUES(`dimensioneCache`), `velocitaMassimaScrittura`=VALUES(`velocitaMassimaScrittura`), " .
			"`velocitaMassimaLettura`=VALUES(`velocitaMassimaLettura`), `interfaccia`=VALUES(`interfaccia`), `consumo`=VALUES(`consumo`), `formFactor`=VALUES(`formFactor`), " .
			"`velocitaRotazione`=VALUES(`velocitaRotazione`), `tipoNAND`=VALUES(`tipoNAND`), `TBW`=VALUES(`TBW`);");
		$stmt->bind_param("siiiisisisi", $id, $this->capacity, $this->cacheSize, $this->maxWriteSpeed, $this->maxReadSpeed, $this->interface, $this->powerConsumption, $this->formFactor,
			$this->rotationSpeed, $this->NANDType, $this->tbw);
		$stmt->execute();
		$stmt->close();

		$db->commit();

		return $id;
	}

	public static function get(array|String $ids) {
		if (is_string($ids)) {
			$ids = [$ids];
		}
		if (count($ids) < 1) {
			throw new Exception("Array di ID vuoto");
		}
		$db = DatabaseReader::get();
		$query = "SELECT `Prodotto`.*, `MemoriaDiMassa`.`capacita`, `MemoriaDiMassa`.`dimensioneCache`, `MemoriaDiMassa`.`velocitaMassimaScrittura`, `MemoriaDiMassa`.`velocitaMassimaLettura`, " .
			"`MemoriaDiMassa`.`interfaccia`, `MemoriaDiMassa`.`consumo`, `MemoriaDiMassa`.`formFactor`, `MemoriaDiMassa`.`velocitaRotazione`, `MemoriaDiMassa`.`tipoNand`, `MemoriaDiMassa`.`TBW` " .
			"FROM `Prodotto` JOIN `MemoriaDiMassa` ON `Prodotto`.`idProdotto` = `MemoriaDiMassa`.`Prodotto_idProdotto` WHERE `Prodotto`.`idProdotto` IN (";
		$query .= substr(str_repeat("?, ", count($ids)), 0, -2) . ");";
		$stmt = $db->prepare($query);
		$stmt->bind_param(str_repeat("s", count($ids)), ...$ids);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$rows = $result->fetch_all(MYSQLI_NUM);
		$returns = [];
		foreach ($rows as $i => $row) {
			$disk = new MemoriaDiMassa(...$row);
			$returns[] = $disk;
		}
		return $returns;
	}
}

?>
