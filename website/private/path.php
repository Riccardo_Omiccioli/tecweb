<?php


#Use inside php or inside html code with "<?php echo VAR;? >relativePath"
define("PROJECT_ROOT", dirname($_SERVER['DOCUMENT_ROOT']));
define("PAGE", PROJECT_ROOT . "/private/");
define("TEMPLATE", PAGE . "/template/");
define("JAVASCRIPT", "/static/javascript/");
define("CSS", "/static/CSS/");
define("ICON", "/static/icon/");
define("IMAGE", "/static/image/");
define("PRODUCTIMAGE", "/static/image/product/");
//questa qua da togliere e unire con le altre funzioni di utility
define("UTILS", "/private/utils/");

spl_autoload_register(function($className) {
	$classPath = PAGE . str_replace("\\", DIRECTORY_SEPARATOR, $className) . ".php";
	require_once($classPath);
});

?>
