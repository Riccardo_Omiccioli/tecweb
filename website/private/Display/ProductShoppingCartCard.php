<?php
namespace Display;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");
use \Database\Products\Prodotto as Prodotto;

class ProductShoppingCartCard {
	const STR = <<<EOD
		<div><img src="%s" alt="immagine prodotto"/></div>
		<section><p>%s</p><p>%s€</p><p class="%s">Disponibile:%d</p><p class='tooMuchNon unavailable'>Quantità eccessiva</p></section>
		<input type="number" id="%s" min="1" max="%d" oninput="this.value = Math.abs(this.value)" name="search" value="%d">
    	<button id="x"><img src="%scross.svg" alt="x"/></button>
    	
EOD;
	private Prodotto $product;
	private int $shoppingValue;
	public function __construct(Prodotto $product, int $shoppingValue) {
		$this->product = $product;
		$this->shoppingValue = $shoppingValue;
	}

	public function __toString() {
		$specs = [];
		foreach ($this->product->getSummarySpecs() as $spec) {
			$specs[] = implode(": ", $spec);
		}
		$discount = $this->product->getDiscount();
		return sprintf(self::STR,
				IMAGE . "/product/" . $this->product->getImages()[0],
				$this->product->getName(),
				number_format($this->product->getPrice()/ 100 * (1- $discount), 2),
				($this->product->getQuantity() > 0 ) ? "available" : "unavailable",
				$this->product->getQuantity(),
				$this->product->getID(),
				$this->product->getQuantity(),
				$this->shoppingValue,
				ICON
			);
	}
}
