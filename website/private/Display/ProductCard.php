<?php
namespace Display;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");
use \Database\Products\Prodotto as Prodotto;

class ProductCard {
	const STR = <<<EOD
	<li>
		<a href="productPage.php?product=%s">
			<img src="%s" alt="immagine prodotto"/>
			<section>
				<h2>%s</h2>
				%s
			</section>
			<p class="%s">Disponibili: %d</p>
			<span>
				%s
			</span>
			<span>
				%s
			</span>
			<span>
				%.2f€
			</span>
		</a>
	</li>
EOD;
	private Prodotto $product;
	public function __construct(Prodotto $product) {
		$this->product = $product;
	}

	public function __toString() {
		$specs = [];
		foreach ($this->product->getSummarySpecs() as $spec) {
			$specs[] = implode(": ", $spec);
		}
		$discount = $this->product->getDiscount();
		return sprintf(self::STR, 
				$this->product->getID(), 
				IMAGE . "/product/" . $this->product->getImages()[0], 
				$this->product->getName(), 
				implode("<br/>", $specs),
				($this->product->getQuantity() > 0 ) ? "available" : "unavailable", 
				$this->product->getQuantity(),
				$discount != 0 ? sprintf("%.2f€", $this->product->getPrice() / 100) : "",
				$discount != 0 ? sprintf("-%d%%", $discount*100) : "",
				$this->product->getPrice() / 100 * (1 - $discount)
			);
	}
}
