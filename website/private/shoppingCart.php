<?php 
$cart = [];
if (isset($_COOKIE['cart'])) {
    foreach(json_decode($_COOKIE["cart"]) as $productID => $quantity) {
        $cart[] = [\Database\Products\Prodotto::get($productID)[0], $quantity];
    }
}
$counter = 0;
?>

<div>
    <ul>
        <?php foreach(\Database\Products\Prodotto::getCategoryLinks() as $category): ?>
            <li>
                <p><?php echo $category['name']; ?></p><hr>
                <?php foreach($cart as $item) {

                    if ($item[0]->getType() === $category['link']) {
                        echo new \Display\ProductShoppingCartCard($item[0], $item[1]);
                        $counter = 1;
                    }
                }
                if($counter == 0) {
                        echo "<section class=\"empty\">
                                <p>Nessuna selezione</p>
                                <a href=/productList.php?category=".$category['link'].">Clicca qui per aggiungere prodotto</a>
                            </section>";
                    }
                    $counter = 0;
                ?>
        <?php endforeach; ?>
    </ul>

	<p id="totale">Totale: <?php
					$totale = 0;
					foreach($cart as $item) {
						$totale += ($item[0]->getPrice() * (1 - $item[0]->getDiscount())) * $item[1];
					}
					echo number_format($totale/100, 2)."€";
				?>
	</p>
	<button id="buy">Procedi all'acquisto</button>
    <p id='error' class='unavailable'>Inserire almeno un prodotto da acquistare</p>
</div>