/*
function toggleDrawer(element) {
    if($("body > main > div > ul > li > p").hasClass("open")) {
        $("body > main > div > ul > li > p").removeClass("open").slideUp();
    } else {
        $('body > main > div > ul > li > p').addClass("open").slideDown();
    }
}



*/


$(document).ready(function(){
    $("body > main > div > ul > li > p").click(function(event){
        event.preventDefault();
        if( $(this).next().next().hasClass("open") ){
            $(this).next().next().removeClass("open").slideUp();
        } else {
             $(this).next().next().addClass("open").slideDown();
        }
    });
});
/*
    $("body > main > div > div > div:nth-of-type(2)").click(function(event){
        event.preventDefault();
        if($("body > main > div > nav:nth-of-type(1)").hasClass("open")){
            toggleDrawerFilterLeft();
           
        }

        toggleDrawerFilterRight();
    });

    /*window.onresize = function(event){
        event.preventDefault();
        if($("body > header > form").is(":visible")) {
            closeSearch();
        }
    }
*/
