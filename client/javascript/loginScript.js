$(document).ready(function(){
    $(document.getElementById("showPassword")).click(function(event){
        event.preventDefault();
        if ($(document.getElementById("passwordIn")).attr("type") == "password") {
            $(document.getElementById("passwordIn")).attr("type", "text");
        } else {
            $(document.getElementById("passwordIn")).attr("type", "password");
        }
    })

    $(document.getElementById("showNewPassword")).click(function(event){
        event.preventDefault();
        if ($(document.getElementById("newRePasswordIn")).attr("type") == "password") {
            $(document.getElementById("newPasswordIn")).attr("type", "text");
            $(document.getElementById("newRePasswordIn")).attr("type", "text");
        } else {
            $(document.getElementById("newPasswordIn")).attr("type", "password");
            $(document.getElementById("newRePasswordIn")).attr("type", "password");
        }
    })
});