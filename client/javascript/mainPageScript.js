
function toggleDrawer() {
    if($("body > main > aside").hasClass("open")) {
        $("body > main > aside").removeClass("open").animate({left: "-=40%"}, 300, "linear", console.log("closing drawer"));
    } else {
        $('body > main > aside').addClass("open").animate({left: "+=40%"}, 300, "linear", console.log("opening drawer"));
    }
}

function closeSearch() {
    if($("body > main > form").hasClass("open")) {
        $("body > main > form").removeClass("open").hide();
    }
}

function toggleSearch() {
    console.log("toggle search");
    if($("body > main > form").hasClass("open")) {
        $("body > main > form").removeClass("open").slideUp();
    } else {
        $('body > main > form').addClass("open").slideDown();
    }
}

$(document).ready(function(){
    $("body > header > img:first-child").click(function(event){
        event.preventDefault();
        toggleDrawer();
    });

    $("body > header > img:nth-of-type(5)").click(function(event){
        toggleSearch();
    });

    window.onresize = function(event){
        event.preventDefault();
        if($("body > header > form").is(":visible")) {
            closeSearch();
        }
    }
});