$(document).ready(function(){
    $(document.getElementById("tipoIn")).change(function(event){
        event.preventDefault();
        var product = document.getElementById("tipoIn").value;
        $('body > main > div > form:not(#' + product + '):not(#productForm)').hide();
        switch(product) {
        case "alimentatore":
            $(document.getElementById("alimentatore")).show();
            break;
        case "case":
            $(document.getElementById("case")).show();
            break;
        case "dissipatoreAria":
            $(document.getElementById("dissipatoreAria")).show();
            break;
        case "dissipatoreLiquido":
            $(document.getElementById("dissipatoreLiquido")).show();
            break;
        case "HDD":
            $(document.getElementById("HDD")).show();
            break;
        case "RAM":
            $(document.getElementById("RAM")).show();
            break;
        case "monitor":
            $(document.getElementById("monitor")).show();
            break;
        case "pastaTermica":
            $(document.getElementById("pastaTermica")).show();
            break;
        case "CPU":
            $(document.getElementById("CPU")).show();
            break;
        case "schedaMadre":
            $(document.getElementById("schedaMadre")).show();
            break;
        case "schedaEthernet":
            $(document.getElementById("schedaEthernet")).show();
            break;
        case "schedaWifi":
            $(document.getElementById("schedaWifi")).show();
            break;
        case "GPU":
            $(document.getElementById("GPU")).show();
            break;
        case "SSD":
            $(document.getElementById("SSD")).show();
            break;
        case "ventola":
            $(document.getElementById("ventola")).show();
            break;
        }
    });

    $(document.getElementById("deleteButton")).click(function(event){
        event.preventDefault();
    });

    $(document.getElementById("saveButton")).click(function(event){
        event.preventDefault();
        let form = document.createElement("form");
        for (const child of document.getElementById("productForm").children) {
            form.appendChild(child.cloneNode());
        }
        let tipo = $(document.getElementById("tipoIn")).val();
        let form2 = document.getElementById(tipo);
        if(form2) {
            for (const child of document.getElementById(tipo).children) {
                form.appendChild(child.cloneNode());
            }
        }
        form.setAttribute("method", "post");
        form.setAttribute("action", "https://hardware2unbox.shop");
        form.setAttribute("style", "visibility:hidden;");
        document.getElementsByTagName("body")[0].appendChild(form);
        form.submit();
    });

});